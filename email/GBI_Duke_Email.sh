#
# Copyright: Copyright © 2017
# This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
# form by any means or for any purpose without the express written permission of Johnson & Johnson.
#
EMAIL_LIST="DL-NCSUS-GBI@ITS.JNJ.com"
BUSINESS_EMAIL="DL-NCSUS-GBI@ITS.JNJ.com"