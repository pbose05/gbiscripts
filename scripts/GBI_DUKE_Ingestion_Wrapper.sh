#!/bin/bash
#
# Copyright: Copyright © 2018
# This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
# form by any means or for any purpose without the express written permission of Johnson & Johnson.
#
#================================================================================================
#FILE           : GBI_DUKE_Ingestion_Wrapper.sh
#DESCRIPTION	: Generic script to copy file from S3 to EC2 and from EC2 to Postgres tables
#AUTHOR     	: Arijeet Mukherjee
#				: Sanjukta Bhattacharya
#VERSION    	: 1.1
#CREATED    	: 01.11.2017
#REVISON		: 31.01.2019
#               1)Added logic to pass Database Connection string as a parameter
#				2)Added exception hadling/file does not exists in s3 to the script  
#				3)Writing the output of the s3 to ec2 copy and ec2 to postgre to log file
#				4)Config file is used to pick up parameters and write to log file
#				5)Config file path changed
#				6)Deleteing intermediate files created in EC2 path after table load
#				7)Added Audit Logging to the Ingestion script
#				8)Added logic for Ingesting a particular Job
#================================================================================================
export ENV_FILE=/app/GBI/Duke/config/Config_Global_GBI_Duke.cfg
. ${ENV_FILE}
SCRIPT_NAME=$(basename -- "$0")
SCRIPT_NM=$(echo $SCRIPT_NAME | cut -d '.' -f1)
CURR_DATE=`date +"%Y%m%d_%H%M"`
LOG_FILE=$LOG_FILE_PATH/${SCRIPT_NM}_${CURR_DATE}.log

echo "$(date "+%F %T") ${SCRIPT_NM} Starts" | tee -a $LOG_FILE

#===================================================================================
# Ingestion job IN PROGRESS status entered in the ETL_RUN_CTL table
#===================================================================================
MASTER_START_TIME=`date +"%Y-%m-%d %T"`
MASTER_END_TIME=`date +"%Y-%m-%d %T"`
former_seconds=$(date --date="$MASTER_START_TIME" +%s)
later_seconds=$(date --date="$MASTER_END_TIME" +%s)
echo $((later_seconds-former_seconds)) > $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time
MASTER_ELAPSED_TIME=`cat $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time`

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "insert into GBI_CTL.ETL_RUN_CNTL values(DEFAULT,0,CURRENT_DATE,'$SCRIPT_NM','$MASTER_START_TIME','$MASTER_END_TIME','IN PROGRESS','$MASTER_ELAPSED_TIME',current_user,'$MASTER_START_TIME',current_user,'$MASTER_END_TIME');commit;";

#===================================================================================
# Parameter to pass for the Database connection
#===================================================================================

INGEST_JOB_NM=$1
echo "[Info] Ingesting for $INGEST_JOB_NM Files" >> $LOG_FILE

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "UPDATE GBI_CTL.ingest_param SET act_in ='N';"

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "UPDATE GBI_CTL.ingest_param SET act_in ='Y' WHERE file_nm IN (SELECT DISTINCT busn_rule_gr_3 FROM GBI_CDH.busn_rule WHERE busn_rule_gr_2 = '$INGEST_JOB_NM');"
#===================================================================================
# Ingestion parameter table loaded into an array for reading it and assigning values
#===================================================================================

Ingest_mstr_tbl=$(psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "select file_nm,file_src,file_tgt,tbl_nm,column_names from GBI_CTL.ingest_param where act_in='Y' and trunc_in='Y' and file_dlm=','";)

#===================================================================================
# Store the output from the ingest_param table to a temporary file temp.txt
#===================================================================================

echo "${Ingest_mstr_tbl}" >  $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | tail -n +1 > $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt
#===================================================================================
# Store the File names in the a temporary file file_name.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f1 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_name.txt

#===================================================================================
# Store the File source path in S3 in the a temporary file file_source.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f2 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_source.txt

#===================================================================================
#Exception Handling when one of the files do not come the process continues
#Logic would change later based on the business
#===================================================================================
for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_source.txt)
do
        echo "/usr/local/bin/aws s3 ls $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_check

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name.txt)
do
        echo "$field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_check

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_check $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_check > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_list

sed -e 's/$/ |head | rev | cut -f1 -d'"' '"' | rev/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_list > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_source.sh

for x in `sh $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_source.sh`
	do
			if [ -z $x ] #checking null
			then
				echo "$x does not exist"
				exit 1
			else
				echo "$x exist"
			fi
	done > $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_exist 

cat $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_exist | cut -d ' ' -f1 > $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_filter

fgrep -f $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_filter $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_dump_temp.txt

\cp $TEMP_FILE_PATH/${SCRIPT_NM}_dump_temp.txt $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt

#===================================================================================
# Store the File names in the a temporary file file_name.txt again 
# after exception handling
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f1 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_name.txt

#===================================================================================
# Store the File source path in S3 in the a temporary file file_source.txt again 
# after exception handling
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f2 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_source.txt


#===================================================================================
# Store the File target path in S3 in the a temporary file file_tgt.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f3 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt.txt

#===================================================================================
# Store the Staging table name in Postgres in the a temporary file stg_table.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f4 >  $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table.txt

#===================================================================================
# Store the Column names of each table in Postgres in the a temporary file 
# column_names.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f5 >  $TEMP_FILE_PATH/${SCRIPT_NM}_column_names.txt

#===================================================================================
# Loop the contents of the file_name.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name.txt)
do
        echo "$field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script

#===================================================================================
# Loop the contents of the file_source.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_source.txt)
do
        echo "/usr/local/bin/aws s3 cp $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_script

#===================================================================================
# Loop the contents of the file_tgt.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt.txt)
do
        echo " $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script

#===================================================================================
# Loop the contents of the column_names.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

#for field in $(cat column_names.txt)
#do
#        echo " $field"
#done > column_names_script

#===================================================================================
# Loop the contents of the stg_table.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table.txt)
do
        echo "\copy $field "
done > $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_script

#===================================================================================
# Add column names to the copy command required for creation of the generic script 
# to copy from S3 to EC2
#===================================================================================

paste $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_script $TEMP_FILE_PATH/${SCRIPT_NM}_column_names.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_copy_cmd

sed -i -e 's/$/ FROM/' $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_copy_cmd

#===================================================================================
# Combine all the temp files created in the above steps to generate the script  
# to copy all the files in S3 to EC2 path defined in the ingest_param table
#===================================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_s3_to_ec2.sh

#===================================================================================
# Execute the script to copy files from S3 to EC2
#===================================================================================

sh $TEMP_FILE_PATH/${SCRIPT_NM}_copy_s3_to_ec2.sh | tee -a $LOG_FILE


#===================================================================================
# Remove invalid UTF8 encoded data in the source file 
#===================================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script > $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv
sed -e 's/^/iconv -f utf-8 -t utf-8 -c /' $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv > $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_iconv

cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script | cut -d '.' -f1 > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_without_type

sed -e 's/$/_UTF8/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_without_type > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8

sed -i -e "s/$/.csv/" $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8 > $TEMP_FILE_PATH/${SCRIPT_NM}_src_tmp_conv
paste -d '>' $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_iconv $TEMP_FILE_PATH/${SCRIPT_NM}_src_tmp_conv > $TEMP_FILE_PATH/${SCRIPT_NM}_src_fin_conv.sh

#===================================================================================
# Execute the generic script to remove invalid UTF8 encoded data in the source file 
#===================================================================================

sh $TEMP_FILE_PATH/${SCRIPT_NM}_src_fin_conv.sh

#===================================================================================
# Copy the updated source file back to the original file 
#===================================================================================

sed -i -e 's/^/\\cp /' $TEMP_FILE_PATH/${SCRIPT_NM}_src_tmp_conv

paste $TEMP_FILE_PATH/${SCRIPT_NM}_src_tmp_conv $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv > $TEMP_FILE_PATH/${SCRIPT_NM}_cp_to_original.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_cp_to_original.sh

#===================================================================================
# Create generic COPY command for all the files
# Files to be copied from EC2 to Postgres table in GBI_STG schema
#===================================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_copy_cmd $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script > $TEMP_FILE_PATH/${SCRIPT_NM}_merged_copy_to_psql_tmp.sql

sed -i 's/$/ DELIMITER '"'"','"'"' csv header;/' $TEMP_FILE_PATH/${SCRIPT_NM}_merged_copy_to_psql_tmp.sql


####################	
#===================================================================================
# Truncate the table names loaded in Postgres DB
#===================================================================================

sed -i -e 's/^/truncate table /' $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table.txt

sed -i -e 's/$/;/' $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table.txt $TEMP_FILE_PATH/${SCRIPT_NM}_merged_copy_to_psql_tmp.sql > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_ec2_to_psql.sql

catch_error='\set ON_ERROR_STOP on'
echo $catch_error > $TEMP_FILE_PATH/${SCRIPT_NM}_catch_error_in_sql.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_catch_error_in_sql.txt $TEMP_FILE_PATH/${SCRIPT_NM}_copy_ec2_to_psql.sql > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_ec2_to_psql_final_comma.sql


#################Pipe delimited starts############

#===================================================================================
# Ingestion parameter table loaded into an array for reading it and assigning values
#===================================================================================

Ingest_mstr_tbl=$(psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "select file_nm,file_src,file_tgt,tbl_nm,column_names from GBI_CTL.ingest_param where act_in='Y' and trunc_in='Y' and file_dlm='|'";)

#===================================================================================
# Store the output from the ingest_param table to a temporary file temp.txt
#===================================================================================

echo "${Ingest_mstr_tbl}" >  $TEMP_FILE_PATH/${SCRIPT_NM}_temp_pipe.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp_pipe.txt | tail -n +1 > $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt
#===================================================================================
# Store the File names in the a temporary file file_name.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt | cut -d '|' -f1 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_pipe.txt

#===================================================================================
# Store the File source path in S3 in the a temporary file file_source.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt | cut -d '|' -f2 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_pipe.txt

#===================================================================================
#Exception Handling when one of the files do not come the process continues
#Logic would change later based on the business
#===================================================================================
for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_pipe.txt)
do
        echo "/usr/local/bin/aws s3 ls $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_check_pipe

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_pipe.txt)
do
        echo "$field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_check_pipe

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_check_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_check_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_list_pipe

sed -e 's/$/ |head | rev | cut -f1 -d'"' '"' | rev/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_list_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_source_pipe.sh

for x in `sh $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_source_pipe.sh`
	do
			if [ -z $x ] #checking null
			then
				echo "$x does not exist"
				exit 1
			else
				echo "$x exist"
			fi
	done > $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_exist_pipe

cat $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_exist_pipe | cut -d ' ' -f1 > $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_filter_pipe

fgrep -f $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_filter_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_temp_pipe.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_dump_temp_pipe.txt

\cp $TEMP_FILE_PATH/${SCRIPT_NM}_dump_temp_pipe.txt $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt

#===================================================================================
# Store the File names in the a temporary file file_name.txt again 
# after exception handling
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt | cut -d '|' -f1 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_pipe.txt

#===================================================================================
# Store the File source path in S3 in the a temporary file file_source.txt again 
# after exception handling
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt | cut -d '|' -f2 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_pipe.txt


#===================================================================================
# Store the File target path in S3 in the a temporary file file_tgt.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt | cut -d '|' -f3 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_pipe.txt

#===================================================================================
# Store the Staging table name in Postgres in the a temporary file stg_table.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt | cut -d '|' -f4 >  $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_pipe.txt

#===================================================================================
# Store the Column names of each table in Postgres in the a temporary file 
# column_names.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2_pipe.txt | cut -d '|' -f5 >  $TEMP_FILE_PATH/${SCRIPT_NM}_column_names_pipe.txt

#===================================================================================
# Loop the contents of the file_name.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_pipe.txt)
do
        echo "$field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script_pipe

#===================================================================================
# Loop the contents of the file_source.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_pipe.txt)
do
        echo "/usr/local/bin/aws s3 cp $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_script_pipe

#===================================================================================
# Loop the contents of the file_tgt.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_pipe.txt)
do
        echo " $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script_pipe

#===================================================================================
# Loop the contents of the column_names.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

#for field in $(cat column_names.txt)
#do
#        echo " $field"
#done > column_names_script

#===================================================================================
# Loop the contents of the stg_table.txt temp file to generate another temp file  
# required for creation of the generic script to copy from S3 to EC2
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_pipe.txt)
do
        echo "\copy $field "
done > $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_script_pipe

#===================================================================================
# Add column names to the copy command required for creation of the generic script 
# to copy from S3 to EC2
#===================================================================================

paste $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_script_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_column_names_pipe.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_copy_cmd_pipe

sed -i -e 's/$/ FROM/' $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_copy_cmd_pipe

#===================================================================================
# Combine all the temp files created in the above steps to generate the script  
# to copy all the files in S3 to EC2 path defined in the ingest_param table
#===================================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_script_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_s3_to_ec2_pipe.sh

#===================================================================================
# Execute the script to copy files from S3 to EC2
#===================================================================================

sh $TEMP_FILE_PATH/${SCRIPT_NM}_copy_s3_to_ec2_pipe.sh | tee -a $LOG_FILE


#===================================================================================
# Remove invalid UTF8 encoded data in the source file 
#===================================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv_pipe
sed -e 's/^/iconv -f utf-8 -t utf-8 -c /' $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_iconv_pipe

cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script_pipe | cut -d '.' -f1 > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_without_type_pipe

sed -e 's/$/_UTF8/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_without_type_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8_pipe

sed -i -e "s/$/.txt/" $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8_pipe

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_src_tmp_conv_pipe
paste -d '>' $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_iconv_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_src_tmp_conv_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_src_fin_conv_pipe.sh

#===================================================================================
# Execute the generic script to remove invalid UTF8 encoded data in the source file 
#===================================================================================

sh $TEMP_FILE_PATH/${SCRIPT_NM}_src_fin_conv_pipe.sh

#===================================================================================
# Copy the updated source file back to the original file 
#===================================================================================

sed -i -e 's/^/\\cp /' $TEMP_FILE_PATH/${SCRIPT_NM}_src_tmp_conv_pipe

paste $TEMP_FILE_PATH/${SCRIPT_NM}_src_tmp_conv_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_cp_to_original_pipe.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_cp_to_original_pipe.sh

#===================================================================================
# Create generic COPY command for all the files
# Files to be copied from EC2 to Postgres table in GBI_STG schema
#===================================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_copy_cmd_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_merged_copy_to_psql_tmp_pipe.sql

sed -i 's/$/ DELIMITER '"'"'|'"'"' csv;/' $TEMP_FILE_PATH/${SCRIPT_NM}_merged_copy_to_psql_tmp_pipe.sql

#===================================================================================
# Truncate the table names loaded in Postgres DB
#===================================================================================

sed -i -e 's/^/truncate table /' $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_pipe.txt

sed -i -e 's/$/;/' $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_pipe.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_stg_table_pipe.txt $TEMP_FILE_PATH/${SCRIPT_NM}_merged_copy_to_psql_tmp_pipe.sql > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_ec2_to_psql_pipe.sql

#catch_error='\set ON_ERROR_STOP on'
#echo $catch_error > $TEMP_FILE_PATH/${SCRIPT_NM}_catch_error_in_sql.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_copy_ec2_to_psql_final_comma.sql $TEMP_FILE_PATH/${SCRIPT_NM}_copy_ec2_to_psql_pipe.sql > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_ec2_to_psql_final.sql

#################Pipe delimited ends############

#===================================================================================
# Execute the COPY and truncate table sqls for all the files
# Tables to the truncated
# Files to be copied from EC2 to Postgres table in GBI_STG schema
#===================================================================================
MASTER_END_TIME2=`date +"%Y-%m-%d %T"`
former_second2=$(date --date="$MASTER_START_TIME" +%s)
later_second2=$(date --date="$MASTER_END_TIME2" +%s)
echo $((later_second2-former_second2)) > $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time_2
MASTER_ELAPSED_TIME2=`cat $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time_2`

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -a -f "$TEMP_FILE_PATH/${SCRIPT_NM}_copy_ec2_to_psql_final.sql" &>> $LOG_FILE
Sql_Status=$?

if [ $Sql_Status -eq 0 ]
		then
			psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='SUCCESSFUL',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$SCRIPT_NM' and sts='IN PROGRESS';commit;";
				echo "$SCRIPT_NM Completed Successfully" | mailx -s "$SCRIPT_NM Status" $EMAIL_LIST
		else
			psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='FAILED',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$SCRIPT_NM' and sts='IN PROGRESS';commit;";
				echo "$SCRIPT_NM Failed" | mailx -s "$SCRIPT_NM Status" $EMAIL_LIST
fi	

#===================================================================================
# Delete the temporary files and files created in ec2 path during Ingestion
#===================================================================================
if [ $Sql_Status -eq 0 ]

then

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8 > $TEMP_FILE_PATH/${SCRIPT_NM}_file_UTF8_delete

sed -e 's/^/rm -f/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_UTF8_delete > $TEMP_FILE_PATH/${SCRIPT_NM}_UTF8_file_ec2_delete.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_UTF8_file_ec2_delete.sh

sed -e 's/^/rm -f/' $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv > $TEMP_FILE_PATH/${SCRIPT_NM}_orig_file_ec2_delete.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_orig_file_ec2_delete.sh

###################Pipe files deletion

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_file_UTF8_delete_pipe

sed -e 's/^/rm -f/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_UTF8_delete_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_UTF8_file_ec2_delete_pipe.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_UTF8_file_ec2_delete_pipe.sh

sed -e 's/^/rm -f/' $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_orig_file_ec2_delete_pipe.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_orig_file_ec2_delete_pipe.sh

####################

rm -f ${TEMP_FILE_PATH}/${SCRIPT_NM}_*

echo "$(date "+%F %T") ${SCRIPT_NM} Ends" | tee -a $LOG_FILE

else

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8 > $TEMP_FILE_PATH/${SCRIPT_NM}_file_UTF8_delete

sed -e 's/^/rm -f/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_UTF8_delete > $TEMP_FILE_PATH/${SCRIPT_NM}_UTF8_file_ec2_delete.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_UTF8_file_ec2_delete.sh

sed -e 's/^/rm -f/' $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv > $TEMP_FILE_PATH/${SCRIPT_NM}_orig_file_ec2_delete.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_orig_file_ec2_delete.sh

###################Pipe files deletion

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_script_pipe $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_UTF8_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_file_UTF8_delete_pipe

sed -e 's/^/rm -f/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_UTF8_delete_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_UTF8_file_ec2_delete_pipe.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_UTF8_file_ec2_delete_pipe.sh

sed -e 's/^/rm -f/' $TEMP_FILE_PATH/${SCRIPT_NM}_src_path_conv_pipe > $TEMP_FILE_PATH/${SCRIPT_NM}_orig_file_ec2_delete_pipe.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_orig_file_ec2_delete_pipe.sh

####################

rm -f ${TEMP_FILE_PATH}/${SCRIPT_NM}_*

echo "$(date "+%F %T") ${SCRIPT_NM} Ends" | tee -a $LOG_FILE

exit 1

fi