#!/bin/bash
#
# Copyright: Copyright © 2018
# This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
# form by any means or for any purpose without the express written permission of Johnson & Johnson.
#
#================================================================================================
#FILE           : GBI_DUKE_RS_S3.sh
#DESCRIPTION	: Retrieval of DPS and CSS sales files from OneMD Redshift DB to S3
#AUTHOR     	: Arijeet Mukherjee
#VERSION    	: 1.0
#CREATED    	: 31.01.2018
#REVISON		: 09.02.2018
#				1) Added Audit Logging to script
#================================================================================================
#===============================
# Set environment variables
#===============================

export ENV_FILE=/app/GBI/Duke/config/Config_Global_GBI_Duke.cfg
. ${ENV_FILE}

SCRIPT_NAME=$(basename -- "$0")
SCRIPT_NM=$(echo $SCRIPT_NAME | cut -d '.' -f1)
CURR_DATE=`date +"%Y%m%d_%H%M"`
LOG_FILE=$LOG_FILE_PATH/${SCRIPT_NM}_${CURR_DATE}.log

echo "$(date "+%F %T") ${SCRIPT_NM} Execution Starts" | tee -a $LOG_FILE
#===================================================================================
# RS to S3 job IN PROGRESS status entered in the ETL_RUN_CTL table
#===================================================================================
MASTER_START_TIME=`date +"%Y-%m-%d %T"`
MASTER_END_TIME=`date +"%Y-%m-%d %T"`
former_seconds=$(date --date="$MASTER_START_TIME" +%s)
later_seconds=$(date --date="$MASTER_END_TIME" +%s)
echo $((later_seconds-former_seconds)) > $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time
MASTER_ELAPSED_TIME=`cat $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time`

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "insert into GBI_CTL.ETL_RUN_CNTL values(DEFAULT,0,CURRENT_DATE,'$SCRIPT_NM','$MASTER_START_TIME','$MASTER_END_TIME','IN PROGRESS','$MASTER_ELAPSED_TIME',current_user,'$MASTER_START_TIME',current_user,'$MASTER_END_TIME');commit;";
#===================================================================================
# Parameter to pass for the Database connection
#===================================================================================

INGEST_JOB_NM=$1
echo "[Info] Ingesting for $INGEST_JOB_NM Files" >> $LOG_FILE

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "UPDATE gbi_ctl.etl_metadata SET exec_flg ='N';"

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "UPDATE gbi_ctl.etl_metadata SET exec_flg ='Y' WHERE s3_file_nm IN (SELECT DISTINCT busn_rule_gr_3 FROM GBI_CDH.busn_rule WHERE busn_rule_gr_2 = '$INGEST_JOB_NM');"
#===================================================================================
# Hitting the etl_metadata table to get the OneMD sales file details
#===================================================================================

sql_run_master=$(psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "select s3_file_nm,sql_file_nm,obj_id,s3_file_path,file_tgt,sql_file_nm_2,file_intrm_path,s3_landing_path,sql_file_path from gbi_ctl.etl_metadata where src_typ='Redshift' and exec_flg='Y' ORDER BY obj_id";)

echo "${sql_run_master}" >  $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f1 >  $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_name.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f2 >  $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f3 >  $TEMP_FILE_PATH/${SCRIPT_NM}_obj_id.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f4 >  $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f5 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f6 >  $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name_2.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f7 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_intrm_path.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f8 >  $TEMP_FILE_PATH/${SCRIPT_NM}_s3_landing_path.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | cut -d '|' -f9 >  $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_ec2.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_name.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_obj_id.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name_2.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_file_intrm_path.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_s3_landing_path.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_ec2.txt

#=============================================
# Get access key and secret key
#=============================================

cat ~/.aws/credentials | grep aws_access_key_id | awk -F'=' '{print$2}' | sed -e 's/ //g' > $TEMP_FILE_PATH/${SCRIPT_NM}_access_key

cat ~/.aws/credentials | grep aws_secret_access_key | awk -F'=' '{print$2}' | sed -e 's/ //g' > $TEMP_FILE_PATH/${SCRIPT_NM}_secret_key

paste $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name.txt $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name_2.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_names

access_key=`cat $TEMP_FILE_PATH/${SCRIPT_NM}_access_key`
secret_key=`cat $TEMP_FILE_PATH/${SCRIPT_NM}_secret_key`

cat $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_names | while read sql_file_nm_path sql_file_nm_path_var
	do
		cat $SQL_FILE_PATH/$sql_file_nm_path_var | sed -e "s/"v1"/${access_key}/g;s/"v2"/${secret_key}/g" > $SQL_FILE_PATH/$sql_file_nm_path
	done
#=============================================
# Extract the files from OneMD to S3 instance
#=============================================
export DB_CONF_FILE=/app/GBI/Duke/config/dbcon.properties
. ${DB_CONF_FILE}

paste $TEMP_FILE_PATH/${SCRIPT_NM}_obj_id.txt $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name.txt $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_name.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_id_sql_s3_name
		
cat $TEMP_FILE_PATH/${SCRIPT_NM}_id_sql_s3_name | while read obj_id_nm sql_file_name s3_file_name
	do	
		psql --host=$HOST_RS --port=$PORT_RS --username=$USERNAME_RS --dbname=$DBNAME_RS -t -f $SQL_FILE_PATH/$sql_file_name &>> $LOG_FILE
		Script_Status=$?
				if [ $Script_Status -eq 0 ]
				  then
					echo "$(date "+%F %T") INFO: ${sql_file_name} SQL has been successfully executed." | tee -a ${LOG_FILE}
				else
					echo "$(date "+%F %T") FAILURE: ${sql_file_name} SQL has FAILED." | tee -a ${LOG_FILE}
					echo "$sql_file_name Failed" | mailx -s "$sql_file_name Failed" $EMAIL_LIST
					break
				fi
	done
#====================================================================
# Copy from S3 bucket to EC2. 
#====================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path.txt)
do
	echo "/usr/local/bin/aws s3 cp $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path_script

paste $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_to_ec2

sed -e 's/$/ --recursive/' $TEMP_FILE_PATH/${SCRIPT_NM}_copy_to_ec2 > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_to_ec2_cmd.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_copy_to_ec2_cmd.sh &>> $LOG_FILE
Copy_to_ec2_Status=$?

if [ $Copy_to_ec2_Status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_copy_to_ec2_cmd.sh " | tee -a ${LOG_FILE}
fi				

#====================================================================
# Merge the part files in ec2
#====================================================================

sed -e 's/$/* /' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_all

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_intrm_path.txt $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_name.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name

paste -d '> ' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_all $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name > $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_nm_script

sed -e 's/^/cat /' $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_nm_script > $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_nm_script_final.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_nm_script_final.sh &>> $LOG_FILE
ec2_path_file_nm_status=$?

if [ $ec2_path_file_nm_status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_nm_script_final.sh " | tee -a ${LOG_FILE}
fi
#====================================================================
# Load the merged file in ec2 to S3 landing location
#====================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name)
do
	echo "/usr/local/bin/aws s3 cp $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_script

paste $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_script $TEMP_FILE_PATH/${SCRIPT_NM}_s3_landing_path.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_copy_to_s3_cmd.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_copy_to_s3_cmd.sh &>> $LOG_FILE
Copy_to_s3_Status=$?

if [ $Copy_to_s3_Status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_script_cmd.sh " | tee -a ${LOG_FILE}
fi

#===================================================================================
# RS to S3 job completion status entered in the ETL_RUN_CTL table and email
#===================================================================================
PGPASSWORD=`cat /root/.pgpass | awk -F'*:' '{print$5}'`

MASTER_END_TIME2=`date +"%Y-%m-%d %T"`
former_second2=$(date --date="$MASTER_START_TIME" +%s)
later_second2=$(date --date="$MASTER_END_TIME2" +%s)
echo $((later_second2-former_second2)) > $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time_2
MASTER_ELAPSED_TIME2=`cat $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time_2`

if [ $Copy_to_ec2_Status -eq 0 ] && [ $ec2_path_file_nm_status -eq 0 ] && [ $Copy_to_s3_Status -eq 0 ]
		then
			psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='SUCCESSFUL',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$SCRIPT_NM' and sts='IN PROGRESS';commit;";
				echo "$SCRIPT_NM Completed Successfully" | mailx -s "$SCRIPT_NM Status" $EMAIL_LIST
		else
			psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='FAILED',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$SCRIPT_NM' and sts='IN PROGRESS';commit;";
				echo "$SCRIPT_NM Failed" | mailx -s "$SCRIPT_NM Status" $EMAIL_LIST
fi
#====================================================================
# Delete intermediate s3 paths and files. 
#====================================================================
if [ $Copy_to_ec2_Status -eq 0 ] && [ $ec2_path_file_nm_status -eq 0 ] && [ $Copy_to_s3_Status -eq 0 ]

then

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path.txt)
do
	echo "/usr/local/bin/aws s3 rm $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path_script_delete

sed -e 's/$/ --recursive/' $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path_script_delete > $TEMP_FILE_PATH/${SCRIPT_NM}_delete_part_files_from_s3.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_delete_part_files_from_s3.sh &>> $LOG_FILE
Delete_part_files_s3_Status=$?

if [ $Delete_part_files_s3_Status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_delete_part_files_from_s3.sh " | tee -a ${LOG_FILE}
fi			
#====================================================================
# Delete intermediate ec2 paths and files. 
#====================================================================

sed -e 's/^/rm -f /' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_all > $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_file_deletes.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_file_deletes.sh &>> $LOG_FILE
ec2_file_delete_status=$?

if [ $ec2_file_delete_status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_file_deletes.sh " | tee -a ${LOG_FILE}
fi

#====================================================================
# Delete sql files having access key and secret key from ec2 instance 
#====================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_ec2.txt $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path

sed -e 's/^/rm -f /' $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path > $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_final.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_final.sh &>> $LOG_FILE

sql_file_delete_status=$?

if [ $sql_file_delete_status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_final.sh " | tee -a ${LOG_FILE}
fi	

sed -e 's/^/rm -f /' $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name > $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_delete.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_delete.sh &>> $LOG_FILE

ec2_path_file_delete_status=$?

if [ $ec2_path_file_delete_status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_delete.sh " | tee -a ${LOG_FILE}
fi	

rm -f ${TEMP_FILE_PATH}/${SCRIPT_NM}_*

echo "$(date "+%F %T") ${SCRIPT_NM} Execution Ends" | tee -a $LOG_FILE

else

#====================================================================
# Delete intermediate s3 paths and files. 
#====================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path.txt)
do
	echo "/usr/local/bin/aws s3 rm $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path_script_delete

sed -e 's/$/ --recursive/' $TEMP_FILE_PATH/${SCRIPT_NM}_s3_file_path_script_delete > $TEMP_FILE_PATH/${SCRIPT_NM}_delete_part_files_from_s3.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_delete_part_files_from_s3.sh &>> $LOG_FILE
Delete_part_files_s3_Status=$?

if [ $Delete_part_files_s3_Status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_delete_part_files_from_s3.sh " | tee -a ${LOG_FILE}
fi

#====================================================================
# Delete intermediate ec2 paths and files. 
#====================================================================

sed -e 's/^/rm -f /' $TEMP_FILE_PATH/${SCRIPT_NM}_file_tgt_all > $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_file_deletes.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_file_deletes.sh &>> $LOG_FILE
ec2_file_delete_status=$?

if [ $ec2_file_delete_status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_file_deletes.sh " | tee -a ${LOG_FILE}
fi	

#====================================================================
# Delete sql files having access key and secret key from ec2 instance 
#====================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_ec2.txt $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_name.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path

sed -e 's/^/rm -f /' $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path > $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_final.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_final.sh &>> $LOG_FILE

sql_file_delete_status=$?

if [ $sql_file_delete_status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_sql_file_path_final.sh " | tee -a ${LOG_FILE}
fi

sed -e 's/^/rm -f /' $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name > $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_delete.sh

sh $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_delete.sh &>> $LOG_FILE

ec2_path_file_delete_status=$?

if [ $ec2_path_file_delete_status -ne 0 ]
	then
		echo "$(date "+%F %T") FAILED. Please check the command $TEMP_FILE_PATH/${SCRIPT_NM}_ec2_path_file_name_delete.sh " | tee -a ${LOG_FILE}
fi

rm -f ${TEMP_FILE_PATH}/${SCRIPT_NM}_*

echo "$(date "+%F %T") ${SCRIPT_NM} Execution Ends" | tee -a $LOG_FILE

exit 1

fi
