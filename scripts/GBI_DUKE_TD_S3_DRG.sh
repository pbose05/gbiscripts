#/**************************************************************************************************************/
#
# Script Name :         GBI_DUKE_TD_S3_DRG.sh
# Author  :             Sanjukta Bhattacharya
# Creation Date :       30-January-2018
# Description :         This script is used for loading DRG data from SMGS_V.EUSS_SUB_NATIONAL_SALES to a EUSS_SUB_NATIONAL_SALES_DRG.csv file.
# Input parameters :    NA
# Output parameters:    NA
#
#/**************************************************************************************************************/
export RUNDATE=$(date +"%m-%d-%Y")
export ENV_FILE=/app/GBI/Duke/config/Config_Global_GBI_Duke.cfg
if [ ! -f "${ENV_FILE}" ]; then
  echo -e "[ERR] Environment file ${ENV_FILE} does not exist!" >> ${AUDIT_LOG}
exit 2 
fi
. ${ENV_FILE}
SCRIPT_NAME=$(basename -- "$0")
SCRIPT_NM=$(echo $SCRIPT_NAME | cut -d '.' -f1)
export AUDIT_LOG=$LOG_FILE_PATH/${SCRIPT_NM}_$$_${CURR_DATE}.log
export Target_File=$DATA_IN_FILE_PATH/EUSS_SUB_NATIONAL_SALES_DRG.csv
#export Target_File=$S3_LOCATION_LANDING/EUSS_SUB_NATIONAL_SALES_DRG.csv
echo -e "[INFO] Script started at $(date +"%m-%d-%Y %T")." > ${AUDIT_LOG}

#===================================================================================
# Export job IN PROGRESS status entered in the ETL_RUN_CTL table
#===================================================================================
MASTER_START_TIME=`date +"%Y-%m-%d %T"`
MASTER_END_TIME=`date +"%Y-%m-%d %T"`
former_seconds=$(date --date="$MASTER_START_TIME" +%s)
later_seconds=$(date --date="$MASTER_END_TIME" +%s)
echo $((later_seconds-former_seconds)) > $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time
MASTER_ELAPSED_TIME=`cat $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time`

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "insert into GBI_CTL.ETL_RUN_CNTL values(DEFAULT,0,CURRENT_DATE,'$SCRIPT_NM','$MASTER_START_TIME','$MASTER_END_TIME','IN PROGRESS','$MASTER_ELAPSED_TIME',current_user,'$MASTER_START_TIME',current_user,'$MASTER_END_TIME');commit;";



echo "[INFO] Entering the bteq " >>  ${AUDIT_LOG}
bteq << EOI >> ${AUDIT_LOG}
.SET ERROROUT STDOUT
.SET WIDTH 1000
.logmech ldap;
.logon ${TD_SERVER}/${ETL_USER},${ETL_PWD}
.EXPORT FILE = ${Target_File}
.SET SEPARATOR ','
.SET PAGELENGTH 2000
.set titledashes off


SELECT	TRIM(PROD_BASE_CD), TRIM(FISC_YR_ID), TRIM(QTR_ID), TRIM(FISC_MO_ID),TRIM( AGGR_EUS_PROD_QTY),
		TRIM(AGGR_EUS_PROD_SLS_AMT), TRIM(AGGR_NT_PROD_QTY),TRIM(AGGR_NT_PROD_SLS_AMT),
		TRIM(SRC_SYS_CD), TRIM(SRC_CRT_USRID), TRIM(SRC_MOD_USRID), CRT_TMST, UPDT_TMST
FROM SMGS_V.EUSS_SUB_NATIONAL_SALES ;

.IF ERRORCODE >0 THEN .EXIT ERRORCODE ;
.LOGOFF ;
.QUIT;
EOI
if [ "$?" != "0" ] ; then

echo "[ERR] BTEQ ${SQL_FILE_NAME} Unsuccessful.." >> ${AUDIT_LOG}

exit 2
fi
#===================================================================================
# Copy file into S3
#===================================================================================
/usr/local/bin/aws s3 cp "${Target_File}" $S3_LOCATION_LANDING
rm ${Target_File}
MASTER_END_TIME2=`date +"%Y-%m-%d %T"`
former_second2=$(date --date="$MASTER_START_TIME" +%s)
later_second2=$(date --date="$MASTER_END_TIME2" +%s)
echo $((later_second2-former_second2)) > $TEMP_FILE_PATH/GBI_DUKE_TD_S3_GHX_elap_time_2
MASTER_ELAPSED_TIME2=`cat $TEMP_FILE_PATH/GBI_DUKE_TD_S3_GHX_elap_time_2`

#===================================================================================
# Updating ETL_RUN_CNTL with script status 
#===================================================================================
Sql_Status=$?

if [ $Sql_Status -eq 0 ]
                                then
                                                psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='SUCCESSFUL',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$SCRIPT_NM' and sts='IN PROGRESS';commit;";
                                                                echo "$SCRIPT_NM Completed Successfully" | mailx -s "$SCRIPT_NM Status" $EMAIL_LIST
                                else
                                                psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='FAILED',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$SCRIPT_NM' and sts='IN PROGRESS';commit;";
                                                                echo "$SCRIPT_NM Failed" | mailx -s "$SCRIPT_NM Status" $EMAIL_LIST
fi             

rm -f ${TEMP_FILE_PATH}/${SCRIPT_NM}_*
export ENDDATE=$(date +"%m-%d-%Y")
echo -e "\n\n\n[INFO] Script execution completed at $(date +"%m-%d-%Y %T")." >> ${AUDIT_LOG}


