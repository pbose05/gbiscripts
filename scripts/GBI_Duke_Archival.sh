#!/bin/bash
#
# Copyright: Copyright © 2017
# This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
# form by any means or for any purpose without the express written permission of Johnson & Johnson.
#
#================================================================================================
#FILE           : GBI_Duke_Archival.sh
#DESCRIPTION	: Archival of all files in Landing after Ingestion to Staging layer
#AUTHOR     	: Arijeet Mukherjee
#VERSION    	: 1.0
#CREATED    	: 23.11.2017
#REVISON		: 20.12.2017
#               1) Config file is used to pick up parameters
#				2) Logging added
#				3) Temp files deleted after archival ends
#				4) Config file path changed in unix
#				5) Added audit logging to Archival process
#================================================================================================
export ENV_FILE=/app/GBI/Duke/config/Config_Global_GBI_Duke.cfg
. ${ENV_FILE}
SCRIPT_NAME=$(basename -- "$0")
SCRIPT_NM=$(echo $SCRIPT_NAME | cut -d '.' -f1)
CURR_DATE=`date +"%Y%m%d_%H%M"`
LOG_FILE=$LOG_FILE_PATH/${SCRIPT_NM}_${CURR_DATE}.log

echo "$(date "+%F %T") Archival Starts" | tee -a $LOG_FILE

#===================================================================================
# Archival job IN PROGRESS status entered in the ETL_RUN_CTL table
#===================================================================================
MASTER_START_TIME=`date +"%Y-%m-%d %T"`
MASTER_END_TIME=`date +"%Y-%m-%d %T"`
former_seconds=$(date --date="$MASTER_START_TIME" +%s)
later_seconds=$(date --date="$MASTER_END_TIME" +%s)
echo $((later_seconds-former_seconds)) > $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time
MASTER_ELAPSED_TIME=`cat $TEMP_FILE_PATH/${SCRIPT_NM}_elap_time`

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "insert into GBI_CTL.ETL_RUN_CNTL values(DEFAULT,0,CURRENT_DATE,'$SCRIPT_NM','$MASTER_START_TIME','$MASTER_END_TIME','IN PROGRESS','$MASTER_ELAPSED_TIME',current_user,'$MASTER_START_TIME',current_user,'$MASTER_END_TIME');commit;";

#===================================================================================
# Ingestion parameter table loaded into an array for reading it and assigning values
#===================================================================================

Ingest_mstr_tbl=$(psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "select file_nm,file_src,archive_path from GBI_CTL.ingest_param where act_in='Y' and trunc_in='Y'";)

#===================================================================================
# Store the output from the ingest_param table to a temporary file temp.txt
#===================================================================================

echo "${Ingest_mstr_tbl}" >  $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt | tail -n +1 > $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt
#===================================================================================
# Store the File names in the a temporary file file_name.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f1 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_name.txt

#===================================================================================
# Store the File source path in S3 in the a temporary file file_source.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f2 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_source.txt

#===================================================================================
#Exception Handling when one of the files do not come the process continues
#===================================================================================
for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_source.txt)
do
        echo "/usr/local/bin/aws s3 ls $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_check

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name.txt)
do
        echo "$field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_check

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_check $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_check > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_list

sed -e 's/$/ |head | rev | cut -f1 -d'"' '"' | rev/' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_list > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_source.sh

for x in `sh $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_source.sh`
	do
			if [ -z $x ] #checking null
			then
				echo "$x does not exist"
				exit 1
			else
				echo "$x exist"
			fi
	done > $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_exist 

cat $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_exist | cut -d ' ' -f1 > $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_filter

fgrep -f $TEMP_FILE_PATH/${SCRIPT_NM}_source_files_filter $TEMP_FILE_PATH/${SCRIPT_NM}_temp.txt > $TEMP_FILE_PATH/${SCRIPT_NM}_dump_temp.txt

\cp $TEMP_FILE_PATH/${SCRIPT_NM}_dump_temp.txt $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt

#===================================================================================
# Store the File names in the a temporary file file_name.txt again 
# after exception handling
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f1 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_name.txt

#===================================================================================
# Store the File source path in S3 in the a temporary file file_source.txt again 
# after exception handling
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f2 >  $TEMP_FILE_PATH/${SCRIPT_NM}_file_source.txt


#===================================================================================
# Store the Archive path in S3 in the a temporary file file_tgt.txt
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_temp2.txt | cut -d '|' -f3 >  $TEMP_FILE_PATH/${SCRIPT_NM}_archive_path.txt

#===================================================================================
# Loop the contents of the file_name.txt temp file to generate another temp file  
# required for creation of the generic archival script
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name.txt)
do
        echo "$field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script

#===================================================================================
# Loop the contents of the file_source.txt temp file to generate another temp file  
# required for creation of the generic archival script
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_source.txt)
do
        echo "/usr/local/bin/aws s3 cp $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_script

#===================================================================================
# Loop the contents of the GBI_DUKE_archive_path.txt temp file to generate another temp file  
# required for creation of the generic archival script
#===================================================================================

for field in $(cat $TEMP_FILE_PATH/${SCRIPT_NM}_archive_path.txt)
do
        echo " $field"
done > $TEMP_FILE_PATH/${SCRIPT_NM}_archive_path

#===================================================================================
# Capturing the name of the files only without the type(.csv)  
#===================================================================================

cat $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script | cut -d '.' -f1 > $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_without_type

#===================================================================================
# Adding timestamp at the end of files 
#===================================================================================

sed -i -e "s/$/_$(date +"%Y%m%d_%H%M")/"  $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_without_type

sed -i -e "s/$/.csv/" $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_without_type

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_archive_path $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_without_type > $TEMP_FILE_PATH/${SCRIPT_NM}_archive_path_with_file_name

#===================================================================================
# Combine all the temp files created in the above steps for creation of the 
# generic archival script
#===================================================================================

paste -d '\0' $TEMP_FILE_PATH/${SCRIPT_NM}_file_source_script $TEMP_FILE_PATH/${SCRIPT_NM}_file_name_script $TEMP_FILE_PATH/${SCRIPT_NM}_archive_path_with_file_name > $TEMP_FILE_PATH/${SCRIPT_NM}_archival_script.sh

#===================================================================================
# Execute the script to archive files from Landing to Archives in S3
#===================================================================================
MASTER_END_TIME2=`date +"%Y-%m-%d %T"`
former_second2=$(date --date="$MASTER_START_TIME" +%s)
later_second2=$(date --date="$MASTER_END_TIME2" +%s)
echo $((later_second2-former_second2)) > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time_2
MASTER_ELAPSED_TIME2=`cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time_2`

sh $TEMP_FILE_PATH/${SCRIPT_NM}_archival_script.sh &>> $LOG_FILE
Archival_Status=$?

if [ $Archival_Status -eq 0 ]
		then
			psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='SUCCESSFUL',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$SCRIPT_NM' and sts='IN PROGRESS';commit;";
				echo "$SCRIPT_NM Completed Successfully" | mailx -s "$SCRIPT_NM Status" $EMAIL_LIST
		else
			psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='FAILED',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$SCRIPT_NM' and sts='IN PROGRESS';commit;";
				echo "$SCRIPT_NM Failed" | mailx -s "$SCRIPT_NM Status" $EMAIL_LIST
fi	
#===================================================================================
# Delete the temporary files created during the Archival process run
#===================================================================================
if [ $Archival_Status -eq 0 ]
		then
			rm -f ${TEMP_FILE_PATH}/${SCRIPT_NM}_*

			echo "$(date "+%F %T") Archival Ends" | tee -a $LOG_FILE
else
			rm -f ${TEMP_FILE_PATH}/${SCRIPT_NM}_*

			echo "$(date "+%F %T") Archival Ends" | tee -a $LOG_FILE
			exit 1
fi