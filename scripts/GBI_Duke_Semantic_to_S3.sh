#!/bin/bash
#
# Copyright: Copyright © 2017
# This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
# form by any means or for any purpose without the express written permission of Johnson & Johnson.
#
#================================================================================================
#FILE           : GBI_Duke_Semantic_to_S3.sh
#DESCRIPTION	: Load Aggregate Market Share data for each franchise to S3
#AUTHOR     	: Arijeet Mukherjee
#VERSION    	: 1.0
#CREATED    	: 04.01.2018
#REVISON		: 04.01.2018
#================================================================================================
export ENV_FILE=/app/GBI/Duke/config/Config_Global_GBI_Duke.cfg
. ${ENV_FILE}
CURR_DATE=`date +"%Y%m%d_%H%M"`
FRANCHISE_IDENTIFIER=$1
LOG_FILE=$LOG_FILE_PATH/${FRANCHISE_IDENTIFIER}_${CURR_DATE}.log

echo "$(date "+%F %T") Semnatic layer to S3 Load Starts" | tee -a $LOG_FILE

#===================================================================================
# Semantic to S3 job IN PROGRESS status entered in the ETL_RUN_CTL table
#===================================================================================
MASTER_START_TIME=`date +"%Y-%m-%d %T"`
MASTER_END_TIME=`date +"%Y-%m-%d %T"`
former_seconds=$(date --date="$MASTER_START_TIME" +%s)
later_seconds=$(date --date="$MASTER_END_TIME" +%s)
echo $((later_seconds-former_seconds)) > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_elap_time
MASTER_ELAPSED_TIME=`cat $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_elap_time`

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "insert into GBI_CTL.ETL_RUN_CNTL values(DEFAULT,0,CURRENT_DATE,'$FRANCHISE_IDENTIFIER','$MASTER_START_TIME','$MASTER_END_TIME','IN PROGRESS','$MASTER_ELAPSED_TIME',current_user,'$MASTER_START_TIME',current_user,'$MASTER_END_TIME');commit;";

#===================================================================================
# Copy the history files from S3 current location to S3 history location
#===================================================================================

/usr/local/bin/aws s3 ls $S3_LOCATION_CURRENT | grep $FRANCHISE_IDENTIFIER | awk '{print $4}' > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name

cat $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name | wc -l > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_file_count

for field in $(cat $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name)
do
        echo "/usr/local/bin/aws s3 cp $S3_LOCATION_CURRENT$field"
done > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_script.txt

for field in $(cat $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name)
do
        echo "/usr/local/bin/aws s3 rm $S3_LOCATION_CURRENT$field"
done > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_script_delete.sh

file_count=`cat $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_file_count`

if [ $file_count -gt 0 ]
then
	cat $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name | cut -d '.' -f1 > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_without_type
	sed -e "s/$/_$(date +"%Y%m%d_%H%M")/" $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_without_type > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_tmst
	sed -i -e "s/$/.csv/" $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_tmst
	
	echo " $S3_LOCATION_HISTORY" > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_his_loc.txt

	paste -d '\0' $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_script.txt $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_his_loc.txt $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_tmst > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_script_final.sh
	
	sh $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_script_final.sh &>> $LOG_FILE
	hist_arch_status=$?
	
	if [ $hist_arch_status -eq 0 ]
		then
			echo "Current file for $FRANCHISE_IDENTIFIER moved to history location $S3_LOCATION_HISTORY successfully" | tee -a ${LOG_FILE}
		else
			echo "Current file for $FRANCHISE_IDENTIFIER movement to history location $S3_LOCATION_HISTORY failed" | tee -a ${LOG_FILE}
	fi
#===================================================================================
# Delete the current files from S3 current location
#===================================================================================	
	sh $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_curr_file_name_script_delete.sh &>> $LOG_FILE
		curr_del_status=$?
	
	if [ $curr_del_status -eq 0 ]
		then
			echo "Current file for $FRANCHISE_IDENTIFIER deleted from location $S3_LOCATION_CURRENT successfully" | tee -a ${LOG_FILE}
		else
			echo "Current file for $FRANCHISE_IDENTIFIER deletion from location $S3_LOCATION_CURRENT failed" | tee -a ${LOG_FILE}
	fi
else
	echo "No files for $FRANCHISE_IDENTIFIER exist in $S3_LOCATION_CURRENT location" | tee -a ${LOG_FILE}
	echo "No files for $FRANCHISE_IDENTIFIER exist in $S3_LOCATION_CURRENT location" | mailx -s "$FRANCHISE_IDENTIFIER Status" $EMAIL_LIST
	continue
fi

#===================================================================================
# Unload table to EC2 instance based on the FRANCHISE_IDENTIFIER
#===================================================================================

if [ "${FRANCHISE_IDENTIFIER}" = 'GBI_SEM_AGGR_MKT_SHR_ETH' ]
	then
		ETH_GHX="GBI_SEM_AGGR_MKT_SHR_GHX"
		ETH_DRG="GBI_SEM_AGGR_MKT_SHR_DRG"
		psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "\copy (select * from gbi_sem.agg_mkt_shr where crtd_by in ('$ETH_GHX','$ETH_DRG')) to '$DATA_OUT_FILE_PATH/$FRANCHISE_IDENTIFIER.csv' DELIMITER ',' CSV HEADER";
	else
		psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "\copy (select * from gbi_sem.agg_mkt_shr where crtd_by='$FRANCHISE_IDENTIFIER') to '$DATA_OUT_FILE_PATH/$FRANCHISE_IDENTIFIER.csv' DELIMITER ',' CSV HEADER";
fi

#===================================================================================
# Capturing the name of the files only without the type(.csv) and renaming file
#===================================================================================

echo $DATA_OUT_FILE_PATH/$FRANCHISE_IDENTIFIER.csv | cut -d '.' -f1 > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_file_name_without_type
 
#===================================================================================
# Adding timestamp at the end of files 
#===================================================================================
sed -i -e "s/$/_$(date +"%Y%m%d_%H%M")/"  $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_file_name_without_type

sed -i -e "s/$/.csv/" $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_file_name_without_type

#===================================================================================
# Copy the current files from EC2 instance to S3 based on the FRANCHISE_IDENTIFIER
#===================================================================================

file_name_to_archive=`cat $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_file_name_without_type`

mv $DATA_OUT_FILE_PATH/$FRANCHISE_IDENTIFIER.csv $file_name_to_archive

MASTER_END_TIME2=`date +"%Y-%m-%d %T"`
former_second2=$(date --date="$MASTER_START_TIME" +%s)
later_second2=$(date --date="$MASTER_END_TIME2" +%s)
echo $((later_second2-former_second2)) > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_elap_time_2
MASTER_ELAPSED_TIME2=`cat $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_elap_time_2`

/usr/local/bin/aws s3 cp "$file_name_to_archive" $S3_LOCATION_CURRENT &>> $LOG_FILE
Current_Load_Status=$?

if [ $Current_Load_Status -eq 0 ]
		then
			psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='SUCCESSFUL',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$FRANCHISE_IDENTIFIER' and sts='IN PROGRESS';commit;";
				echo "Files for $FRANCHISE_IDENTIFIER copied to $S3_LOCATION_CURRENT location Successfully" | mailx -s "$FRANCHISE_IDENTIFIER Status" $EMAIL_LIST
		else
			psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='FAILED',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$FRANCHISE_IDENTIFIER' and sts='IN PROGRESS';commit;";
				echo "File copy for $FRANCHISE_IDENTIFIER to $S3_LOCATION_CURRENT location Failed" | mailx -s "$FRANCHISE_IDENTIFIER Status" $EMAIL_LIST
fi	
	
#===================================================================================
# Delete the temporary files created during the Semnatic layer to S3 Load process
#===================================================================================

sed -e 's/^/rm -f /' $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_file_name_without_type > $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_orig_file_ec2_delete.sh

sh $TEMP_FILE_PATH/${FRANCHISE_IDENTIFIER}_orig_file_ec2_delete.sh

rm -f ${TEMP_FILE_PATH}/${FRANCHISE_IDENTIFIER}_*

echo "$(date "+%F %T") Semnatic layer to S3 Load Ends" | tee -a $LOG_FILE