#!/bin/bash
#
# Copyright: Copyright © 2017
# This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
# form by any means or for any purpose without the express written permission of Johnson & Johnson.
#
#==============================================================================================
#FILE           : M_Data_Processing.sh
#DESCRIPTION	: Master script for execution of SQLs
#AUTHOR     	: Arijeet Mukherjee
#VERSION    	: 1.0
#CREATED    	: 01.12.2017
#REVISON		: 22.12.2017
#				1) Elapsed time for master job updated upon completion in ETL_RUN_CNTL table
#				2) Master job existence check in the GBI_CTL.JOB_PROFILE table  
#==============================================================================================

#===================================================================================
# Config file passes all the variables used in the script
#===================================================================================

export ENV_FILE=/app/GBI/Duke/config/Config_Global_GBI_Duke.cfg
. ${ENV_FILE}
#SCRIPT_NAME=$(basename -- "$0")
#SCRIPT_NM=$(echo $SCRIPT_NAME | cut -d '.' -f1)
MASTER_JOB_NAME=$1
CURR_DATE=`date +"%Y%m%d_%H%M"`
LOG_FILE=$LOG_FILE_PATH/${MASTER_JOB_NAME}_${CURR_DATE}.log

echo "$(date "+%F %T") ${MASTER_JOB_NAME} Execution Starts" | tee -a ${LOG_FILE}

#===================================================================================
# Check for duplicate run_seq_no for a master job when the audit logging starts
#===================================================================================

check_dupl_run_seq_no=$(psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "select count(*) from gbi_ctl.job_profile where mstr_job_nm='$MASTER_JOB_NAME' group by run_seq_no having count(*) > 1";)

echo "${check_dupl_run_seq_no}" >  $TEMP_FILE_PATH/${MASTER_JOB_NAME}_check_dupl_run_seq_no.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${MASTER_JOB_NAME}_check_dupl_run_seq_no.txt

dupl_run_seq_no=`cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_check_dupl_run_seq_no.txt`

if [[ -z "$dupl_run_seq_no" ]]
then
	continue
else
	echo "Duplicate found in Run Sequence Number of gbi_ctl.job_profile table.. Please correct it!! Count is $dupl_run_seq_no" | tee -a ${LOG_FILE}
	echo "Duplicate found in Run Sequence Number of gbi_ctl.job_profile table.. Please correct it" | mailx -s "$MASTER_JOB_NAME  Status" $EMAIL_LIST
	exit 1
fi

#===================================================================================
# Check for correct master job name entry in the tidal job scheduling
#===================================================================================
check_correct_mstr_job_nm=$(psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "select count(*) from gbi_ctl.job_profile where mstr_job_nm='$MASTER_JOB_NAME'";)

if [ $check_correct_mstr_job_nm -eq 0 ]
then
	echo "$MASTER_JOB_NAME MASTER JOB does not exist in the GBI_CTL.JOB_PROFILE table. Please correct the TIDAL job" | tee -a ${LOG_FILE}
	echo "$MASTER_JOB_NAME MASTER JOB does not exist in the GBI_CTL.JOB_PROFILE table. Please correct the TIDAL job" | mailx -s "$MASTER_JOB_NAME  Status" $EMAIL_LIST
	exit 1
else
	continue
fi
	
#===================================================================================
# Master job IN PROGRESS status entered in the ETL_RUN_CTL table
#===================================================================================
MASTER_START_TIME=`date +"%Y-%m-%d %T"`
MASTER_END_TIME=`date +"%Y-%m-%d %T"`
former_seconds=$(date --date="$MASTER_START_TIME" +%s)
later_seconds=$(date --date="$MASTER_END_TIME" +%s)
echo $((later_seconds-former_seconds)) > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time
MASTER_ELAPSED_TIME=`cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time`

psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "insert into GBI_CTL.ETL_RUN_CNTL values(DEFAULT,0,CURRENT_DATE,'$MASTER_JOB_NAME','$MASTER_START_TIME','$MASTER_END_TIME','IN PROGRESS','$MASTER_ELAPSED_TIME',current_user,'$MASTER_START_TIME',current_user,'$MASTER_END_TIME');commit;";

#===================================================================================
# Hitting the JOB_PROFILE table with the MASTER JOB name
#===================================================================================
sql_run_master=$(psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "select child_job_nm,job_id from gbi_ctl.job_profile where mstr_job_nm='$MASTER_JOB_NAME' and is_act='Y' ORDER BY run_seq_no";)

echo "${sql_run_master}" >  $TEMP_FILE_PATH/${MASTER_JOB_NAME}_temp.txt

cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_temp.txt | tail -n +1 > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_temp2.txt

cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_temp2.txt | cut -d '|' -f1 >  $TEMP_FILE_PATH/${MASTER_JOB_NAME}_child_job_name.txt

cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_temp2.txt | cut -d '|' -f2 >  $TEMP_FILE_PATH/${MASTER_JOB_NAME}_job_id.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${MASTER_JOB_NAME}_child_job_name.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${MASTER_JOB_NAME}_job_id.txt

paste $TEMP_FILE_PATH/${MASTER_JOB_NAME}_job_id.txt $TEMP_FILE_PATH/${MASTER_JOB_NAME}_child_job_name.txt > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_job_id_name

#===================================================================================
# Pass the etl_run_id from etl_run_ctl table to the etl_aud_dtl table
#===================================================================================

run_etl_cntl=$(psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -t -c "select etl_run_id from gbi_ctl.etl_run_cntl where mstr_job_nm='$MASTER_JOB_NAME' and sts='IN PROGRESS' ORDER BY etl_run_id desc limit 1";)

echo "${run_etl_cntl}" >  $TEMP_FILE_PATH/${MASTER_JOB_NAME}_etl_run_id.txt

sed -i -e 's/ //g' $TEMP_FILE_PATH/${MASTER_JOB_NAME}_etl_run_id.txt

audit_etl_run_id=`cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_etl_run_id.txt`

#=====================================================================================
# Pass the job_id and child_job_name to populate the etl_run_dtl table for child jobs
#=====================================================================================

		cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_job_id_name | while read job_id_nm child_job_name
			do	
				CHILD_START_TIME=`date +"%Y-%m-%d %T"`
				##Loop_Cnt =1
				psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -a -f $SQL_FILE_PATH/$child_job_name &>> $LOG_FILE
				Script_Status=$?
				if [ $Script_Status -eq 0 ]
				  then
					echo "$(date "+%F %T") INFO: ${child_job_name} SQL has been successfully executed." | tee -a ${LOG_FILE}
					CHILD_END_TIME=`date +"%Y-%m-%d %T"`
					former_seconds=$(date --date="$CHILD_START_TIME" +%s)
					later_seconds=$(date --date="$CHILD_END_TIME" +%s)
					echo $((later_seconds-former_seconds)) > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time_sql
					CHILD_ELAPSED_TIME=`cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time_sql`
					psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "insert into GBI_CTL.ETL_AUD_DTL values($audit_etl_run_id,$job_id_nm,'$CHILD_START_TIME','$CHILD_END_TIME','SUCCESSFUL','$CHILD_ELAPSED_TIME',current_user,'$CHILD_START_TIME',current_user,'$CHILD_END_TIME');commit;";
					echo $Script_Status > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_return_status
				else
					echo "$(date "+%F %T") FAILURE: ${child_job_name} SQL has FAILED." | tee -a ${LOG_FILE}
					echo "$child_job_name Failed" | mailx -s "$child_job_name Failed" $EMAIL_LIST
					CHILD_END_TIME=`date +"%Y-%m-%d %T"`
					former_seconds=$(date --date="$CHILD_START_TIME" +%s)
					later_seconds=$(date --date="$CHILD_END_TIME" +%s)
					echo $((later_seconds-former_seconds)) > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time_sql
					CHILD_ELAPSED_TIME=`cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time_sql`
					psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "insert into GBI_CTL.ETL_AUD_DTL values($audit_etl_run_id,$job_id_nm,'$CHILD_START_TIME','$CHILD_END_TIME','FAILED','$CHILD_ELAPSED_TIME',current_user,'$CHILD_START_TIME',current_user,'$CHILD_END_TIME');commit;";
					echo $Script_Status > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_return_status
					
					break	
				fi
		##Loop_Cnt =`$Loop_Cnt + 1`
		done
MASTER_END_TIME2=`date +"%Y-%m-%d %T"`
former_second2=$(date --date="$MASTER_START_TIME" +%s)
later_second2=$(date --date="$MASTER_END_TIME2" +%s)
echo $((later_second2-former_second2)) > $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time_2
MASTER_ELAPSED_TIME2=`cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_elap_time_2`
#=====================================================================================
# Populate the completion status in the etl_run_dtl table for the master job
#=====================================================================================		
		
		return_status=`cat $TEMP_FILE_PATH/${MASTER_JOB_NAME}_return_status`
		if [[ "$return_status" -eq 0 ]]
			then
				psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='SUCCESSFUL',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$MASTER_JOB_NAME' and sts='IN PROGRESS';commit;";
				echo "$MASTER_JOB_NAME Completed Successfully" | mailx -s "$MASTER_JOB_NAME Status" $EMAIL_LIST
		else
				psql --host=$HOST --port=$PORT --username=$USERNAME --dbname=$DBNAME -c "update GBI_CTL.ETL_RUN_CNTL set sts='FAILED',etl_run_end_dttm='$MASTER_END_TIME2',elap_time='$MASTER_ELAPSED_TIME2' where mstr_job_nm='$MASTER_JOB_NAME' and sts='IN PROGRESS';commit;";
				echo "$MASTER_JOB_NAME Failed" | mailx -s "$MASTER_JOB_NAME Status" $EMAIL_LIST
		fi
         # _LoopCnt= $(_LoopCnt+1)
#===================================================================================
# Delete the temporary files
#===================================================================================	
if [[ "$return_status" -eq 0 ]]
	 then
		rm -f ${TEMP_FILE_PATH}/${MASTER_JOB_NAME}_*
		echo "$(date "+%F %T") ${MASTER_JOB_NAME} Execution Ends" | tee -a ${LOG_FILE}
else
		rm -f ${TEMP_FILE_PATH}/${MASTER_JOB_NAME}_*
		echo "$(date "+%F %T") ${MASTER_JOB_NAME} Execution Ends" | tee -a ${LOG_FILE}
		exit 1
fi