/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin;

delete from gbi_cdh.prod_sclg_fct where fran='CSS' and soc_setng = 'HOSP' AND SCLG_TYP = 'AVG36' and src_sys_cd = 'DRG';

commit;

INSERT INTO gbi_cdh.prod_sclg_fct
(
  fran  ,
  soc_setng,
  scl_lvl ,
  sclg_typ ,
  fisc_mo_id ,
  mo_idx ,
  eus_sls_fct ,
  eus_unt_fct ,
  nts_sls_fct ,
  nts_unt_fct ,
  src_sys_cd ,
  Crtd_By,
  updt_by,
  crt_dttm ,
  updt_dttm
)

select
	'CSS' as fran,
	'HOSP' as soc_setng,
	scl_lvl,
	'AVG36' as sclg_typ,
	fisc_mo_id,
	mo_idx,
	(avg(eus_sls_fct) over (partition by scl_lvl  order by mo_idx desc rows 36 preceding)) as eus_sls_fct,
	(avg(eus_unt_fct) over (partition by scl_lvl  order by mo_idx desc rows 36 preceding)) as eus_unt_fct,
	(avg(nts_sls_fct) over (partition by scl_lvl  order by mo_idx desc rows 36 preceding)) as nts_sls_fct,
	(avg(nts_unt_fct) over (partition by scl_lvl  order by mo_idx desc rows 36 preceding)) as nts_unt_fct,
	'DRG' as src_sys_cd,
	'GBI_CDH_AVG_SCALING_DRG_CSS' as Crtd_By,
	'GBI_CDH_AVG_SCALING_DRG_CSS' as updt_by,
	now() as crt_dttm,
	now() as updt_dttm
from
	gbi_cdh.prod_sclg_fct
	where fran='CSS'
		  and sclg_typ='MONTHLY'
		  and src_sys_cd='DRG';
		  
commit;

delete from gbi_cdh.prod_sclg_fct where fran='CSS' and soc_setng = 'HOSP' AND SCLG_TYP = 'AVG72' and src_sys_cd = 'DRG';


INSERT INTO gbi_cdh.prod_sclg_fct
(
  fran  ,
  soc_setng,
  scl_lvl ,
  sclg_typ ,
  fisc_mo_id ,
  mo_idx ,
  eus_sls_fct ,
  eus_unt_fct ,
  nts_sls_fct ,
  nts_unt_fct ,
  src_sys_cd ,
  Crtd_By,
  updt_by,
  crt_dttm ,
  updt_dttm
)

select
	'CSS' as fran,
	'HOSP' as soc_setng,
	scl_lvl,
	'AVG72' as sclg_typ,
	b.fisc_mo_id,
	b.idx_nbr as mo_idx,
	eus_sls_fct,
	eus_unt_fct,
	nts_sls_fct,
	nts_unt_fct,
	'DRG' as src_sys_cd,
	'GBI_CDH_AVG_SCALING_DRG_CSS' as Crtd_By,
	'GBI_CDH_AVG_SCALING_DRG_CSS' as updt_by,
	now() as crt_dttm,
	now() as updt_dttm
from
	(select 
		scl_lvl,
		avg(eus_sls_fct) as eus_sls_fct,
		avg(eus_unt_fct) as eus_unt_fct,
		avg(nts_sls_fct) as nts_sls_fct,
		avg(nts_unt_fct) as nts_unt_fct
	from
		gbi_cdh.prod_sclg_fct
	where fran='CSS'
		  and sclg_typ='MONTHLY'
		  and src_sys_cd='DRG'
		  and mo_idx between 1 and 72
	group by scl_lvl) a
	inner join
	(select * from gbi_cdh.dim_time_rolling where src_sys_cd='CSS') b
	on 1=1;
		  
commit;
end;