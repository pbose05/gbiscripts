/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

begin;

delete from gbi_cdh.dim_time_rolling where src_sys_cd='ETH' ;

INSERT INTO gbi_cdh.dim_time_rolling(
            idx_nbr, mo_nm, fisc_mo_id, cal_qtr_nm, roll_qtr_id, rqtr_nbr, 
            calqtr_nbr, yr_nbr, yr_roll_nbr, src_sys_cd, crt_dttm, updt_dttm)
SELECT
	idx_nbr,
	mo_nm,
	fisc_mo_id,
	cal_qtr_nm,
	case 
	when idx_nbr%3 = 1 then fisc_mo_id
	when idx_nbr%3 = 2 then (case when (fisc_mo_id + 1)%100 > 12 then (fisc_mo_id + 1) + 88 else (fisc_mo_id + 1)end)
	when idx_nbr%3 = 0 then (case when (fisc_mo_id + 2)%100 > 12 then (fisc_mo_id + 2) + 88 else (fisc_mo_id + 2)end)
	end roll_qtr_id,
	case when (mod(idx_nbr,3)=0) then ceil(idx_nbr/3) else ceil(idx_nbr/3)+1 end rqtr_nbr,
	case
		when qtr_offset = 0 then (idx_nbr+2)/3
		when qtr_offset = 1 then (idx_nbr+1)/3
		when qtr_offset = 2 then (idx_nbr)/3
	end calqtr_nbr,
	yr_nbr,
	(idx_nbr-1)/12+1 yr_roll_nbr,
	src_sys_cd,
	Crt_Dttm,
	Updt_Dttm
FROM
(
	SELECT 
		ROW_NUMBER() OVER(ORDER BY MO_ID DESC) idx_nbr, 
		YR_ID ||' - '|| MO_OF_THE_YR_DESC mo_nm, 
		MO_ID fisc_mo_id,
		YR_ID ||'-'|| QTR_OF_THE_YR cal_qtr_nm,
		QTR_OFFSET,
	    MAX_YR%YR_ID+1 yr_nbr, 
	    'ETH' src_sys_cd, 
	    current_timestamp Crt_Dttm, 
	    current_timestamp Updt_Dttm
	FROM
	(
		SELECT DISTINCT YR_ID, QTR_OF_THE_YR, FISCAL_DAY.MO_ID, T1.MAX_YR, T1.QTR_OFFSET, FISCAL_DAY.MO_OF_THE_YR_DESC 
		FROM gbi_cdh.FISCAL_DAY, 
		(
			SELECT (cast(extract (year from cast (GRP_ATTR1_VAL as date)) as integer) * 100 ) + cast(extract (month from cast (GRP_ATTR1_VAL as date)) as integer) MO_ID,
					cast(extract (year from cast (GRP_ATTR1_VAL as date)) as integer) MAX_YR,
					cast(extract (month from cast (GRP_ATTR1_VAL as date)) as integer)%3 QTR_OFFSET
			FROM GBI_CTL.LKUP_PARAM_GRP 
			WHERE GRP_TYP_CD='DATA_MONTH' 
			AND SRC_SYS_CD='EUS'
			AND GRP_CD='CURR_MTH'
		) T1
		WHERE FISCAL_DAY.MO_ID  <= T1.MO_ID
		ORDER BY MO_ID DESC
		LIMIT 74
	) Q1
) Q2;


commit;
end;