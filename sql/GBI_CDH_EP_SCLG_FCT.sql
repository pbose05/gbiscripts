/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on
begin;
delete from gbi_cdh.prod_sclg_fct where fran ='CSS' and  crtd_by='GBI_CDH_EP_SCLG_FCT';

insert into gbi_cdh.prod_sclg_fct 
select distinct
'CSS' as fran,'HOSP' as soc_setng,
(brand_family ||'|'|| brand ||'|'|| grp ||'|'|| sub_grp ||'|'|| size), 
'MONTHLY' as sclg_typ, 
fisc_mo_id, 
cast(right(cast(fisc_mo_id as character varying(6)) , 2) as integer) as mo_idx,
scld_fractn as eus_sls_fct, 
scld_fractn as eus_unt_fct, 
0 as nts_sls_fct, 
0 as nts_unt_fct, 
src_sys_cd, 
'GBI_CDH_EP_SCLG_FCT' as crtd_by, 
'GBI_CDH_EP_SCLG_FCT' as updt_by, 
now() ,
now()
from gbi_wrk.ep_prod_sclg ;


end;
