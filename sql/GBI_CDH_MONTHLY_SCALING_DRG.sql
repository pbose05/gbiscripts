/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

begin;

delete from gbi_cdh.prod_sclg_fct where fran = 'ETH' and soc_setng = 'HOSP' and sclg_typ = 'MONTHLY' and src_sys_cd = 'DRG';

commit;

INSERT INTO gbi_cdh.prod_sclg_fct
(
  fran  ,
  soc_setng,
  scl_lvl ,
  sclg_typ ,
  fisc_mo_id ,
  mo_idx ,
  eus_sls_fct ,
  eus_unt_fct ,
  nts_sls_fct ,
  nts_unt_fct ,
  src_sys_cd ,
  Crtd_By,
  updt_by,
  crt_dttm ,
  updt_dttm
)

select 'ETH' AS FRANCHISE , 
'HOSP' AS SOC_SETNG,
INT_SLS.SCLG_LVL_VAL ,'MONTHLY' AS SCLG_TYP,  INT_SLS.FISC_MO_ID, EXT_SLS.IDX_NBR, 
CASE WHEN EXT_SLS.AGGR_DLR_VAL = 0 THEN 1 ELSE (INT_SLS.AGGR_EUS_PROD_SLS_AMT /EXT_SLS.AGGR_DLR_VAL) END EUS_SLS_FCT,
CASE WHEN EXT_SLS.AGGR_DLR_VAL = 0 THEN 1 ELSE (INT_SLS.AGGR_EUS_PROD_QTY /EXT_SLS.AGGR_UNT_NBR ) END EUS_UNIT_FCT,
CASE WHEN EXT_SLS.AGGR_DLR_VAL = 0 THEN 1 ELSE (INT_SLS.AGGR_NT_PROD_SLS_AMT /EXT_SLS.AGGR_DLR_VAL) END NTS_SLS_FCT,
CASE WHEN EXT_SLS.AGGR_DLR_VAL = 0 THEN 1 ELSE (INT_SLS.AGGR_NT_PROD_QTY /EXT_SLS.AGGR_UNT_NBR ) END NTS_UNIT_FCT,
'DRG' SRC_SYS_CD,
'MKT_SCLG_FCT_DRG' AS Crtd_By,
'MKT_SCLG_FCT_DRG' AS updt_by,
  NOW() AS Crt_Dttm ,
  NOW() AS Updt_Dttm 
 FROM

(
SELECT SCLG_LVL_VAL, 
FISC_MO_ID, 
SUM(AGGR_EUS_PROD_QTY) AS AGGR_EUS_PROD_QTY, 
SUM(AGGR_EUS_PROD_SLS_AMT) AS AGGR_EUS_PROD_SLS_AMT ,
SUM(AGGR_NT_PROD_QTY) AS AGGR_NT_PROD_QTY,
SUM(AGGR_NT_PROD_SLS_AMT) AS AGGR_NT_PROD_SLS_AMT
 FROM GBI_STG.ETH_INTERNAL_SALES_DRG A
INNER JOIN gbi_cdh.GBI_PRODUCT_DIM B
ON trim(upper(A.PROD_BASE_CD))= trim(upper(B.PRODUCT_CODE ))
WHERE UPPER(B.COMPANY_GROUPING)='ETHICON'
 and B.manufacturer NOT IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2='MONTHLY_SCALING' and rule_seq='1')
AND B.EUS='1'
AND B.platform not in (select distinct platform from gbi_cdh.gbi_product_dim where ghx = '1')
GROUP BY SCLG_LVL_VAL, 
FISC_MO_ID
) INT_SLS

inner join

(
SELECT 
  T.SCLG_LVL_VAL,
  T.FISC_MO_ID,
  CASE WHEN T.SRC_SYS_CD = 'GHX' AND C.MIN_IDX_NBR = 2 THEN T.IDX_NBR-1
  WHEN T.SRC_SYS_CD = 'GHX' AND C.MIN_IDX_NBR = 3 THEN T.IDX_NBR-2
  ELSE T.IDX_NBR END AS IDX_NBR,
  T.AGGR_DLR_VAL,
  T.AGGR_UNT_NBR 
FROM (
 SELECT 
  SCLG_LVL_VAL, 
  D.FISC_MO_ID AS FISC_MO_ID, 
  D.IDX_NBR AS IDX_NBR,
  A.SRC_SYS_CD AS SRC_SYS_CD,
  SUM(SLS_AMT) AS AGGR_DLR_VAL, 
  SUM(SLS_UNIT) AS AGGR_UNT_NBR 
 FROM gbi_wrk.intrm_mthly_sls AS A 
 INNER JOIN gbi_cdh.GBI_PRODUCT_DIM AS B
 ON trim(upper(A.PROD_CD)) = trim(upper(B.product_code))
 and trim(upper(A.mfr_nm)) = trim(upper(B.manufacturer))
 INNER JOIN gbi_cdh.dim_time_rolling AS D
 ON A.FISC_MO_ID = D.FISC_MO_ID 
 WHERE UPPER(B.COMPANY_GROUPING) = 'ETHICON'
 and B.manufacturer NOT IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2='MONTHLY_SCALING' and rule_seq='1')
 AND D.SRC_SYS_CD='DRG'
 AND A.SRC_SYS_CD = 'DRG'
 AND B.drg = '1'
 AND B.platform not in (select distinct platform from gbi_cdh.gbi_product_dim where ghx = '1')
 GROUP BY SCLG_LVL_VAL, D.FISC_MO_ID, D.IDX_NBR, A.SRC_SYS_CD) AS T
 CROSS JOIN 
 (SELECT MIN(IDX_NBR) MIN_IDX_NBR
 FROM gbi_wrk.intrm_mthly_sls AS A 
 INNER JOIN gbi_cdh.GBI_PRODUCT_DIM AS B
 ON A.PROD_CD = B.product_code
 INNER JOIN gbi_cdh.dim_time_rolling AS D
 ON A.FISC_MO_ID = D.FISC_MO_ID
 WHERE UPPER(B.COMPANY_GROUPING) = 'ETHICON'
 AND A.SRC_SYS_CD = 'DRG' AND D.SRC_SYS_CD='DRG'
 AND B.drg = '1') AS C
) EXT_SLS

on INT_SLS.SCLG_LVL_VAL  = EXT_SLS.SCLG_LVL_VAL
AND INT_SLS.FISC_MO_ID  = EXT_SLS.FISC_MO_ID	;

COMMIT;
end;