/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on
begin;

delete from gbi_cdh.prod_sclg_fct where fran = 'CSS' and soc_setng = 'HOSP' and sclg_typ = 'MONTHLY' and src_sys_cd = 'DRG';

commit;


WITH 
internal_sales as
(
select 
	coalesce(prod.sclg_lvl,'NA') as sclg_lvl,
	int_sls.fisc_mo_yr,
	--prod.jnj_code,
	--prod.prod_cd,
	sum(int_sls.aggr_eus_prod_qty) as aggr_eus_prod_qty,
	sum(int_sls.aggr_eus_prod_sls_amt) as aggr_eus_prod_sls_amt
from 
	(
	select 
	trim(upper(product_code)) as prod_cd, 
	cast(fiscal_year_number as integer)*100+cast(fiscal_month as integer) as fisc_mo_yr,
	company_code, 
  	sum(eaches_qty) as aggr_eus_prod_qty, 
  	sum(sales_amt) as aggr_eus_prod_sls_amt, 
  	'CSS' as src_sys_cd
	from 
	gbi_stg.css_jpi_bwi_acct_sales
	where shpto_cot_cd in (select busn_rule_gr_3  from gbi_cdh.busn_rule where busn_rule_gr_2='JPI-BWI'
and busn_rule_gr_1='CSS' and rule_seq='1')
	and   shpto_setting_hosp_ooh not in (select busn_rule_gr_3  from gbi_cdh.busn_rule where busn_rule_gr_2='ALL'
														and busn_rule_gr_1='CSS' and rule_seq='2')
	group by 
	trim(upper(product_code)), 
	cast(fiscal_year_number as integer)*100+cast(fiscal_month as integer), 
	company_code, 
	src_sys_cd
	) int_sls
	inner join
	(select distinct
		prod_cd, 
		jnj_code, 
		sclg_lvl
	from
		gbi_wrk.intrm_prod_sclng
		where src_fran='CSS' and jnj_code<>'NA' and UPPER(comp_grpng) in (select distinct busn_rule_gr_2
from gbi_cdh.busn_rule where busn_rule = 'CSS Company Grouping' and  busn_rule_gr_1 ='CSS' and rule_seq='1')
) prod
on TRIM(UPPER(int_sls.prod_cd))=TRIM(UPPER(prod.jnj_code))
--where sclg_lvl IS NULL
group by prod.sclg_lvl,int_sls.fisc_mo_yr
),

external_sales as
(
select
	coalesce(prod.sclg_lvl,'NA') as sclg_lvl,
	to_char(ext_sls.date,'YYYYMM') as yr_month,
	t.idx_nbr,
	sum(ext_sls.units) as aggr_units,
	sum(ext_sls.revenue) as aggr_revenue
from 
	gbi_stg.drg_monthly_sales_all_franchise ext_sls
	inner join
	(select distinct
		data_src_prod_cd, 
		prod_cd, 
		jnj_code, 
		sclg_lvl,mfr_nm
	from
		gbi_wrk.intrm_prod_sclng
		where src_fran='CSS' and UPPER(comp_grpng) in (select distinct busn_rule_gr_2
from gbi_cdh.busn_rule where busn_rule = 'CSS Company Grouping' and  busn_rule_gr_1 ='CSS' and rule_seq='1')
) prod
on TRIM(UPPER(ext_sls.sku))=TRIM(UPPER(prod.data_src_prod_cd))
and CASE WHEN TRIM(UPPER(ext_sls.manufacturer))='CERENOVOUS'::TEXT THEN'CERENOVUS'::TEXT
	  ELSE TRIM(UPPER(ext_sls.manufacturer)) END = TRIM(UPPER(prod.MFR_NM))
inner join
gbi_cdh.dim_time_rolling t
on cast(to_char(ext_sls.date,'YYYYMM') as integer)= t.fisc_mo_id
where ext_sls.franchise in (select distinct busn_rule_gr_3 from gbi_cdh.busn_rule where busn_rule='external_sales'and rule_seq='1'and busn_rule_gr_1='CSS')
and prod.jnj_code<>'NA'
and t.src_sys_cd='CSS'
group by prod.sclg_lvl,to_char(ext_sls.date,'YYYYMM'), t.idx_nbr
)

INSERT INTO gbi_cdh.prod_sclg_fct
(
  fran  ,
  soc_setng,
  scl_lvl ,
  sclg_typ ,
  fisc_mo_id ,
  mo_idx ,
  eus_sls_fct ,
  eus_unt_fct ,
  nts_sls_fct ,
  nts_unt_fct ,
  src_sys_cd ,
  Crtd_By,
  updt_by,
  crt_dttm ,
  updt_dttm
)

select
	'CSS' as fran,
	'HOSP' as soc_setng,
	internal_sales.sclg_lvl,
	'MONTHLY' as sclg_typ,
	internal_sales.fisc_mo_yr,
	external_sales.idx_nbr,
	case 
		when external_sales.aggr_revenue=0 then 1
		else internal_sales.aggr_eus_prod_sls_amt/external_sales.aggr_revenue 
	end as eus_sls_fct,	
	case 
		when external_sales.aggr_units=0 then 1
		else internal_sales.aggr_eus_prod_qty/external_sales.aggr_units 
	end as eus_unt_fct,
	NULL as nts_sls_fct,
	NULL as nts_unt_fct,
	'DRG' as src_sys_cd,
	'GBI_CDH_MONTHLY_SCALING_DRG_CSS' as Crtd_By,
	'GBI_CDH_MONTHLY_SCALING_DRG_CSS' as updt_by,
	now() as crt_dttm,
	now() as updt_dttm
from
	internal_sales
	inner join 	external_sales
on internal_sales.sclg_lvl=external_sales.sclg_lvl
   and internal_sales.fisc_mo_yr=cast(external_sales.yr_month as integer);
   
commit;
end;