/*
* Copyright: Copyright© 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin;

delete from gbi_cdh.prod_sclg_fct where fran = 'DPS' and soc_setng = 'HOSP' and sclg_typ = 'MONTHLY' and src_sys_cd = 'DRG';

commit;

WITH 
internal_sales as
(
select 
       coalesce(prod.sclg_lvl,'NA') as sclg_lvl,
       int_sls.fisc_mo_id fisc_mo_yr,
       --prod.jnj_code,
       --prod.prod_cd,
       sum(int_sls.cnsmr_sls_unit) as aggr_eus_prod_qty,
       sum(int_sls.cnsmr_sls_amt) as aggr_eus_prod_sls_amt
from 
       gbi_wrk.intrm_int_ext_mnthly_sls_dps int_sls
       inner join
       (select distinct
              prod_cd, 
              jnj_code, 
              sclg_lvl
       from
              gbi_wrk.intrm_prod_sclng
              where src_fran='DPS' and jnj_code<>'NA' and UPPER(comp_grpng)='DEPUY SYNTHES') prod
on trim(upper(int_sls.prod_cd))=trim(upper(prod.prod_cd))
--where sclg_lvl IS NULL
group by prod.sclg_lvl,int_sls.fisc_mo_id
)
,

external_sales as
(
select
	coalesce(prod.sclg_lvl,'NA') as sclg_lvl,
	to_char(ext_sls.date,'YYYYMM') as yr_month,
	t.idx_nbr,
	sum(ext_sls.units) as aggr_units,
	sum(ext_sls.revenue) as aggr_revenue
from 
	gbi_stg.drg_monthly_sales_all_franchise ext_sls
	inner join
	(select distinct
		data_src_prod_cd, 
		prod_cd, 
		jnj_code, 
		sclg_lvl,
              mfr_nm
	from
		gbi_wrk.intrm_prod_sclng
		where src_fran='DPS' and jnj_code<>'NA' and UPPER(comp_grpng)='DEPUY SYNTHES') prod
on trim(upper(ext_sls.sku))=trim(upper(prod.data_src_prod_cd))
and trim(upper(ext_sls.manufacturer))=trim(upper(prod.mfr_nm))
inner join
gbi_cdh.dim_time_rolling t
on cast(to_char(ext_sls.date,'YYYYMM') as integer)= t.fisc_mo_id
where ext_sls.franchise in ----- Change done on 14th Feb
			(select distinct busn_rule_gr_3 from gbi_cdh.busn_rule where busn_rule='external_sales'and rule_seq='1'and busn_rule_gr_1='DPS')
and t.src_sys_cd='DPS'
group by prod.sclg_lvl,to_char(ext_sls.date,'YYYYMM'), t.idx_nbr
)

INSERT INTO gbi_cdh.prod_sclg_fct
(
  fran  ,
  soc_setng,
  scl_lvl ,
  sclg_typ ,
  fisc_mo_id ,
  mo_idx ,
  eus_sls_fct ,
  eus_unt_fct ,
  nts_sls_fct ,
  nts_unt_fct ,
  src_sys_cd ,
  Crtd_By,
  updt_by,
  crt_dttm ,
  updt_dttm
)

select
	'DPS' as fran,
	'HOSP' as soc_setng,
	internal_sales.sclg_lvl,
	'MONTHLY' as sclg_typ,
	internal_sales.fisc_mo_yr,
	external_sales.idx_nbr,
	case 
              when external_sales.aggr_revenue=0 then 1
              else internal_sales.aggr_eus_prod_sls_amt/external_sales.aggr_revenue 
       end as eus_sls_fct, 
       case 
              when external_sales.aggr_units=0 then 1
              else internal_sales.aggr_eus_prod_qty/external_sales.aggr_units 

	end as eus_unt_fct,
	NULL as nts_sls_fct,
	NULL as nts_unt_fct,
	'DRG' as src_sys_cd,
	'GBI_CDH_MONTHLY_SCALING_DRG_DPS' as Crtd_By,
	'GBI_CDH_MONTHLY_SCALING_DRG_DPS' as updt_by,
	now() as crt_dttm,
	now() as updt_dttm
from
	internal_sales
	inner join 	external_sales
on internal_sales.sclg_lvl=external_sales.sclg_lvl
   and internal_sales.fisc_mo_yr=cast(external_sales.yr_month as integer);
   
commit;

end;
