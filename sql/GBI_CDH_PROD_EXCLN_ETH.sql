/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

begin;

DELETE FROM GBI_CDH.PROD_EXCLN where FRAN_CD = 'ETH';

commit;

INSERT INTO GBI_CDH.PROD_EXCLN 
		(FRAN_CD, 
		MFR_NM, 
		PROD_CD, 
		DIV, 
		MAJ_PLTF, 
		PLTF, 
		SUB_PLTF, 
		BRND_FMLY, 
		BRND, 
		CORP, 
		OEM_MFR_NM, 
		COMP_GRPNG, 
		JNJ_CD, 
		DATA_SRC_CD,
		PROD_NM, 
		DATA_SRC, 
		OEM_REPROC, 
		MAIN_CMPTTR, 
		MATL, 
		SIZE, 
		TYPE, 
		CRTD_BY, 
		UPDT_BY, 
		CRT_DTTM, 
		UPDT_DTTM
		) 
	SELECT 
	DISTINCT 'ETH' AS FRAN_CD, 
	TRIM(MANUFACTURER) AS MFR_NM, 
	TRIM(harmonized_product_code) AS PROD_CD, 
	TRIM(DIVISION) AS DIVISION, 
	TRIM(MAJOR_PLATFORM) AS MAJOR_PLATFORM, 
	TRIM(PLATFORM) AS PLATFORM, 
	TRIM(SUB_PLATFORM) AS SUB_PLATFORM, 
	TRIM(BRAND_FAMILY) AS BRAND_FAMILY, 
	TRIM(BRAND) AS BRAND, 
	TRIM(CORPORATION) AS CORPORATION, 
	TRIM(OEM_MANUFACTURER) AS OEM_MANUFACTURER, 
	TRIM(COMPANY_GROUPING) AS COMPANY_GROUPING, 
	TRIM(JNJ_CODE) AS JNJ_CODE, 
	case when drg='1' then drg_code
	when ghx='1' then ghx_code END  AS DATA_SRC_CODE, 
	TRIM(PRODUCT_NAME) AS PRODUCT_NAME, 
	case when drg='1' then 'DRG'
	when ghx='1' then 'GHX' END AS DATA_SRC, 
	TRIM(OEM_REPROCESSED) AS OEM_REPROCESSED, 
	TRIM(MAIN_COMPETITOR) AS MAIN_COMPETITOR, 
	TRIM(MATERIAL) AS MATERIAL, 
	TRIM(SIZE) AS SIZE, 
	TRIM(TYPE) AS TYPE, 
	'GBI_CDH_PROD_EXCLN_ETH' AS CRTD_BY,
	'GBI_CDH_PROD_EXCLN_ETH' AS UPDT_BY,
	NOW() AS CRT_DTTM,
	NOW() AS UPDT_DTTM 
FROM GBI_STG.EXCLUSION_ETH;
commit;
end;