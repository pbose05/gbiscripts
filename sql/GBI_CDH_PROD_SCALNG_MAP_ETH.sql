/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

begin;

DELETE FROM GBI_CDH.PROD_SCALNG_MAP where FRAN_CD='ETH';

commit;

INSERT INTO GBI_CDH.PROD_SCALNG_MAP 
		(FRAN_CD, 
		SUB_PLTFRM, 
		SCALING_LVL, 
		DIV, 
		REV_SCALING_SRC, 
		REV_SCALING, 
		SCALED_VALUE, 
		UNIT_SCALING, 
		SCALED_VALUE2, 
		REV_MULT, 
		UNIT_MULT, 
		CRTD_BY, 
		UPDT_BY, 
		CRT_DTTM, 
		UPDT_DTTM
) 
SELECT  
		'ETH' AS FRAN_CD,
		TRIM(SUB_PLATFORM) AS SUB_PLTFRM, 
		TRIM(SCALING_LEVEL) AS SCALING_LEVEL,
		TRIM(DIVISION) AS DIVISION,
		TRIM(REVENUE_SCALING_SOURCE) AS REVENUE_SCALING_SOURCE, 
		TRIM(REVENUE_SCALING) AS REVENUE_SCALING,
		SCALED_VALUE,
		TRIM(UNITS_SCALING) AS UNITS_SCALING, 
		SCALED_VALUE2, 
		TRIM(REVENUE_MULTIPLIER) AS REVENUE_MULTIPLIER, 
		TRIM(UNIT_MULTIPLIER) AS UNIT_MULTIPLIER, 
		'GBI_CDH_PROD_SCALNG_MAP_ETH' AS CRTD_BY,
		'GBI_CDH_PROD_SCALNG_MAP_ETH' AS UPDT_BY,
		NOW() AS CRT_DTTM,
		NOW() AS UPDT_DTTM 
 FROM GBI_STG.SCALINGLEVEL_ETH;

commit;
end;