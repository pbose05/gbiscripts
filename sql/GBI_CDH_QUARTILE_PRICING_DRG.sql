/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

begin;

INSERT INTO gbi_cdh.quartile_prc
SELECT	
	drg_sku AS prod_cd,
	drg_mfg AS mfr_nm,
	year*100+month AS mo_id,
	quarter_ending,
	sum(q1max)  AS q1max, 
	sum(q2max)  AS q2max, 
	sum(q3max) AS q3max,
	sum(q4max)  AS q4max, 
	sum(q1mean)  AS q1mean, 
	sum(q2mean) AS q2mean,
	sum(q3mean)  AS q3mean, 
	sum(q4mean)  AS q4mean, 
	sum(q1min) AS q1min,
	sum(q4maxd)  AS q4maxd, 
	sum(q1uni) AS q1uni,
	sum(q2uni)  AS q2uni, 
	sum(q3uni)  AS q3uni, 
	sum(q4uni) AS q4uni,
	sum(q1dol)  AS q1dol, 
	sum(q2dol)  AS q2dol, 
	sum(q3dol) AS q3dol,
	sum(q4dol)  AS q4dol, 
	'DRG' AS src_sys_cd,
	'GBI_CDH_QUARTILE_PRICING_DRG' AS crtd_by,
	'GBI_CDH_QUARTILE_PRICING_DRG' AS updt_by,
	NOW() AS crt_dttm,
	NOW() AS updt_dttm
	FROM gbi_stg.drg_quartile_pricing
GROUP BY 
	drg_sku,
	drg_mfg,
	year*100+month,
	quarter_ending;

COMMIT;
end;