/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on
begin;

DELETE FROM GBI_CDH.SLS_INT_EXT_MNTHLY 
WHERE FRAN_CD = 'CSS' 
AND SOC_SETNG = 'HOSP' 
AND SRC_SYS_CD IN ('DRG','CSS');	

COMMIT;

/* Insert script for current month's data processing related to DRG*/

INSERT INTO GBI_CDH.SLS_INT_EXT_MNTHLY 
(
	FISC_MO_ID,
	PROD_CD,
	MFR_NM,
	SRC_SYS_CD,
  	FRAN_CD,
  	SOC_SETNG,
	PLTF,		--Added PLTF as this was missing in existing codebase
  	CNSMR_SLS_AMT,
  	CNSMR_SLS_UNIT,
	COMP_SLS_AMT,
  	COMP_SLS_UNIT,
  	NTS_SLS_AMT,
  	NTS_SLS_UNIT,
  	Crtd_By,
  	UPDT_BY,
  	CRT_DTTM,
  	UPDT_DTTM
)
SELECT	  
	CASE 
		WHEN COMP.FISC_MO_ID IS NOT NULL 
		THEN COMP.FISC_MO_ID 
		ELSE CONS.FISC_MO_ID 
	END AS FISC_MO_ID,
	CASE 
		WHEN COMP.PROD_CD IS NOT NULL 
		THEN COMP.PROD_CD 
		ELSE CONS.PROD_CD 
	END AS PROD_CD,
	CASE 
		WHEN COMP.MFR_NM IS NOT NULL 
		THEN COMP.MFR_NM 
		ELSE CONS.MFR_NM 
	END AS MFR_NM,
	CASE 
		WHEN COMP.SRC_SYS_CD IS NOT NULL 
		THEN COMP.SRC_SYS_CD 
		ELSE CONS.SRC_SYS_CD 
	END AS SRC_SYS_CD,
	'CSS' AS FRAN_CD,
	'HOSP' AS SOC_SETNG,
	COALESCE (COMP.PLATFORM1,CONS.PLATFORM) AS PLTF,	--Added PLTF as this was missing in existing code base
	COALESCE(CONS.CNSMR_SALES_AMT,0) AS CNSMR_SLS_AMT, 
	COALESCE(CONS.CNSMR_SALES_UNIT,0) AS CNSMR_SLS_UNIT, 
	COALESCE(COMP.SLS_AMT,0) AS COMP_SLS_AMT,
	COALESCE(COMP.SLS_UNIT,0) AS COMP_SLS_UNIT,
	COALESCE(CONS.NTS_SALES_AMT,0) AS NTS_SLS_AMT, 
	COALESCE(CONS.NTS_SALES_UNIT,0) AS NTS_SLS_UNIT,
	'SLS_INT_EXT_MTHLY_DRG_CSS' AS Crtd_By,
    'SLS_INT_EXT_MTHLY_DRG_CSS' AS UPDT_BY,
	NOW() AS Crt_Dttm,
	NOW() AS Updt_Dttm
FROM
    (SELECT DISTINCT X1.FISC_MO_ID, 
			trim(upper(PB1.PROD_CD)) PROD_cD, 
			PB1.MFR_NM,
			X1.SLS_AMT,
			X1.SLS_UNIT,
			X1.SRC_SYS_CD,
	        PB1.PLATFORM1
	  FROM (SELECT EXTRACT(YEAR FROM A.DATE)*100+EXTRACT(MON FROM A.DATE) FISC_MO_ID,
            SKU,MANUFACTURER MFR_NM,'DRG' SRC_SYS_CD,SUM(REVENUE) SLS_AMT,SUM(UNITS) SLS_UNIT
            from GBI_STG.DRG_MONTHLY_SALES_ALL_FRANCHISE A WHERE A.FRANCHISE in (select distinct busn_rule_gr_3 from gbi_cdh.busn_rule where busn_rule='external_sales'and rule_seq='1'and busn_rule_gr_1='CSS') GROUP BY 1,2,3,4) X1
	  INNER JOIN 
	(select DISTINCT SRC_FRAN,FRAN_CD, MFR_NM,DATA_SRC, DATA_sRC_PROD_cD,COMP_GRPNG,PLTF as PLATFORM1,PROD_CD
 	from GBI_WRK.INTRM_PROD_SCLNG WHERE SRC_FRAN='CSS' AND DATA_SRC='DRG') PB1
	  ON  TRIM(UPPER(X1.SKU)) = TRIM(UPPER(PB1.DATA_SRC_PROD_CD))
	  AND CASE WHEN TRIM(UPPER(X1.MFR_NM))='CERENOVOUS'::TEXT THEN'CERENOVUS'::TEXT
	  ELSE TRIM(UPPER(X1.MFR_NM)) END = TRIM(UPPER(PB1.MFR_NM))
    ) AS COMP
FULL OUTER JOIN
(
	SELECT	PB.MFR_NM AS MFR_NM, 
			SNS.FISC_MO_ID, 
			trim(upper(PB.PROD_CD)) AS PROD_CD,
			aggr_eus_prod_sls_amt AS CNSMR_SALES_AMT,
			aggr_eus_prod_qty AS CNSMR_SALES_UNIT, 
			0 AS NTS_SALES_AMT,
			0 AS NTS_SALES_UNIT, 
			'CSS'::CHARACTER VARYING(20) SRC_SYS_CD,
			PB.PLATFORM
			FROM	
			(
				select 
				trim(upper(product_code)) as prod_cd, 
				cast(fiscal_year_number as integer)*100+cast(fiscal_month as integer) as fisc_mo_id,
				company_code, 
				sum(eaches_qty) as aggr_eus_prod_qty, 
				sum(sales_amt) as aggr_eus_prod_sls_amt, 
				'CSS' as src_sys_cd
				from 
				gbi_stg.css_jpi_bwi_acct_sales
				where shpto_cot_cd in (select busn_rule_gr_3  from gbi_cdh.busn_rule where busn_rule_gr_2='JPI-BWI'
and busn_rule_gr_1='CSS' and rule_seq='1')
				and   shpto_setting_hosp_ooh not in (select busn_rule_gr_3  from gbi_cdh.busn_rule where busn_rule_gr_2='ALL'
														and busn_rule_gr_1='CSS' and rule_seq='2')
				group by 
				trim(upper(product_code)), 
				cast(fiscal_year_number as integer)*100+cast(fiscal_month as integer), 
				company_code, 
				src_sys_cd
			) SNS 
	INNER JOIN (select DISTINCT SRC_FRAN,FRAN_CD, MFR_NM,JNJ_CODE,PROD_CD,COMP_GRPNG,PLTF as PLATFORM from GBI_WRK.INTRM_PROD_SCLNG 
		    WHERE SRC_FRAN='CSS' AND JNJ_CODE<>'NA') PB 
		ON	TRIM(UPPER(SNS.PROD_CD)) = TRIM(UPPER(PB.JNJ_CODE))
		AND TRIM(UPPER(PB.COMP_GRPNG)) IN (select distinct busn_rule_gr_2
from gbi_cdh.busn_rule where busn_rule = 'CSS Company Grouping' and  busn_rule_gr_1 ='CSS' and rule_seq='1')
	
) CONS
ON	TRIM(UPPER(COMP.PROD_CD)) = TRIM(UPPER(CONS.PROD_CD))
AND	TRIM(UPPER(COMP.MFR_NM)) = TRIM(UPPER(CONS.MFR_NM))
AND	COMP.FISC_MO_ID = CONS.FISC_MO_ID;

commit;
end;