/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

begin;

DELETE FROM gbi_cdh.prod_sclg_fct WHERE SRC_SYS_CD='GHX' AND FRAN='ETH' AND SCLG_TYP='SMOOTHED';

INSERT INTO gbi_cdh.prod_sclg_fct (
		fran, 
		soc_setng, 
		scl_lvl, 
		sclg_typ, 
		fisc_mo_id, 
		mo_idx, 
		eus_sls_fct, 
		eus_unt_fct, 
		nts_sls_fct, 
		nts_unt_fct, 
		src_sys_cd, 
		Crtd_By, 
		updt_by, 
		Crt_Dttm, 
		Updt_Dttm
) 
SELECT 	FRAN,
		'HOSP' AS SOC_SETNG, 	
		SCL_LVL,
		SCLG_TYP,
		fisc_mo_id, 
		mo_idx, 
		ConsumerSLSA + (ConsumerSLSB*MO_IDX) AS EUS_SLS_FCT,
		ConsumerUNITA + (ConsumerUNITB*MO_IDX) as EUS_UNIT_FCT,
		NTSSLSA + (NTSSLSB*MO_IDX) AS NTS_SLS_FCT,
		NTSUNITA + (NTSUNITB*MO_IDX) AS NTS_UNIT_FCT,
		src_sys_cd,
		'GBI_CDH_SMOOTH_SCALING_GHX' as Crtd_By,
		'GBI_CDH_SMOOTH_SCALING_GHX' as updt_by,
		NOW() AS Crt_Dttm,
		NOW() AS Updt_Dttm
FROM 	GBI_CDH.VW_MKT_SMOOTH_FCTR
WHERE   src_sys_cd = 'GHX' AND FRAN='ETH';

COMMIT;
end;