/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

 with X AS
(SELECT
GRP_TYP_CD,
GRP_CD,
GRP_SID,
cast((cast (grp_attr1_val as date) + interval '1 month') as varchar) grp_attr1_val,
'EUS'::text src_sys_Cd
FROM gbi_ctl.lkup_param_grp A
WHERE src_sys_cd = 'EUS' AND GRP_CD in ('CURR_MTH')) 

update gbi_ctl.lkup_PARAM_GRP
set grp_attr1_val = X.grp_attr1_val,
Crt_Dttm = now(),
Updt_Dttm = now(),
Crtd_By = 'GBI_CTL_LKUP_PARAM_UPDATE_EUS',
Updt_By = 'GBI_CTL_LKUP_PARAM_UPDATE_EUS'
from X where 
gbi_ctl.lkup_PARAM_GRP.GRP_TYP_CD = X.GRP_TYP_CD
and gbi_ctl.lkup_PARAM_GRP.GRP_CD = X.GRP_CD
and gbi_ctl.lkup_PARAM_GRP.GRP_SID = X.GRP_SID
and gbi_ctl.lkup_PARAM_GRP.SRC_SYS_CD = X.SRC_SYS_CD;


COMMIT; 