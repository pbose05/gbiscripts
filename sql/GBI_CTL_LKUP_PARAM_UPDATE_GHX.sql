/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

with X as 
(SELECT
GRP_TYP_CD,
GRP_CD,
GRP_SID,
cast ((to_Date(grp_attr1_val , 'MM/DD/YYYY') + interval '3 month')  as varchar(100)) grp_attr1_val,
'GHX'::text src_sys_cd
FROM gbi_ctl.lkup_param_grp A
WHERE src_sys_cd = 'GHX'  AND GRP_CD in ('CURR_MTH') and grp_typ_cd = 'DATA_MONTH'
union 
SELECT 
GRP_TYP_CD,
GRP_CD,
GRP_SID,
EXTRACT(YEAR FROM to_Date(grp_attr1_val|| '01' , 'YYYYMMDD') + interval '1 month')::CHARACTER VARYING(512)||trim(to_char(EXTRACT(MONTH FROM to_Date(grp_attr1_val|| '01' , 'YYYYMMDD') + interval '1 month'),'00')) grp_attr1_val,
'GHX'::text src_sys_cd
FROM gbi_ctl.lkup_param_grp A
WHERE src_sys_cd = 'GHX' AND GRP_CD in ('MIN_QTR' , 'MAX_QTR') and grp_typ_cd = 'QTR_ID'
) 

update gbi_ctl.lkup_PARAM_GRP
set grp_attr1_val = X.grp_attr1_val,
Crt_Dttm = now(),
Updt_Dttm = now(),
Crtd_By = 'GBI_CTL_LKUP_PARAM_UPDATE_DRG',
UPDT_BY = 'GBI_CTL_LKUP_PARAM_UPDATE_DRG'
from X where 
gbi_ctl.lkup_PARAM_GRP.GRP_TYP_CD = X.GRP_TYP_CD
and gbi_ctl.lkup_PARAM_GRP.GRP_CD = X.GRP_CD
and gbi_ctl.lkup_PARAM_GRP.GRP_SID = X.GRP_SID
and gbi_ctl.lkup_PARAM_GRP.SRC_SYS_CD = X.SRC_SYS_CD ;

COMMIT; 