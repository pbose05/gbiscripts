/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin;
delete from gbi_sem.agg_mkt_sls where updt_by ='GBI_SEM_AGGR_MKT_EP_SLS_DRG_CSS' ;

commit;

insert into gbi_sem.agg_mkt_sls 
(fran_cd,soc_setng,fisc_mo_id , prod_cd , mfr_nm , pltf , comp_sls_amt , cnsmr_sls_amt , nts_sls_amt , comp_sls_unit,
cnsmr_sls_unit,nts_sls_unit,src_sys_cd,rev_sls_fctr,unit_sls_fctr,crtd_by,updt_by ,crt_dttm, updt_dttm)

(select FRAN as fran_cd,src_sys_cd,
fisc_mo_id , prod_cd , mfr_nm , pltf , tot_amt as comp_sls_amt , tot_amt as cnsmr_sls_amt ,0 as nts_sls_amt , 
0 as comp_sls_unit,tot_unit as cnsmr_sls_unit,0 as nts_sls_unit,src_sys_cd,scld_fractn as rev_sls_fctr,
scld_fractn as unit_sls_fctr,'GBI_SEM_AGGR_MKT_SLS_DRG_CSS' as crtd_by,
'GBI_SEM_AGGR_MKT_EP_SLS_DRG_CSS' as updt_by ,now() as crt_dttm,now() as updt_dttm
from gbi_wrk.ep_prod_sclg
where FRAN='CSS' AND SRC_SYS_CD='CSS' 

union

select FRAN as fran_cd,src_sys_cd,
fisc_mo_id , prod_cd , mfr_nm , pltf , tot_amt as comp_sls_amt , 0 as cnsmr_sls_amt ,0 as nts_sls_amt , 
tot_unit as comp_sls_unit,0 as cnsmr_sls_unit,0 as nts_sls_unit,src_sys_cd,scld_fractn as rev_sls_fctr,
scld_fractn as unit_sls_fctr,'GBI_SEM_AGGR_MKT_SLS_DRG_CSS' as crtd_by,
'GBI_SEM_AGGR_MKT_EP_SLS_DRG_CSS' as updt_by ,now() as crt_dttm,now() as updt_dttm
from gbi_wrk.ep_prod_sclg
 where FRAN='CSS' AND SRC_SYS_CD='DRG') ;
 
commit;
end;