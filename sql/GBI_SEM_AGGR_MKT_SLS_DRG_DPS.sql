/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin;

DELETE FROM GBI_SEM.AGG_MKT_SLS
WHERE FRAN_CD='DPS' AND SRC_SYS_cD in ('DRG','DPS') AND SOC_SETNG='HOSP' ; 


COMMIT;

INSERT INTO GBI_SEM.AGG_MKT_SLS 
       (
              FRAN_CD, SOC_SETNG,
	      FISC_MO_ID, 
              PROD_CD, 
              MFR_NM, 
              PLTF, 
              COMP_SLS_AMT, 
              CNSMR_SLS_AMT, 
              NTS_SLS_AMT, 
              COMP_SLS_UNIT, 
              CNSMR_SLS_UNIT, 
              NTS_SLS_UNIT, 
              SRC_SYS_CD, 
              REV_SLS_FCTR, 
              UNIT_SLS_FCTR, 
              Crtd_By, 
              Updt_By, 
              CRT_DTTM, 
              UPDT_DTTM
       )
       SELECT FRAN_CD, SOC_SETNG,
	      FISC_MO_ID,
              PROD_CD,
              MFR_NM,
              PLATFORM,
              COMP_SLS_AMT,
              CNSMR_SLS_AMT,
              NTS_SLS_AMT,
              COMP_SLS_UNIT,
              CNSMR_SLS_UNIT,
              NTS_SLS_UNIT,
              SRC_SYS_CD,
              CASE 
                     WHEN SLS_FACTOR IS NULL 
                     THEN CAST(1 AS DECIMAL(20,8))    
                     ELSE SLS_FACTOR 
              END    AS  REV_SLS_FCTR, 
              CASE 
                     WHEN UNIT_FACTOR IS NULL 
                     THEN 1 
                     ELSE UNIT_FACTOR 
              END AS UNIT_SLS_FCTR,
              'GBI_SEM_AGGR_MKT_SLS_DRG_DPS' AS Crtd_By,
              'GBI_SEM_AGGR_MKT_SLS_DRG_DPS' AS Updt_By,
              NOW() AS Crt_Dttm,
              NOW() AS Updt_Dttm
       FROM   
              (SELECT DISTINCT 
			   X.FRAN_CD, X.SOC_SETNG,
			   X.FISC_MO_ID,
                           X.PROD_CD AS PROD_CD,
                           X.MFR_NM,
                           X.COMP_SLS_AMT,
                           X.CNSMR_SLS_AMT,
                           X.NTS_SLS_AMT,
                           X.COMP_SLS_UNIT,
                           X.CNSMR_SLS_UNIT,
                           X.NTS_SLS_UNIT,
                           X.SRC_SYS_CD,
                           (CASE  
                                  WHEN X.REV_SCALING_SRC='EUS'                  
                                  THEN Y.EUS_SLS_FCT
                                  ELSE Y.NTS_SLS_FCT 
                           END) AS SLS_FACTOR,
                           (CASE  
                                  WHEN X.REV_SCALING_SRC='EUS'                   
                                  THEN Z.EUS_UNT_FCT
                                  ELSE Z.NTS_UNT_FCT 
                           END) AS UNIT_FACTOR,
                           COALESCE(X.PLATFORM,'NA') AS PLATFORM
              FROM
                     (SELECT      A.FRAN_CD,A.SOC_SETNG,A.FISC_MO_ID,
                                  TRIM(LOWER(A.PROD_CD)) AS PROD_CD,
                                  A.MFR_NM,
                                  A.COMP_SLS_AMT,
                                  A.CNSMR_SLS_AMT,
                                  A.NTS_SLS_AMT,
                                  A.COMP_SLS_UNIT,
                                  A.CNSMR_SLS_UNIT,
                                  A.NTS_SLS_UNIT,
                                  SCLG_LVL_VAL, 
                                  REV_SCALING_SRC,                                                    --SOURCES, 
                                  REV_SCALING,                                                        --REV_METHOD,
                                  UNIT_SCALING,                                                       --UNIT_METHOD, 
                                  A.SRC_SYS_CD,
                                  PB1.PLTF AS PLATFORM                                                --PB1.PLATFORM AS PLATFORM
                     FROM           (SELECT *   FROM GBI_CDH.SLS_INT_EXT_MNTHLY 
				     WHERE FRAN_CD ='DPS' and src_sys_cd ='DRG' AND SOC_SETNG='HOSP' ) A 
                     INNER JOIN                                                                                
                     (SELECT * from GBI_WRK.INTRM_PROD_SCLNG WHERE SRC_FRAN='DPS') PB1
                     ON  TRIM(UPPER(A.PROD_CD)) = TRIM(UPPER(PB1.PROD_CD))
                     AND TRIM(UPPER(A.MFR_NM)) = TRIM(UPPER(PB1.MFR_NM))  ) X 
              LEFT JOIN    (SELECT * 
                           FROM GBI_CDH.PROD_SCLG_FCT 
                           WHERE  SRC_SYS_CD ='DRG' 
                           AND FRAN = 'DPS') Y 
              ON   trim(upper(X.SCLG_LVL_VAL)) = trim(upper(Y.SCL_LVL))
              AND trim(upper(X.UNIT_SCALING)) = trim(upper(Y.SCLG_TYP ))
              AND X.FISC_MO_ID =Y.FISC_MO_ID 
              AND trim(upper(X.SRC_SYS_CD)) = trim(upper(Y.SRC_SYS_CD))
              LEFT JOIN    (SELECT       * 
                           FROM GBI_CDH.PROD_SCLG_FCT 
                           WHERE SRC_SYS_CD ='DRG' 
                           AND FRAN = 'DPS') Z
              ON   trim(upper(X.SCLG_LVL_VAL)) = trim(upper(Z.SCL_LVL))
              AND trim(upper(X.UNIT_SCALING)) = trim(upper(Z.SCLG_TYP ))
              AND X.FISC_MO_ID =Z.FISC_MO_ID 
              AND trim(upper(X.SRC_SYS_CD)) = trim(upper(Z.SRC_SYS_CD))
       UNION
              SELECT A.FRAN_CD,A.SOC_SETNG,FISC_MO_ID,
                           TRIM(LOWER(A.PROD_CD)) AS PROD_CD,
                           A.MFR_NM,
                           COMP_SLS_AMT, 
                           CNSMR_SLS_AMT,
                           NTS_SLS_AMT,
                           COMP_SLS_UNIT,
                           CNSMR_SLS_UNIT, 
                           NTS_SLS_UNIT,
                           A.SRC_SYS_CD,
                           NULL AS SLS_FACTOR,
                           NULL AS UNIT_FACTOR,
                           PB1.PLTF AS PLATFORM
              FROM   (SELECT * FROM GBI_CDH.SLS_INT_EXT_MNTHLY WHERE FRAN_CD ='DPS' and src_sys_cd ='DPS' AND SOC_SETNG='HOSP')  A 
              INNER JOIN                                                                                      
        (SELECT * from GBI_WRK.INTRM_PROD_SCLNG WHERE SRC_FRAN='DPS') PB1
              ON  TRIM(UPPER(A.PROD_CD)) = TRIM(UPPER(PB1.PROD_CD))
              AND TRIM(UPPER(A.MFR_NM)) = TRIM(UPPER(PB1.MFR_NM))  
              WHERE A.SRC_SYS_CD = 'DPS'              
                                                                                                                     
       ) A
;

COMMIT;

end;	