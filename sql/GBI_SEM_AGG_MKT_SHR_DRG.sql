/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin;

DELETE FROM GBI_SEM.AGG_MKT_SHR 
WHERE crtd_by = 'GBI_SEM_AGGR_MKT_SHR_DRG';
	

COMMIT;
 
INSERT INTO GBI_SEM.AGG_MKT_SHR (
              MATL, 
              PROD_SIZE, 
              PROD_NM, 
              PROD_TYP, 
              CO_GRP, 
              EUS_FL, 
              DRG_FL, 
              GHX_FL, 
              MFR_NM, 
              CORP_NM, 
              OEM_MFR, 
              FRAN, 
              DIV, 
              MAJ_PLTF, 
              PLTF, 
              SUB_PLTF, 
              BRND_FMLY, 
              BRND, 
              SCLG_TYPE, 
              SCLG_LVL, 
              SCLG_LVL_VAL, 
              SRC, 
              REV_METH, 
              UNIT_METH, 
              FISC_MO_ID, 
              FISC_MO_STRT_DT, 
              PROD_CD, 
              COMP_SLS_AMT, 
              CNSMR_SLS_AMT, 
              NTS_SLS_AMT, 
              COMP_SLS_UNIT, 
              CNSMR_SLS_UNIT, 
              NTS_SLS_UNIT, 
              REV_SLS_FCTR, 
              UNIT_SLS_FCTR, 
              SCAL_SLS_AMT, 
              SCAL_SLS_UNIT, 
              SRC_SYS_CD, 
              MO_NM, 
              CAL_QTR, 
              ROLL_QTR, 
              RQTR, 
              CQTR, 
              YR, 
              MAT, 
              CAL_QTR_STRT_DT, 
              Q1_MAX_VAL, 
              Q2_MAX_VAL, 
              Q3_MAX_VAL, 
              Q4_MAX_VAL, 
              Q1_MEAN_VAL, 
              Q2_MEAN_VAL, 
              Q3_MEAN_VAL, 
              Q4_MEAN_VAL, 
              Q1_UNIT_VAL, 
              Q2_UNIT_VAL, 
              Q3_UNIT_VAL, 
              Q4_UNIT_VAL, 
              Q1_DLR_VAL, 
              Q2_DLR_VAL, 
              Q3_DLR_VAL, 
              Q4_DLR_VAL, 
              --Q1_MIN_DLR_VAL, 
              --Q1_MIN_UNIT_VAL, 
              Q1_MIN_VAL, 
              Q4_MAXD_VAL, 
              --Q4_MAXU_VAL, 
              PROC_FLD,
	      crtd_by, 
	      updt_by, 
	      crt_dttm, 
	      updt_dttm
              )
SELECT 	      A.MATERIAL AS MATL, 
              A.PROD_SIZE AS SIZE, 
              A.PROD_NM, 
              A.PROD_TYP, 
              A.COMPANY_GROUPING AS CO_GRP, 
              A.EUS AS EUS_FL, 
              A.DRG AS DRG_FL, 
              A.GHX AS GHX_FL, 
              A.MFR_NM, 
              A.CORPORATION AS CORP_NM, 
              A.OEM_MANUFACTURER AS OEM_MFR, 
              A.FRANCHISE AS FRAN, 
              A.DIVISION AS DIV, 
              A.MAJOR_PLATFORM AS MAJ_PLTF, 
              A.PLATFORM AS PLTF, 
              A.SUB_PLATFORM AS SUB_PLTF, 
              A.BRAND_FAMILY AS BRND_FMLY, 
              A.BRAND AS BRND, 
              A.SCLG_TYPE, 
              A.SCLG_LVL, 
              A.SCLG_LVL_VAL, 
              A.SOURCES AS SRC, 
              A.REV_METHOD AS REV_METH, 
              A.UNIT_METHOD AS UNIT_METH, 
              A.FISC_MO_ID, 
              A.FISC_MO_STRT_DT, 
              A.PROD_CD, 
              A.COMP_SLS_AMT, 
              A.CNSMR_SLS_AMT, 
              A.NTS_SLS_AMT, 
              A.COMP_SLS_UNIT, 
              A.CNSMR_SLS_UNIT, 
              A.NTS_SLS_UNIT, 
              A.REV_SLS_FCTR, 
              A.UNIT_SLS_FCTR, 
              A.SCALED_SALES_AMT AS SCAL_SLS_AMT, 
              A.SCALED_SALES_UNIT AS SCAL_SLS_UNIT, 
              A.SRC_SYS_CD, 
              A.MO_NM, 
              A.CAL_QUARTER AS CAL_QTR, 
              A.ROLL_QUARTER AS ROLL_QTR, 
              A.RQTR, 
              A.CQTR, 
              A.YR, 
              A.MAT, 
              A.CAL_QTR_STRT_DT, 
              A.Q1MAX AS Q1_MAX_VAL, 
              A.Q2MAX AS Q2_MAX_VAL, 
              A.Q3MAX AS Q3_MAX_VAL, 
              A.Q4MAX AS Q4_MAX_VAL, 
              A.Q1MEAN AS Q1_MEAN_VAL, 
              A.Q2MEAN AS Q2_MEAN_VAL, 
              A.Q3MEAN AS Q3_MEAN_VAL, 
              A.Q4MEAN AS Q4_MEAN_VAL, 
              A.Q1UNI AS Q1_UNIT_VAL, 
              A.Q2UNI AS Q2_UNIT_VAL, 
              A.Q3UNI AS Q3_UNIT_VAL, 
              A.Q4UNI AS Q4_UNIT_VAL, 
              A.Q1DOL AS Q1_DLR_VAL, 
              A.Q2DOL AS Q2_DLR_VAL, 
              A.Q3DOL AS Q3_DLR_VAL, 
              A.Q4DOL AS Q4_DLR_VAL, 
              --A.Q1MINDOL AS Q1_MIN_DLR_VAL, 
              --A.Q1MINUNI AS Q1_MIN_UNIT_VAL, 
              A.Q1MIN AS Q1_MIN_VAL, 
              A.Q4MAXD AS Q4_MAXD_VAL, 
              --A.Q4MAXU AS Q4_MAXU_VAL, 
              A.PROC_FLD,
	      'GBI_SEM_AGGR_MKT_SHR_DRG' AS Crtd_By,
	      'GBI_SEM_AGGR_MKT_SHR_DRG' AS Updt_By,
	      NOW() AS Crt_Dttm,
	      NOW() AS Updt_Dttm
FROM (
              SELECT       MATERIAL, 
                           SIZE AS PROD_SIZE,
                           PROD.PRODUCT_NAME AS PROD_NM,
                           TYPE AS PROD_TYP, 
                           COMPANY_GROUPING,
                           EUS, 
                           DRG, 
                           GHX, 
                           SALES.MFR_NM, 
                           CORPORATION, 
                           OEM_MANUFACTURER, 
                           FRANCHISE,
                           DIVISION, 
                           MAJOR_PLATFORM, 
                           COALESCE( PROD.PLATFORM,'NA') AS PLATFORM, 
                           SUB_PLATFORM, 
                           BRAND_FAMILY,
                           BRAND, 
                           SCLG_TYPE, 
                           SCLG_LVL, 
                           SCLG_LVL_VAL, 
                           SOURCES, 
                           REV_METHOD,
                           UNIT_METHOD,      
			   SALES.FISC_MO_ID,
			   TO_DATE(CAST(SALES.FISC_MO_ID AS VARCHAR(10)), 'YYYYMM') AS FISC_MO_STRT_DT,
			   SALES.PROD_CD, 
			   COMP_SLS_AMT, 
			   CNSMR_SLS_AMT,
                           NTS_SLS_AMT,
			   SALES.COMP_SLS_UNIT,
			   SALES.CNSMR_SLS_UNIT,
			   SALES.NTS_SLS_UNIT,
                           REV_SLS_FCTR, 
                           UNIT_SLS_FCTR,
                           (CASE 
                                  WHEN  UPPER(OEM_MANUFACTURER) IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'OEM_MANUFACTURER' and rule_seq='1') THEN COMP_SLS_AMT
								  WHEN  UPPER(MANUFACTURER) IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'MANUFACTURER' and rule_seq='1') THEN COMP_SLS_AMT
								  WHEN  UPPER(COMPANY_GROUPING) IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='1' ) AND  PROD.SOURCES IN (select busn_rule_gr_3_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='1') THEN CNSMR_SLS_AMT
								  WHEN  UPPER(COMPANY_GROUPING) IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='2') AND  PROD.SOURCES IN (select busn_rule_gr_3_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='2') THEN NTS_SLS_AMT
                                  WHEN  UPPER(COMPANY_GROUPING) NOT IN ( select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq = '2') AND UPPER(PROD.REVENUE_MULTIPLIER) IN (select busn_rule_gr_1_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='4') THEN COMP_SLS_AMT * REV_SLS_FCTR
                                  WHEN  UPPER(COMPANY_GROUPING) NOT IN ( select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq ='2')  AND UPPER(PROD.REVENUE_MULTIPLIER) IN (select busn_rule_gr_1_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='5')  THEN COMP_SLS_AMT * UNIT_SLS_FCTR
                           END )  SCALED_SALES_AMT,
                           (CASE 
                                  WHEN  UPPER(OEM_MANUFACTURER) IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'OEM_MANUFACTURER' and rule_seq='1') THEN COMP_SLS_UNIT
								  WHEN  UPPER(MANUFACTURER) IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'MANUFACTURER' and rule_seq='1') THEN COMP_SLS_UNIT								  
                                  WHEN  UPPER(COMPANY_GROUPING) IN( select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='1') AND  PROD.SOURCES IN (select busn_rule_gr_3_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='1') THEN CNSMR_SLS_UNIT
                                  WHEN  UPPER(COMPANY_GROUPING) IN (select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='2')  AND  PROD.SOURCES IN (select busn_rule_gr_3_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='2') THEN NTS_SLS_UNIT
                                  WHEN  UPPER(COMPANY_GROUPING) NOT IN ( select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq ='2')  AND UPPER(PROD.UNIT_MULTIPLIER) IN (select busn_rule_gr_1_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='4') THEN COMP_SLS_UNIT * REV_SLS_FCTR 
                                  WHEN UPPER(COMPANY_GROUPING) NOT IN ( select busn_rule_gr_2_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq ='2') AND UPPER(PROD.UNIT_MULTIPLIER) IN (select busn_rule_gr_1_nm from gbi_cdh.busn_rule where busn_rule_gr_2 = 'COMPANY_GROUPING' and rule_seq='5') THEN COMP_SLS_UNIT * UNIT_SLS_FCTR
                           END )  SCALED_SALES_UNIT,
                           SALES.SRC_SYS_CD, 
                           MO_NM, 
                           CAL_QTR_NM AS CAL_QUARTER,
                           ROLL_QTR_ID AS ROLL_QUARTER, 
                           RQTR_NBR AS RQTR,
                           CALQTR_NBR AS CQTR , 
                           YR_NBR  AS YR,
                           YR_ROLL_NBR AS MAT,
                           TO_DATE(SUBSTR(CAL_QTR_NM,1,4)||(CASE WHEN SUBSTR(CAL_QTR_NM,6,1)='1'THEN'03'WHEN SUBSTR(CAL_QTR_NM,6,1)='2'THEN'06'WHEN SUBSTR(CAL_QTR_NM,6,1)='3'THEN'09'WHEN SUBSTR(CAL_QTR_NM,6,1)='4'THEN'12'ELSE'00' END)||'01' ,'YYYYMMDD') AS CAL_QTR_STRT_DT,
                           Q1MAX,
                           Q2MAX,
                           Q3MAX,
                           Q4MAX,
                           Q1MEAN,
                           Q2MEAN,
                           Q3MEAN,
                           Q4MEAN,
                           Q1UNI,
                           Q2UNI,
                           Q3UNI,
                           Q4UNI,
                           Q1DOL,
                           Q2DOL,
                           Q3DOL,
                           Q4DOL,
                           --Q1MINDOL,
                           --Q1MINUNI,
                           Q1MIN,
                           Q4MAXD,
                           --Q4MAXU,
                           PROD.MAIN_COMPETITOR AS PROC_FLD
              FROM GBI_SEM.AGG_MKT_SLS AS SALES 
              INNER JOIN GBI_CDH.GBI_PRODUCT_DIM  AS PROD 
              ON TRIM(LOWER(SALES.PROD_CD)) = TRIM(LOWER(PROD.PRODUCT_CODE))
              AND TRIM(LOWER(SALES.MFR_NM)) = TRIM(LOWER(PROD.MANUFACTURER))
              LEFT JOIN GBI_CDH.DIM_TIME_ROLLING AS TIM 
              ON SALES.FISC_MO_ID = TIM.FISC_MO_ID AND SALES.SRC_SYS_CD=TIM.SRC_SYS_CD
              LEFT JOIN gbi_cdh.quartile_prc AS PRC 
              ON TRIM(LOWER(SALES.PROD_CD)) = TRIM(LOWER(PRC.PROD_CD))  
              AND TRIM(LOWER(SALES.MFR_NM)) = TRIM(LOWER(PRC.MFR_NM))
              AND SALES.FISC_MO_ID = PRC.MO_ID
              WHERE PROD.platform not in (select distinct platform from gbi_cdh.gbi_product_dim where ghx ='1')
			  and sales.fran_cd = 'ETH'
    ) A ;

commit;
end;