/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

BEGIN;
DELETE FROM gbi_wrk.ep_prod_sclg
WHERE FRAN='CSS' AND SRC_SYS_CD='DRG' ;

COMMIT;

WITH 
NON_BWI_INT_SLS as 
(
SELECT 
x1.FISC_MO_ID,
upper(platform) AS platform,
upper(sub_platform)  as sub_platform,
upper(brand_family) as brand_family,
upper(brand) as brand,
sum(SLS_UNIT) as tot_unit,
sum(SLS_AMT) as tot_amt
FROM
	(	SELECT 
		FISC_MO_ID,
        prod_cd,
		MFR_NM,
		SUM(comp_sls_amt) as SLS_AMT,
		 SUM(comp_sls_unit)  as SLS_UNIT
         from gbi_cdh.sls_int_ext_mnthly A where fran_cd ='CSS'
		 and cnsmr_sls_unit =0
		 and cnsmr_sls_unit =0
		 GROUP BY 1,2,3
	) X1
	  INNER JOIN 
	(
	
		select * from
		gbi_stg.dim_prod_css
		where upper(platform) 
			in (select distinct upper(busn_rule_gr_3)  from gbi_cdh.busn_rule
				where busn_rule ='Consortium_Scaling' and busn_rule_gr_1='CSS'
				and busn_rule_gr_2 ='dim_prod_css' and rule_seq='1' 
				and busn_rule_gr_3_nm='Platform' ) --(upper('Therapeutic Catheter'),upper('Diagnostic Catheter'))
		and upper(company_grouping) not 
			in (select distinct busn_rule_gr_2
from gbi_cdh.busn_rule where busn_rule = 'CSS Company Grouping' and  busn_rule_gr_1 ='CSS' and rule_seq='1') -- 'Biosense Webster'
	) PB1
	  ON  TRIM(UPPER(X1.prod_cd)) = TRIM(UPPER(PB1.DATA_SRC_CD))
	  AND TRIM(UPPER(X1.MFR_NM)) = TRIM(UPPER(PB1.manufacturer))
	  group by 1,2,3,4,5
) ,
consort as 
(
select
cast(yr as integer)*100+cast(mnth as integer) as fisc_mo_id,
ep_scaling_css.grp, 
ep_scaling_css.sub_group,
ep_scaling_css.size,
pltf, sub_pltf,
case when brnd_fmly IS NULL then '#'
else brnd_fmly end as brnd_fmly,
case when brnd IS NULL then '#'
else brnd end as brnd,
sum(cast(submission_units as numeric(24,8))) as unit,
sum(cast(Aggregate_Units as numeric(24,8))) as agg_unit
from gbi_stg.ep_scaling_css
inner join gbi_wrk.intrm_ep_mapping on ep_scaling_css.grp=intrm_ep_mapping.grp
and ep_scaling_css.sub_group= intrm_ep_mapping.sub_grp and ep_scaling_css.size=intrm_ep_mapping.size
where upper(ep_scaling_css.grp) in 
(select distinct upper(busn_rule_gr_3)  from gbi_cdh.busn_rule
						  where busn_rule ='Consortium_Scaling' and busn_rule_gr_1='CSS'
						  and busn_rule_gr_2 ='ep_scaling_css' and rule_seq='1' 
						  and busn_rule_gr_3_nm='Group')--('EP Ablation Catheters','EP Diagnostic Catheters')
group by 1,2,3,4,5,6,7,8
)
,scl_fct as 
(select
consort.fisc_mo_id,
consort.grp,
consort.sub_group,
consort.size,
consort.pltf,
consort.sub_pltf,
coalesce(consort.brnd_fmly,'#') as brnd_fmly,
coalesce(consort.brnd,'#') as brnd,
consort.unit as ep_sbmssn_unit,
consort.agg_unit as ep_tot_agg_unit,
--BWI_INT_SLS.prod_cd,
NON_BWI_INT_SLS.tot_unit as tot_unit_ext,
NON_BWI_INT_SLS.tot_amt as tot_amt_ext,
case when NON_BWI_INT_SLS.tot_unit ='0' then '0'
else (((consort.agg_unit)-(consort.unit))/NON_BWI_INT_SLS.tot_unit)
end as scld_frac
from NON_BWI_INT_SLS  inner join consort  
on NON_BWI_INT_SLS.fisc_mo_id=consort.fisc_mo_id
and upper(consort.pltf)=upper(NON_BWI_INT_SLS.platform)
and upper(consort.sub_pltf)=upper(NON_BWI_INT_SLS.sub_platform)
and COALESCE(upper(consort.brnd_fmly),'#')=COALESCE(upper(NON_BWI_INT_SLS.brand_family),'#')
and COALESCE(upper(consort.brnd),'#')=COALESCE(upper(NON_BWI_INT_SLS.brand),'#')
) ,
prod as 
(SELECT 
x1.FISC_MO_ID,upper(trim(prod_cd)) as prod_cd,upper(trim(manufacturer)) as mfr_nm,
upper(platform) AS platform,
upper(sub_platform)  as sub_platform,
coalesce(upper(brand_family),'#') as brand_family,
coalesce(upper(brand),'#') as brand,
sum(SLS_UNIT) as tot_unit,
sum(SLS_AMT) as tot_amt
FROM
	(	SELECT 
		FISC_MO_ID,
        prod_cd,
		MFR_NM,
		SUM(comp_sls_amt) as SLS_AMT,
		SUM(comp_sls_unit)  as SLS_UNIT
        from gbi_cdh.sls_int_ext_mnthly A where fran_cd ='CSS'
		and cnsmr_sls_unit =0
		and cnsmr_sls_unit =0
		GROUP BY 1,2,3
	) X1
	  INNER JOIN 
	(
	
		select * from
		gbi_stg.dim_prod_css
		where upper(platform) 
			in (select distinct upper(busn_rule_gr_3)  from gbi_cdh.busn_rule
				where busn_rule ='Consortium_Scaling' and busn_rule_gr_1='CSS'
				and busn_rule_gr_2 ='dim_prod_css' and rule_seq='1' 
				and busn_rule_gr_3_nm='Platform' ) --(upper('Therapeutic Catheter'),upper('Diagnostic Catheter'))
		and upper(company_grouping) not 
			in (select distinct busn_rule_gr_2
from gbi_cdh.busn_rule where busn_rule = 'CSS Company Grouping' and  busn_rule_gr_1 ='CSS' and rule_seq='1') -- 'Biosense Webster'
	) PB1
	  ON  TRIM(UPPER(X1.prod_cd)) = TRIM(UPPER(PB1.DATA_SRC_CD))
	  AND TRIM(UPPER(X1.MFR_NM)) = TRIM(UPPER(PB1.manufacturer))
	  group by 1,2,3,4,5,6,7
) 
INSERT INTO gbi_wrk.ep_prod_sclg
SELECT 
prod.fisc_mo_id, prod.prod_cd,prod.mfr_nm, prod.platform, prod.sub_platform, prod.brand_family, prod.brand, scl_fct.grp, scl_fct.sub_group, scl_fct.size, 
prod.tot_unit,(prod.tot_unit*scl_fct.scld_frac),prod.tot_amt,(prod.tot_amt*scl_fct.scld_frac),
scl_fct.scld_frac,scl_fct.tot_unit_ext as tot_sbmssn_unit ,(scl_fct.ep_tot_agg_unit - scl_fct.ep_sbmssn_unit) as ep_sbmssn_unit,scl_fct.ep_tot_agg_unit,
'CSS' as fran,'HOSP' as soc_setng,'DRG' as src_sys_cd,'GBI_WRK_EP_SCLNG_EXT' AS crtd_by,
'GBI_WRK_EP_SCLNG_EXT' AS EP_SCLNG_INT,NOW(),NOW()
FROM PROD inner join scl_fct 
on prod.fisc_mo_id = scl_fct.fisc_mo_id
and upper(trim(prod.platform))=upper(trim(scl_fct.pltf))
and UPPER(TRIM(prod.sub_platform))=UPPER(TRIM(scl_fct.sub_pltf))
and COALESCE(upper(prod.brand_family),'#')=COALESCE(upper(scl_fct.brnd_fmly),'#')
and COALESCE(upper(prod.brand),'#')=COALESCE(upper(scl_fct.brnd),'#') ;

COMMIT;
END;


