/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
--Pre SQL Delete:
\set ON_ERROR_STOP on

begin;

DELETE  FROM GBI_WRK.INTRM_GHX_SLS
WHERE SRC_SYS_CD = 'GHX' 
AND	QTR_ID IN (
				SELECT DISTINCT  CAST(SUBSTR (CAL_QTR_NM, 1 , 4) || '0' || SUBSTR (CAL_QTR_NM, 6, 1) AS INTEGER) FROM  gbi_cdh.DIM_TIME_ROLLING
				WHERE IDX_NBR BETWEEN
							(SELECT MIN(IDX_NBR) FROM gbi_cdh.DIM_TIME_ROLLING 
									WHERE CAL_QTR_NM = (SELECT  SUBSTR (GRP_ATTR1_VAL,1,4) || '-' ||   SUBSTR (GRP_ATTR1_VAL,6,1) FROM  GBI_CTL.LKUP_PARAM_GRP 
										WHERE GRP_TYP_CD = 'QTR_ID' 
										AND GRP_CD = 'MAX_QTR'
										AND SRC_SYS_CD = 'GHX') )
							AND 
							(SELECT MIN(IDX_NBR) +35  FROM gbi_cdh.DIM_TIME_ROLLING
 									WHERE CAL_QTR_NM = (SELECT  SUBSTR (GRP_ATTR1_VAL,1,4) || '-' ||   SUBSTR (GRP_ATTR1_VAL,6,1) FROM  GBI_CTL.LKUP_PARAM_GRP 
 										WHERE GRP_TYP_CD = 'QTR_ID' 
 										AND GRP_CD = 'MAX_QTR'
										AND SRC_SYS_CD = 'GHX') )
				);

commit;

--Insert SQL:

INSERT INTO gbi_wrk.intrm_ghx_sls(data_mth, prod_cd, mfr_nm, msr_time, qtr_id, dlr_val, unit_nbr,src_sys_cd, Crtd_By, updt_by, Crt_Dttm, Updt_Dttm)
select
	to_date(param.grp_attr1_val , 'yyyy-mm-dd') as  data_mth,
	ghx.hpis_sku as prod_cd,
	p.manufacturer as mfr_nm,
	'CALQTR' as msr_time,
	cast(cast(year as varchar(4)) || cast(lpad(cast(quarter as varchar(2)),2,'0') as varchar(2)) as integer) as qtr_id,
	sum(ghx.dollars) dlr_val,
	sum(ghx.units) unit_nbr,
	cast('GHX' as varchar(20)) as src_sys_cd,
	'GBI_WRK_INTRM_GHX_SLS' Crtd_By,
	'GBI_WRK_INTRM_GHX_SLS' updt_by,				
	now() Crt_Dttm, 		
	now() Updt_Dttm     							
from gbi_stg.ghx_quarterly_sales as ghx		
inner join gbi_cdh.gbi_product_dim as p	
on   trim(lower(ghx.hpis_sku)) = trim(lower(p.product_code))
and  trim(lower(ghx.mfg_name)) = trim(lower(p.manufacturer)) 	
and p.platform in (select distinct platform from gbi_cdh.gbi_product_dim where ghx = '1')	
cross join (
		select	grp_attr1_val  
		from	gbi_ctl.lkup_param_grp param
		where	param.grp_typ_cd = 'DATA_MONTH' 
		and	param.grp_cd = 'CURR_MTH'
		and	param.src_sys_cd = 'GHX'
	    ) as param
group by param.grp_attr1_val,
	 ghx.hpis_sku,
	 p.manufacturer,
	 qtr_id
;

commit;

--Post SQL Delete:

DELETE FROM GBI_WRK.INTRM_GHX_SLS
WHERE SRC_SYS_CD = 'GHX' 
AND   QTR_ID = CAST((SELECT GRP_ATTR1_VAL  FROM  GBI_CTL.LKUP_PARAM_GRP
               	WHERE GRP_TYP_CD = 'QTR_ID' 
               	AND GRP_CD = 'MIN_QTR'
				AND SRC_SYS_CD = 'GHX')	AS INTEGER)	;

commit;
end;