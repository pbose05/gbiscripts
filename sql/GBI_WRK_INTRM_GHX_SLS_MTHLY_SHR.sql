/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
--Pre SQL Delete
\set ON_ERROR_STOP on

begin;

DELETE FROM gbi_wrk.intrm_ghx_sls_mthly_shr;

commit;
--Insert
INSERT INTO gbi_wrk.intrm_ghx_sls_mthly_shr
		   (mo_id, 
			qtr, 
			mo_sls, 
			qtr_sls, 
			mo_unit, 
			qtr_unit, 
			sls_shr, 
			unit_shr, 
            src_sys_cd, 
			Crtd_By, 
			updt_by, 
			Crt_Dttm, 
			Updt_Dttm
			)
SELECT 	cast(B.FISC_MO_ID as INTEGER), 
		cast(B.QTR_ID as INTEGER), 
		B.MONTHLY_SALES,
		A.Quarterly_Sales,
		B.MONTHLY_UNIT, 
		A.Quarterly_Unit, 
		(round( (case when  A.Quarterly_Sales <> 0 then Monthly_Sales/Quarterly_Sales	else 0 end)*10))/10 AS SALES_SHARE,
		(round( (case when  A.Quarterly_UNIT <> 0 then Monthly_UNIT/Quarterly_UNIT	else 0 end)*10))/10 AS UNIT_SHARE,
		'GHX',
		'GBI_WRK_INTRM_GHX_SLS_MTHLY_SHR',
		'GBI_WRK_INTRM_GHX_SLS_MTHLY_SHR',
		now(),
		now()
FROM
	(SELECT QTR_ID, 
		SUM(EIS_A.AGGR_EUS_PROD_QTY) Quarterly_Unit, 
		SUM(EIS_A.AGGR_EUS_PROD_SLS_AMT) Quarterly_Sales
	   FROM GBI_STG.ETH_INTERNAL_SALES_GHX EIS_A 
   GROUP BY QTR_ID) A,				--sq1
	(SELECT QTR_ID, 
			FISC_MO_ID, 
			SUM(EIS_B.AGGR_EUS_PROD_QTY) MONTHLY_UNIT, 
			SUM(EIS_B.AGGR_EUS_PROD_SLS_AMT) MONTHLY_SALES
	   FROM GBI_STG.ETH_INTERNAL_SALES_GHX EIS_B
   GROUP BY QTR_ID, FISC_MO_ID) B  --sq2
WHERE A.QTR_ID = B.QTR_ID;

commit;
end;