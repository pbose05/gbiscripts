/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/


\set ON_ERROR_STOP on

begin;

truncate table gbi_wrk.intrm_knee_alctn_dps;

commit;


WITH 
Bipolar AS 
	(	select 
		left(cast(dtr.cal_dt_yyyymmdd as varchar(20)),length(cast(dtr.cal_dt_yyyymmdd as varchar(20)))-2 ) as fisc_mo_yr_bipolar,
		coalesce(sum(tot_units),'0') as tot_units_bipolar,
		coalesce(sum(tot_dol),'0') as tot_dol_bipolar 
		from
		(select distinct cal_dt_yyyymmdd from gbi_cdh.fiscal_day) dtr 
		left outer join 
		(SELECT 
		replace(trim(upper(prod_dps.jnj_code)),'-','') as prod_cd,
		sls_dps.fisc_mo_yr,
		aggr_eus_prod_qty as tot_units, 
		aggr_eus_prod_sls_amt as tot_dol
		FROM 
		(	
		select * from gbi_wrk.intrm_prod_sclng where sub_pltf= 'Bipolar' and src_fran='DPS' and jnj_code <> 'NA'
		) prod_dps
		INNER JOIN gbi_wrk.intrm_int_sls_dps sls_dps
		ON replace(trim(upper(prod_dps.jnj_code)),'-','') = replace(trim(upper(sls_dps.prod_cd)),'-','') 
		) bp on left(cast(dtr.cal_dt_yyyymmdd as varchar(20)),length(cast(dtr.cal_dt_yyyymmdd as varchar(20)))-2 )=cast(bp.fisc_mo_yr as varchar(20))
		group by fisc_mo_yr_bipolar
	) , 
	
Head_Metal AS 
	(
		select 
		left(cast(dtr.cal_dt_yyyymmdd as varchar(20)),length(cast(dtr.cal_dt_yyyymmdd as varchar(20)))-2 ) as fisc_mo_yr_HM,
		coalesce(sum(tot_units),'0') as tot_units_HM,
		coalesce(sum(tot_dol),'0') as tot_dol_HM 
		from
		(select distinct cal_dt_yyyymmdd from gbi_cdh.fiscal_day) dtr left outer join 
		(SELECT 
		prod_dps.prod_cd,
		sls_dps.fisc_mo_yr,
		aggr_eus_prod_qty as tot_units, 
		aggr_eus_prod_sls_amt as tot_dol
		FROM 
		(	
		select 
		distinct replace(trim(upper(left(jnj_code,length(jnj_code) - 3))),'-','') as prod_cd
		from gbi_wrk.intrm_prod_sclng where sub_pltf= 'Head-Metal' and src_fran='DPS' and jnj_code <> 'NA'
		) prod_dps
		INNER JOIN gbi_wrk.intrm_int_sls_dps sls_dps
		ON prod_dps.prod_cd= replace(trim(upper(sls_dps.prod_cd)),'-','') 
		) up on left(cast(dtr.cal_dt_yyyymmdd as varchar(20)),length(cast(dtr.cal_dt_yyyymmdd as varchar(20)))-2 )=cast(up.fisc_mo_yr as varchar(20))
		group by fisc_mo_yr_HM
	)
	
	
insert into gbi_wrk.intrm_knee_alctn_dps
(
	fisc_mo_id,
	tot_units_hm,
	tot_dol_hm,
	tot_units_bipolar,
	tot_dol_bipolar,
	percent_allocation_hm,
	src_sys_cd,
	crt_dttm,
	crtd_by
)	

select 
	cast(fisc_mo_yr_HM as integer) as fisc_mo_id,
	tot_units_HM,
	tot_dol_HM,
	tot_units_bipolar,
	tot_dol_bipolar,
	case
		when tot_units_bipolar > tot_units_HM then 1 
		when (tot_units_bipolar < tot_units_HM and tot_units_HM <> 0) then tot_units_bipolar/tot_units_HM
		when (tot_units_bipolar < tot_units_HM and tot_units_HM = 0) then NULL 
		end as percent_allocation_hm,
	'DPS' as src_sys_cd,
	now() as crt_dttm,
	'GBI_WRK_INTRM_KNEE_ALLC_DPS' as crtd_by
	from 
	Bipolar 
		full outer join 
	Head_Metal 
	on Bipolar.fisc_mo_yr_bipolar=Head_Metal.fisc_mo_yr_HM;
	
commit;


end;