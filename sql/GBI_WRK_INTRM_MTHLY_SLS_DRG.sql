/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
\set ON_ERROR_STOP on

begin;

DELETE FROM gbi_wrk.intrm_mthly_sls WHERE SRC_SYS_CD = 'DRG';

commit;

INSERT INTO gbi_wrk.intrm_mthly_sls 
(
	fisc_mo_id
       , prod_cd
       , mfr_nm
       , sls_amt
       , sls_unit
       , src_sys_cd
       , Crtd_By
       , updt_by
       , crt_dttm
       , updt_dttm
) 
(
	SELECT 
		fisc_mo_id, 
		prod_cd,
		manufacturer,
		SUM(REVENUE) sls_amt, 
		SUM(UNITS) sls_unit,
		'DRG' src_sys_cd,
		'GBI_WRK_INTRM_MTHLY_SLS_DRG' Crtd_By,
		'GBI_WRK_INTRM_MTHLY_SLS_DRG' updt_by,
		NOW() Crt_Dttm,
		NOW() Updt_Dttm
	FROM 
	(
		SELECT 
			CAST( OVERLAY(SUBSTRING(cast(a.date as text),1,7) 
					  PLACING '' FROM POSITION('-' IN cast(a.date as text)) FOR 1) 
					  AS INTEGER
					) fisc_mo_id, 
			A.SKU PROD_CD , 
			B.MANUFACTURER , 
			A.REVENUE  , 
			A.UNITS 
		FROM 
			gbi_stg.drg_monthly_sales A 
		INNER JOIN gbi_cdh.GBI_PRODUCT_DIM B
		ON  trim(lower(B.PRODUCT_CODE)) = trim(lower(A.SKU))
		AND trim(lower(B.MANUFACTURER)) = trim(lower(A.MANUFACTURER))
		AND B.DRG='1'		
	) Q1
GROUP BY 
		fisc_mo_id, 
		prod_cd,
		manufacturer,
		src_sys_cd
);

COMMIT;
end;