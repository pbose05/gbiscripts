/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/
--Pre SQL Delete:
\set ON_ERROR_STOP on

begin;

DELETE FROM gbi_wrk.intrm_mthly_sls WHERE SRC_SYS_CD='GHX';

COMMIT;

--Insert:

INSERT INTO gbi_wrk.intrm_mthly_sls (
		 fisc_mo_id, 
		 prod_cd, 
		 mfr_nm, 
		 sls_amt, 
		 sls_unit, 
		 src_sys_cd, 
		 Crtd_By, 
		 updt_by, 
		 Crt_Dttm, 
		 Updt_Dttm
) 
SELECT 
 TM.FISC_MO_ID, 
 SLS.PROD_CD, 
 SLS.MFR_NM, 
 SLS.DLR_VAL * SHR.SLS_SHR AS SLS_AMT, 
 SLS.UNIT_NBR * SHR.UNIT_SHR AS SLS_UNIT,
 'GHX',
 'GBI_WRK_INTRM_MNTHLY_SLS_GHX',
 'GBI_WRK_INTRM_MNTHLY_SLS_GHX',
 NOW(),
 NOW()
FROM
gbi_wrk.intrm_ghx_sls AS SLS  --   IMS_GHX_NATNL_SLS
 INNER JOIN ( select fisc_mo_id, 
					 CAST(OVERLAY(cal_qtr_nm PLACING '0' FROM POSITION('-' IN cal_qtr_nm) FOR 1) AS INTEGER) AS CAL_QTR_NM
				from gbi_cdh.dim_time_rolling 
				WHERE SRC_SYS_CD='GHX'
			 ) TM
 ON SLS.QTR_ID = TM.CAL_QTR_NM
 INNER JOIN gbi_wrk.intrm_ghx_sls_mthly_shr SHR  --  GHX_SLS_MTHLY_SHR
 ON TM.FISC_MO_ID = SHR.MO_ID
WHERE
 SLS.SRC_SYS_CD ='GHX'   ;
 
 COMMIT;
end;