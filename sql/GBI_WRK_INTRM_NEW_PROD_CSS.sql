/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on


begin;

-------------CSS QC for EXT & INT Sales Starts--------------

-------------CSS QC for DRG (EXT) Sales Starts--------------

----drg_no_map_ind

-- codes in CSS EXT Sales file (drg_monthly_sales_all_franchise) but not in mapping file (dim_prod_CSS)

delete from gbi_wrk.intrm_new_prod where src_fran = 'DRG_CSS' and drg_no_map_ind = 'Y';

commit;

insert into gbi_wrk.intrm_new_prod 
(
        src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , drg_no_map_ind
       , Crtd_By
       , Crt_Dttm
) 
with 
codes_in_drg_not_in_map as
(
                select distinct
                                'DRG_CSS'::varchar(30) as src_fran,
                                q1.fisc_mo_id,
                                q1.prod_cd,
                                q1.mfr_nm,
                                q1.div,
                                q1.pltf,
                                q1.revn,
                                q1.unit,
                                case
                                    when e.data_src_cd is not null 
									then 'Y' 
									else 'N'
                                end excln_ind,
                                q1.drg_no_map_ind,
                                q1.Crtd_By,
                                q1.Crt_Dttm
                from
                (
                                select distinct
                                                cast(substring(replace(cast(dms.date as varchar), '-','') from 1 for 6) as integer) fisc_mo_id,
                                                trim(dms.sku) prod_cd,
                                                trim(dms.manufacturer) mfr_nm,
												COALESCE (trim(dms.division),'N/A') div,
												COALESCE (trim(dms.platform),'N/A') pltf,
                                                sum(dms.revenue) revn,
                                                sum(dms.units) unit,
                                                'Y'::char drg_no_map_ind,
                                                'CSS_QC_EXT-PRODUCT_IN_DRG_SALES_NOT_IN_MAPPING'::varchar(50) Crtd_by,
                                                current_timestamp Crt_Dttm
                                from 
                                                gbi_stg.drg_monthly_sales_all_franchise dms
								where 			franchise in 	(select distinct busn_rule_gr_3 from gbi_cdh.busn_rule 
																where busn_rule = 'external_sales' and busn_rule_gr_1 = 'CSS' 
																and busn_rule_gr_2 = 'drg_monthly_sales_all_franchise')          -- = 'CSS'
                                and not exists
												(select 1 from gbi_stg.dim_prod_CSS css
												where 	upper(trim(css.data_src_cd)) = upper(trim(dms.sku))
												and 	upper(trim(css.manufacturer)) = upper(trim(dms.manufacturer))
												and 	css.data_src = 'DRG'
												)
--                left outer join gbi_stg.dim_prod_dps dps
--                on upper(trim(dps.jnj_code)) = upper(trim(djrs.product_code)) 
--                where 			dps.crtd_by is null
                                group by 
                                                dms.date,
                                                dms.sku,
                                                dms.manufacturer,
												trim(dms.division),
												trim(dms.platform) 
                )q1
                left outer join (SELECT DISTINCT data_src_cd,manufacturer FROM gbi_stg.exclusion_css) e  --modified to remove duplicates in exclusion file
                on           	upper(trim(q1.prod_cd)) = upper(trim(e.data_src_cd))
                and 			upper(trim(q1.mfr_nm)) = upper(trim(e.manufacturer))
),
prod_code_in_map_with_diff_mfr as
(
              select trim(prod_cd) prod_cd from codes_in_drg_not_in_map
                intersect
              select trim(data_src_cd) prod_cd from gbi_stg.dim_prod_css
				WHERE data_src = 'DRG' 
)
select distinct
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.div,
                t1.pltf,
                t1.revn,
                t1.unit,
                case 
                    when t2.prod_cd is not null 
					then 'Y'
					else null
                end mfr_diff_ind,
                t1.excln_ind,
                t1.drg_no_map_ind,
                t1.Crtd_By,
                t1.Crt_Dttm
from      
                codes_in_drg_not_in_map t1
left outer join 
                prod_code_in_map_with_diff_mfr t2
on           	upper(trim(t1.prod_cd)) = upper(trim(t2.prod_cd))
;
 
commit;

----map_no_drg_ind

-- codes in mapping file(dim_prod_CSS) but not in CSS EXT Sales file (drg_monthly_sales_all_franchise)
 
delete from gbi_wrk.intrm_new_prod where src_fran = 'DRG_CSS' and map_no_drg_ind = 'Y';

commit;
 
insert into gbi_wrk.intrm_new_prod 
(
         src_fran
       , fisc_mo_id
       , prod_cd
	   	 , hrmnzd_prod_cd
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_drg_ind
       , crtd_by
       , crt_dttm
)
with 
codes_in_map_not_in_drg as
(
                select distinct
                                trim(css.data_src_cd) prod_cd, trim(css.hrmnzd_prod_cd) hrmnzd_prod_cd,
                                trim(css.manufacturer) mfr_nm, 
                                COALESCE (trim(css.division),'N/A') div, 	 
                                COALESCE (trim(css.platform),'N/A') pltf 	
                from gbi_stg.dim_prod_css css
				WHERE css.data_src = 'DRG'
                AND not exists 
                (
                select 	1
                from 	gbi_stg.drg_monthly_sales_all_franchise dms
                where 	franchise in 	(select distinct busn_rule_gr_3 from gbi_cdh.busn_rule 
										where busn_rule = 'external_sales' and busn_rule_gr_1 = 'CSS' 
										and busn_rule_gr_2 = 'drg_monthly_sales_all_franchise')          -- = 'CSS'
                and     upper(trim(css.data_src_cd)) = upper(trim(dms.sku)) 
				and 	upper(trim(css.manufacturer)) = upper(trim(dms.manufacturer))
                )
),
prod_code_in_drg_with_diff_mfr as
(
            	
				select trim(prod_cd) prod_cd from codes_in_map_not_in_drg
                intersect
                select trim(sku) prod_cd from gbi_stg.drg_monthly_sales_all_franchise
                where 	franchise in 	(select distinct busn_rule_gr_3 from gbi_cdh.busn_rule 
										where busn_rule = 'external_sales' and busn_rule_gr_1 = 'CSS' 
										and busn_rule_gr_2 = 'drg_monthly_sales_all_franchise')
)
select distinct
				'DRG_CSS'::VARCHAR(30) AS src_fran,
                '000000'::integer fisc_mo_id,   ---added 000000 as cannot insert 'NULL' in PK column
                t1.prod_cd, 
				t1.hrmnzd_prod_cd,    
                t1.mfr_nm, 
                t1.div, 
                t1.pltf,
				null::integer revn,
				null::integer unit,
                case
					when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case
                    when e.data_src_cd is not null 
					then 'Y' 
					else 'N'
                end excln_ind,					
                'Y' map_no_drg_ind,
                'CSS_QC_EXT-PRODUCT_IN_MAPPING_NOT_IN_DRG_SALES' crtd_by,
                current_timestamp crt_dttm      
from			codes_in_map_not_in_drg t1
left outer join prod_code_in_drg_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd
left outer join (select distinct data_src_cd, manufacturer from gbi_stg.exclusion_css) e	
on           	upper(trim(t1.prod_cd)) = upper(trim(e.data_src_cd))
and				upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))	
where 			t1.prod_cd is not null
;
 
commit;

-------------CSS QC for DRG (EXT) Sales Ends---------------

-------------CSS QC for OneMD (INT) Sales Starts-----------

-- codes_in_css_not_in_map

-- codes in CSS INT Sales file (css_jpi_bwi_acct_sales) but not in mapping file (dim_prod_CSS)

delete from gbi_wrk.intrm_new_prod where src_fran='ONEMD_CSS' 
and css_no_map_ind = 'Y';

commit;

insert into gbi_wrk.intrm_new_prod 
(
         src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , css_no_map_ind
       , crtd_by
       , Crt_Dttm
) 
with codes_in_css_not_in_map as
(
                select distinct 'ONEMD_CSS'::varchar(30) as src_fran,
                                cast(cjbas.fiscal_year_number || cjbas.fiscal_month as numeric) as fisc_mo_id,
                                cjbas.product_code as prod_cd,
                                cjbas.company_name as mfr_nm, 
                                sum(cjbas.sales_amt) revn,
								sum(cjbas.eaches_qty) unit,  
                                null mfr_diff_ind,   
                    			'Y' css_no_map_ind, 
                    			'CSS_QC_JPIBWI-PRODUCT_IN_1MD_SALES_NOT_IN_MAPPING' crtd_by,
                    			current_timestamp Crt_Dttm 
    			from 			gbi_stg.css_jpi_bwi_acct_sales cjbas
				where not exists															
					(select 1 from gbi_stg.dim_prod_css css
					where upper(trim(css.jnj_code)) = upper(trim(cjbas.product_code))
					--and css.data_src = 'DRG'
					and css.jnj_code is not null
					)				
--                 left outer join gbi_stg.dim_prod_css css
--                on 				upper(trim(css.jnj_code)) = upper(trim(cjbas.product_code))
--                where 			css.crtd_by is null    										             
                group by 		fisc_mo_id,
								prod_cd,
								mfr_nm 
)
select 	distinct
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.revn,
                t1.unit,
                t1.mfr_diff_ind,
                case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,
                t1.css_no_map_ind,
                t1.crtd_by,
                t1.Crt_Dttm
from 			codes_in_css_not_in_map t1
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_css) e
on          	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and 			upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))	
where 			t1.prod_cd is not null;
 
commit;


--map_no_css_ind
--codes_in_map_not_in_css
--codes in mapping file (dim_prod_CSS) but not in CSS INT Sales file (css_jpi_bwi_acct_sales)

delete from gbi_wrk.intrm_new_prod where src_fran='ONEMD_CSS'
and map_no_css_ind = 'Y';

commit;

insert into gbi_wrk.intrm_new_prod
(
         src_fran
       , fisc_mo_id
       , prod_cd
	   , hrmnzd_prod_cd 
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_css_ind
       , crtd_by
       , crt_dttm
)
with codes_in_map_not_in_css as
(
                select distinct
                                trim(css.jnj_code) prod_cd, 
								trim(css.hrmnzd_prod_cd) hrmnzd_prod_cd,
                                trim(css.manufacturer) mfr_nm, 
                                COALESCE (trim(css.division),'N/A') div, 
                                COALESCE (trim(css.platform),'N/A') pltf
                from gbi_stg.dim_prod_css css
				WHERE css.data_src = 'DRG'
				and not exists														
	                (
	                select 	1
	                from  	gbi_stg.css_jpi_bwi_acct_sales dtcas
	                where 	upper(trim(css.jnj_code)) = upper(trim(dtcas.product_code))
	                )				
--                left outer join gbi_stg.css_jpi_bwi_acct_sales dtcas
--                on upper(trim(css.jnj_code)) = upper(trim(dtcas.product_code)) 
--                where dtcas.crt_dttm is null                      					 
),
prod_code_in_css_with_diff_mfr as
(
                select trim(prod_cd) as prod_cd from codes_in_map_not_in_css
                intersect
                select trim(product_code) as prod_cd from gbi_stg.css_jpi_bwi_acct_sales
)
select 
                'ONEMD_CSS'::varchar(30) as src_fran,
                '000000'::integer fisc_mo_id,
                t1.prod_cd,
				t1.hrmnzd_prod_cd,
                t1.mfr_nm, 
                t1.div, 
                t1.pltf,
    			null::integer revn,
    			null::integer unit,
                case
                    when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,				
                'Y' map_no_css_ind,
                'CSS_QC_JPIBWI-PRODUCT_IN_MAPPING_NOT_IN_1MD_SALES' crtd_by,
                current_timestamp crt_dttm      
from 			codes_in_map_not_in_css t1
left outer join prod_code_in_css_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd 
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_css) e		
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and				upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null;
 
commit;

-------------CSS QC for OneMD (INT) Sales Ends-------------

-------------CSS QC for EXT & INT Sales Ends---------------
 

end;
