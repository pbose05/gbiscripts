/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on
begin;
-------------DPS QC (EXT & INT Sales) Starts---
-------------DPS QC for DRG Sales Starts-------
-- codes in External Sales File (drg_monthly_sales_all_franchise) but not in mapping file (dim_prod_dps)
----drg_no_map_ind
delete from gbi_wrk.intrm_new_prod where src_fran = 'DRG_DPS' and drg_no_map_ind = 'Y';

commit;

insert into gbi_wrk.intrm_new_prod
(
        src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , drg_no_map_ind
       , Crtd_By
       , Crt_Dttm
)    
with 
codes_in_drg_not_in_map as
(
                select distinct
                                'DRG_DPS'::varchar(30) as src_fran,
                                q1.fisc_mo_id,
                                q1.prod_cd,
                                q1.mfr_nm,
                                q1.div,
                                q1.pltf,
                                q1.revn,
                                q1.unit,
                                case
                                    when e.data_src_cd is not null 
									then 'Y' 
									else 'N'
                                end excln_ind,
                                q1.drg_no_map_ind,
                                q1.Crtd_By,
                                q1.Crt_Dttm
                from
                (
                                select distinct
                                                cast(substring(replace(cast(dms.date as varchar), '-','') from 1 for 6) as integer) fisc_mo_id,
                                                trim(dms.sku) prod_cd,
                                                trim(dms.manufacturer) mfr_nm,
												COALESCE (trim(dms.division),'N/A') div,
												COALESCE (trim(dms.platform),'N/A') pltf,												
                                                sum(dms.revenue) revn,
                                                sum(dms.units) unit,
                                                'Y'::char drg_no_map_ind,
                                                'DPS_QC_EXT-PRODUCT_IN_DRG_SALES_NOT_IN_MAPPING'::varchar(50) Crtd_by,
                                                current_timestamp Crt_Dttm
                                from 
                                                gbi_stg.drg_monthly_sales_all_franchise dms
								where 			franchise in 	(select distinct busn_rule_gr_3 from gbi_cdh.busn_rule 
																where busn_rule = 'external_sales' and busn_rule_gr_1 = 'DPS' 
																and busn_rule_gr_2 = 'drg_monthly_sales_all_franchise')			
                                and not exists
												(select 1 from gbi_stg.dim_prod_dps dps
												where 	upper(trim(dps.data_src_cd)) = upper(trim(dms.sku))
												and 	upper(trim(dps.manufacturer)) = upper(trim(dms.manufacturer))
												and 	dps.data_src = 'DRG'  
												)
--                left outer join 	gbi_stg.dim_prod_dps dps
--                on 				upper(trim(dps.jnj_code)) = upper(trim(djrs.product_code)) 
--                where 			dps.crtd_by is null
                                group by 
                                                dms.date,
                                                dms.sku,
                                                dms.manufacturer,
												trim(dms.division),
												trim(dms.platform) 												
                )q1
                left outer join (SELECT DISTINCT data_src_cd,manufacturer FROM gbi_stg.exclusion_dps) e  
                on           	upper(trim(q1.prod_cd)) = upper(trim(e.data_src_cd))
                and 			upper(trim(q1.mfr_nm)) = upper(trim(e.manufacturer))
),
prod_code_in_map_with_diff_mfr as
(
                select trim(prod_cd) prod_cd from codes_in_drg_not_in_map
                intersect
                select trim(data_src_cd) prod_cd from gbi_stg.dim_prod_dps
				WHERE data_src = 'DRG' 
)
select distinct
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.div,
                t1.pltf,
                t1.revn,
                t1.unit,
                case 
                    when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
                t1.excln_ind,
                t1.drg_no_map_ind,
                t1.Crtd_By,
                t1.Crt_Dttm
from      
                codes_in_drg_not_in_map t1
left outer join 
                prod_code_in_map_with_diff_mfr t2
on           	upper(trim(t1.prod_cd)) = upper(trim(t2.prod_cd))
where 			t1.prod_cd is not null
;
 
commit;

-- codes in mapping file (dim_prod_dps) but not in External Sales File (drg_monthly_sales_all_franchise)

----map_no_drg_ind

delete from gbi_wrk.intrm_new_prod where src_fran = 'DRG_DPS' and map_no_drg_ind = 'Y';

commit;
 
insert into gbi_wrk.intrm_new_prod 
(
         src_fran
       , fisc_mo_id
       , prod_cd
	   , hrmnzd_prod_cd
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_drg_ind
       , crtd_by
       , crt_dttm
)
with 
codes_in_map_not_in_drg as
(
                select distinct
                                trim(dps.data_src_cd) prod_cd, trim(dps.hrmnzd_prod_cd) hrmnzd_prod_cd,
                                trim(dps.manufacturer) mfr_nm, 
                                COALESCE (trim(dps.division),'N/A') div, 	
                                COALESCE (trim(dps.platform),'N/A') pltf 	
                from gbi_stg.dim_prod_dps dps
				WHERE dps.data_src = 'DRG'
                and not exists 
                (
                select 	1
                from 	gbi_stg.drg_monthly_sales_all_franchise dms
                where 	franchise in 	(select distinct busn_rule_gr_3 from gbi_cdh.busn_rule 
										where busn_rule = 'external_sales' and busn_rule_gr_1 = 'DPS' 
										and busn_rule_gr_2 = 'drg_monthly_sales_all_franchise')			
                and     upper(trim(dps.data_src_cd)) = upper(trim(dms.sku)) 
				and 	upper(trim(dps.manufacturer)) = upper(trim(dms.manufacturer))
                )
),
prod_code_in_drg_with_diff_mfr as
(
                select trim(prod_cd) prod_cd from codes_in_map_not_in_drg
                intersect
                select trim(sku) prod_cd from gbi_stg.drg_monthly_sales_all_franchise 
                where 	franchise in 	(select distinct busn_rule_gr_3 from gbi_cdh.busn_rule 
										where busn_rule = 'external_sales' and busn_rule_gr_1 = 'DPS' 
										and busn_rule_gr_2 = 'drg_monthly_sales_all_franchise')
)
select distinct
				'DRG_DPS'::VARCHAR(30) AS src_fran,
                '000000'::integer fisc_mo_id,   ---added 000000 as cannot insert 'NULL' in PK column
                t1.prod_cd, 
				t1.hrmnzd_prod_cd,    				
                t1.mfr_nm, 
                t1.div, 
                t1.pltf,
				null::integer revn,
				null::integer unit,
                case
					when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case 
                    when e.data_src_cd is not null 
					then 'Y' 
					else 'N'
                end excln_ind,					
                'Y' map_no_drg_ind,
                'DPS_QC_EXT-PRODUCT_IN_MAPPING_NOT_IN_DRG_SALES' crtd_by,
                current_timestamp crt_dttm      
from			codes_in_map_not_in_drg t1
left outer join prod_code_in_drg_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd
left outer join (select distinct data_src_cd, manufacturer from gbi_stg.exclusion_dps) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.data_src_cd))
and 			upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null
;
 
commit;

-------------DPS QC for DRG Sales Ends------------------------- 

-------------DPS QC for OneMD Sales Starts---------------------

-- codes in 4 dps internal sales files (tables) but not in mapping file (dim_prod_dps)

-----dps_no_map_ind

delete from gbi_wrk.intrm_new_prod where src_fran = 'ONEMD_DPS' and dps_no_map_ind = 'Y';

commit;
 
---gbi_stg.dps_joint_recon_sales----
 
insert into gbi_wrk.intrm_new_prod 
(
		 src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , dps_no_map_ind
       , Crtd_By
       , Crt_Dttm
) 
with codes_in_dps_not_in_map as
(
                select distinct 'ONEMD_DPS'::varchar(30) as src_fran,
                                cast(djrs.fiscal_year_number || djrs.fiscal_month as numeric) as fisc_mo_id,
                                trim(djrs.product_code) as prod_cd,
                                trim(djrs.company_name) as mfr_nm, 
                                sum(djrs.sales_amt) revn,
								sum(djrs.eaches_qty) unit,  								
                                null mfr_diff_ind,   
								'Y' dps_no_map_ind, 
								'DPS_QC_JR-PRODUCT_IN_1MD_SALES_NOT_IN_MAPPING' Crtd_By,
								current_timestamp Crt_Dttm 
				from 			gbi_stg.dps_joint_recon_sales djrs
				where not exists
					(select 1 from gbi_stg.dim_prod_dps dps
					where upper(trim(dps.jnj_code)) = upper(trim(djrs.product_code))
					and dps.jnj_code is not null
					)
--                left outer join gbi_stg.dim_prod_dps dps
--                on upper(trim(dps.jnj_code)) = upper(trim(djrs.product_code)) 
--                where 			dps.crtd_by is null
                group by 		fisc_mo_id,
                                prod_cd, 
								mfr_nm 
)
select 	distinct
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.revn,
                t1.unit,
                t1.mfr_diff_ind,
                case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,
                t1.dps_no_map_ind,
                t1.crtd_by,
                t1.Crt_Dttm
from			codes_in_dps_not_in_map t1
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_dps) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and 			upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null; 
 
commit;
 
 
------gbi_stg.dps_mitek_acct_sales-------
 
insert into gbi_wrk.intrm_new_prod 
(
        src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , revn
	   , unit
       , mfr_diff_ind
       , excln_ind
       , dps_no_map_ind
       , Crtd_By
       , Crt_Dttm
) 
with codes_in_dps_not_in_map as
(
                select 	distinct 'ONEMD_DPS'::varchar(30) as src_fran,
                                cast(dmas.fiscal_year_number || dmas.fiscal_month as numeric) as fisc_mo_id,
                                trim(dmas.product_code) as prod_cd,
                                trim(dmas.company_name) as mfr_nm, 
                                sum(dmas.sales_amt) revn,
								sum(dmas.eaches_qty) unit,  							
                                null mfr_diff_ind,   
								'Y' dps_no_map_ind, 
								'DPS_QC_MITEK-PRODUCT_IN_1MD_SALES_NOT_IN_MAPPING' Crtd_By,
								current_timestamp Crt_Dttm 
				from 			gbi_stg.dps_mitek_acct_sales dmas
				where not exists
					(select 1 from gbi_stg.dim_prod_dps dps
					where upper(trim(dps.jnj_code)) = upper(trim(dmas.product_code))
					and dps.jnj_code is not null
					)				
--                left outer join gbi_stg.dim_prod_dps dps
--                on upper(trim(dps.jnj_code)) = upper(trim(dmas.product_code)) 
--                where 			dps.crtd_by is null
                group by 		fisc_mo_id,
                                prod_cd, 
								mfr_nm 
)
select 	distinct
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.revn,
                t1.unit,
                t1.mfr_diff_ind,
                case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,
                t1.dps_no_map_ind,
                t1.Crtd_By,
                t1.Crt_Dttm
from 			codes_in_dps_not_in_map t1
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_dps) e  
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and 			upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null;     

commit;
 
----gbi_stg.dps_spine_acct_sales-----------

insert into gbi_wrk.intrm_new_prod 
(
        src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , dps_no_map_ind
       , Crtd_By
       , Crt_Dttm
)
with codes_in_dps_not_in_map as
(
                select distinct 'ONEMD_DPS'::varchar(30) as src_fran,
                                cast(dsas.fiscal_year_number || dsas.fiscal_month as numeric) as fisc_mo_id,
                                trim(dsas.product_code) as prod_cd,
                                trim(dsas.company_name) as mfr_nm, 
                                sum(dsas.sales_amt) revn,
								sum(dsas.eaches_qty) unit,  								
                                null mfr_diff_ind,   
								'Y' dps_no_map_ind, 
								'DPS_QC_SPINE-PRODUCT_IN_1MD_SALES_NOT_IN_MAPPING' Crtd_By,
								current_timestamp Crt_Dttm 
				from 			gbi_stg.dps_spine_acct_sales dsas
				where not exists
					(select 1 from gbi_stg.dim_prod_dps dps
					where upper(trim(dps.jnj_code)) = upper(trim(dsas.product_code))
					and dps.jnj_code is not null
					)				
--                left outer join gbi_stg.dim_prod_dps dps
--                on upper(trim(dps.jnj_code)) = upper(trim(dsas.product_code))
--                where 			dps.crtd_by is null              
                group by 		fisc_mo_id,
                                prod_cd, 
								mfr_nm 
)
select 	distinct
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.revn,
                t1.unit,
                t1.mfr_diff_ind,
                case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,
                t1.dps_no_map_ind,
                t1.Crtd_By,
                t1.Crt_Dttm
from 			codes_in_dps_not_in_map t1
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_dps) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and 			upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null		
;     
 
commit;
 
---------gbi_stg.dps_trauma_cmf_acct_sales---------

insert into gbi_wrk.intrm_new_prod
(
        src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , dps_no_map_ind
       , Crtd_By
       , Crt_Dttm
) 
with codes_in_dps_not_in_map as
(
                select distinct	'ONEMD_DPS'::varchar(30) as src_fran,
                                cast(dtcas.fiscal_year_number || dtcas.fiscal_month as numeric) as fisc_mo_id,
                                trim(dtcas.product_code) as prod_cd,
                                trim(dtcas.company_name) as mfr_nm, 
                                sum(dtcas.sales_amt) revn,
								sum(dtcas.eaches_qty) unit, 								
                                null mfr_diff_ind,   
								'Y' dps_no_map_ind, 
								'DPS_QC_TRCMF-PRODUCT_IN_1MD_SALES_NOT_IN_MAPPING' Crtd_By,
								current_timestamp Crt_Dttm 
				from 			gbi_stg.dps_trauma_cmf_acct_sales dtcas
				where not exists
					(select 1 from gbi_stg.dim_prod_dps dps
					where upper(trim(dps.jnj_code)) = upper(trim(dtcas.product_code))
					and dps.jnj_code is not null
					)				
--                left outer join gbi_stg.dim_prod_dps dps
--                on 				upper(trim(dps.jnj_code)) = upper(trim(dtcas.product_code))
--                where 			dps.crtd_by is null              
                group by 		fisc_mo_id,
                                prod_cd, 
								mfr_nm 
)
select  distinct	   
                t1.src_fran,
				t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.revn,
                t1.unit,
                t1.mfr_diff_ind,
                case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,
                t1.dps_no_map_ind,
                t1.Crtd_By,
                t1.Crt_Dttm
from 			codes_in_dps_not_in_map t1
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_dps) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and 			upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null;
 
commit;


-- codes in mapping file (dim_prod_dps) but not in 4 DPS internal sales files (tables)

------map_no_dps_ind----------
 
 
delete from gbi_wrk.intrm_new_prod where src_fran = 'ONEMD_DPS' and  map_no_dps_ind = 'Y';

commit;
 
--------gbi_stg.dps_joint_recon_sales------------
 
insert into gbi_wrk.intrm_new_prod 
(
         src_fran
       , fisc_mo_id
       , prod_cd
	   , hrmnzd_prod_cd 
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_dps_ind
       , crtd_by
       , crt_dttm
)
with codes_in_map_not_in_dps as
(
                select distinct
                                trim(dps.jnj_code) prod_cd, 
								trim(dps.hrmnzd_prod_cd) hrmnzd_prod_cd,								
                                trim(dps.manufacturer) mfr_nm, 
                                COALESCE (trim(dps.division),'N/A') div, 
                                COALESCE (trim(dps.platform),'N/A') pltf
                from 			gbi_stg.dim_prod_dps dps
				where 			dps.data_src = 'DRG'				
				and not exists 
					(
					select 	1
					from  	gbi_stg.dps_joint_recon_sales dps_jr
					where 	upper(trim(dps.jnj_code)) = upper(trim(dps_jr.product_code))
					)
--                left outer join gbi_stg.dps_joint_recon_sales djrs
--                on 				upper(trim(dps.jnj_code)) = upper(trim(djrs.product_code)) 
--                where 			djrs.crt_dttm is null
),
prod_code_in_dps_with_diff_mfr as
(
                select trim(prod_cd) as prod_cd from codes_in_map_not_in_dps
                intersect
                select trim(product_code) as prod_cd from gbi_stg.dps_joint_recon_sales  
)
select 	
                'ONEMD_DPS'::varchar(30) as src_fran,
                '000000'::integer fisc_mo_id,
                t1.prod_cd,
				t1.hrmnzd_prod_cd,				
                t1.mfr_nm, 
                t1.div, 
                t1.pltf,
    			null::integer revn,
    			null::integer unit,
                case
                    when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,
                'Y' map_no_dps_ind,
                'DPS_QC_JR-PRODUCT_IN_MAPPING_NOT_IN_1MD_SALES' crtd_by,
                current_timestamp crt_dttm      
from 			codes_in_map_not_in_dps t1
left outer join prod_code_in_dps_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd 
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_dps) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and				upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null;
 
commit;
-----------gbi_stg.dps_mitek_acct_sales------------

insert into gbi_wrk.intrm_new_prod
(
        src_fran
       , fisc_mo_id
       , prod_cd
	   , hrmnzd_prod_cd	   
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_dps_ind
       , crtd_by
       , crt_dttm
)
with codes_in_map_not_in_dps as
(
                select distinct
                                trim(dps.jnj_code) prod_cd,
								trim(dps.hrmnzd_prod_cd) hrmnzd_prod_cd,								
                                trim(dps.manufacturer) mfr_nm, 
                                COALESCE (trim(dps.division),'N/A') div, 
                                COALESCE (trim(dps.platform),'N/A') pltf
				from 			gbi_stg.dim_prod_dps dps
				where not exists 
                (
                select 	1
                from  	gbi_stg.dps_mitek_acct_sales dps_m
                where 	upper(trim(dps.jnj_code)) = upper(trim(dps_m.product_code))
                )
				and dps.data_src = 'DRG'				
--                left outer join gbi_stg.dps_mitek_acct_sales dmas
--                on upper(trim(dps.jnj_code)) = upper(trim(dmas.product_code)) 
--                where dmas.crt_dttm is null
),
prod_code_in_dps_with_diff_mfr as
(
                select trim(prod_cd) as prod_cd from codes_in_map_not_in_dps
                intersect
                select trim(product_code) as prod_cd from gbi_stg.dps_mitek_acct_sales   
)
select 
                'ONEMD_DPS'::varchar(30) as src_fran,
                '000000'::integer fisc_mo_id,
                t1.prod_cd,
				t1.hrmnzd_prod_cd,				
                t1.mfr_nm, 
                t1.div, 
                t1.pltf,
    			null::integer revn,
    			null::integer unit,
                case
                    when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,				
                'Y' map_no_dps_ind,
                'DPS_QC_MITEK-PRODUCT_IN_MAPPING_NOT_IN_1MD_SALES' crtd_by,
                current_timestamp crt_dttm      
from 			codes_in_map_not_in_dps t1
left outer join prod_code_in_dps_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_dps) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and				upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null;

commit;
----------------gbi_stg.dps_spine_acct_sales-----------------
insert into gbi_wrk.intrm_new_prod 
(
        src_fran
       , fisc_mo_id
       , prod_cd
	   , hrmnzd_prod_cd	   
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_dps_ind
       , crtd_by
       , crt_dttm
)
with codes_in_map_not_in_dps as
(
                select distinct
                                trim(dps.jnj_code) prod_cd,
								trim(dps.hrmnzd_prod_cd) hrmnzd_prod_cd,								
                                trim(dps.manufacturer) mfr_nm, 
                                COALESCE (trim(dps.division),'N/A') div, 
                                COALESCE (trim(dps.platform),'N/A') pltf
                from 			gbi_stg.dim_prod_dps dps 
				where not exists 
                (
                select 	1
                from  	gbi_stg.dps_spine_acct_sales dps_s
                where 	upper(trim(dps.jnj_code)) = upper(trim(dps_s.product_code))
                )	
				and dps.data_src = 'DRG'				
--                left outer join gbi_stg.dps_spine_acct_sales dsas
--                on upper(trim(dps.jnj_code)) = upper(trim(dsas.product_code)) 
--                where dsas.crt_dttm is null
),
prod_code_in_dps_with_diff_mfr as
(
                select trim(prod_cd) as prod_cd from codes_in_map_not_in_dps
                intersect
                select trim(product_code) as prod_cd from gbi_stg.dps_spine_acct_sales   
)
select 
                'ONEMD_DPS'::varchar(30) as src_fran,
                '000000'::integer fisc_mo_id,
                t1.prod_cd, 
				t1.hrmnzd_prod_cd,				
                t1.mfr_nm, 
                t1.div, 
                t1.pltf,
    			null::integer revn,
    			null::integer unit,
                case
                    when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,				
                'Y' map_no_dps_ind,
                'DPS_QC_SPINE-PRODUCT_IN_MAPPING_NOT_IN_1MD_SALES' crtd_by,
                current_timestamp crt_dttm      
from 			codes_in_map_not_in_dps t1
left outer join prod_code_in_dps_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd 
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_dps) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and				upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null;

commit;

------------gbi_stg.dps_trauma_cmf_acct_sales-------------

insert into gbi_wrk.intrm_new_prod 
(
        src_fran
       , fisc_mo_id
       , prod_cd
	   , hrmnzd_prod_cd	   
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_dps_ind
       , crtd_by
       , crt_dttm
)
with codes_in_map_not_in_dps as
(
                select distinct
                                trim(dps.jnj_code) prod_cd,
								trim(dps.hrmnzd_prod_cd) hrmnzd_prod_cd,								
                                trim(dps.manufacturer) mfr_nm, 
                                COALESCE (trim(dps.division),'N/A') div, 
                                COALESCE (trim(dps.platform),'N/A') pltf
                from gbi_stg.dim_prod_dps dps
				where not exists 
					(
					select 	1
					from  	gbi_stg.dps_trauma_cmf_acct_sales dps_trcmf
					where 	upper(trim(dps.jnj_code)) = upper(trim(dps_trcmf.product_code))
					)
				and dps.data_src = 'DRG'					
--                left outer join gbi_stg.dps_trauma_cmf_acct_sales dtcas
--                on upper(trim(dps.jnj_code)) = upper(trim(dtcas.product_code)) 
--                where dtcas.crt_dttm is null
),
prod_code_in_dps_with_diff_mfr as
(
                select trim(prod_cd) as prod_cd from codes_in_map_not_in_dps
                intersect
                select trim(product_code) as prod_cd from gbi_stg.dps_trauma_cmf_acct_sales
)
select distinct
                'ONEMD_DPS'::varchar(30) as src_fran,
                '000000'::integer fisc_mo_id,
                t1.prod_cd,
				t1.hrmnzd_prod_cd,				
                t1.mfr_nm, 
                t1.div, 
                t1.pltf,
				null::integer revn,
				null::integer unit,
                case
                    when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case 
                    when e.jnj_code is not null 
					then 'Y' 
					else 'N'
                end excln_ind,				
                'Y' map_no_dps_ind,
                'DPS_QC_TRCMF-PRODUCT_IN_MAPPING_NOT_IN_1MD_SALES' crtd_by,
                current_timestamp crt_dttm      
from 			codes_in_map_not_in_dps t1
left outer join prod_code_in_dps_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd 
left outer join (select distinct jnj_code, manufacturer from gbi_stg.exclusion_dps) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.jnj_code))
and				upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null;

commit;
-------------DPS QC OneMD Ends---------------

-------------DPS QC (EXT & INT Sales) Ends---

end;
