/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on 

-------------ETH QC Starts-------------------------
begin;

-----Start-----ETH QC for Internal (EUSS) sales vs Mapping File--------

----Start----eth_no_map_ind------

delete from gbi_wrk.intrm_new_prod where src_fran = 'EUSS_ETH' and eth_no_map_ind = 'Y';

commit;

 
insert into gbi_wrk.intrm_new_prod 
(
         src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , eth_no_map_ind
       , Crtd_By
       , Crt_Dttm
) 
with codes_in_eth_not_in_map as
(
                select 
                                'EUSS_ETH'::varchar(30) as src_fran,                        --added this fields as Source (EUSS/DRG/GHX) & Franchise (ETH/DPS/CSS) combinations
                                eis.fisc_mo_id,
                                eis.prod_base_cd prod_cd,
                                'Ethicon' mfr_nm, 
                                sum(eis.aggr_eus_prod_sls_amt) revn,
								sum(eis.aggr_nt_prod_qty) UNIT,
								'Y' eth_no_map_ind,
								'ETH_QC_INT-PRODUCT_IN_EUSS_SALES_NOT_IN_MAPPING' Crtd_By,
								current_timestamp Crt_Dttm 
                from
                                gbi_stg.eth_internal_sales_drg eis
				where not exists
								(select 1 from gbi_stg.dim_prod_eth eth
								where 	upper(trim(eth.jnj_code)) = upper(trim(eis.prod_base_cd))
								and 	upper(trim(eth.company_grouping)) = 'ETHICON'
								and		eth.eth_ind is not null                             --Added on 01/29/2018
								)
				and eis.prod_base_cd is not null   											--prod_cd can't be null, added this filter condition since euss_sub_national_sales is having 'null' prod_cd for 18 records
                group by
                                eis.fisc_mo_id,
                                eis.prod_base_cd
)
select 
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.revn,
                t1.unit,
				null::char(1) mfr_diff_ind,
                case 
                    when e.harmonized_product_code is not null   ----use jnj_code in future if the format of Exclusion file changes to be similar to DPS/CSS Exclusion files 
					then 'Y' 
					else 'N'
                end excln_ind,
                t1.eth_no_map_ind,
                t1.Crtd_By,
                t1.Crt_Dttm
from			codes_in_eth_not_in_map t1
left outer join (select distinct harmonized_product_code, manufacturer from gbi_stg.exclusion_eth) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.harmonized_product_code))
and 			upper(trim(e.manufacturer))='ETHICON'
where 			t1.prod_cd is not null
;
 
commit;
----End----eth_no_map_ind------


----Start----map_no_eth_ind------

/*Added a new column map_no_eth_ind to existing gbi_wrk.intrm_new_prod table to capture this data*/
delete from gbi_wrk.intrm_new_prod where src_fran = 'EUSS_ETH' and  map_no_eth_ind = 'Y';

commit;
 
insert into gbi_wrk.intrm_new_prod 
(
         src_fran
       , fisc_mo_id
       , prod_cd
	   , hrmnzd_prod_cd
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_eth_ind
       , crtd_by
       , crt_dttm
)
with 
codes_in_map_not_in_eth as
(
                select 			trim(eth.jnj_code) prod_cd, 
								trim(eth.hrmnzd_prod_cd) hrmnzd_prod_cd,				
                                trim(eth.manufacturer) mfr_nm, 
                                COALESCE (trim(eth.division),'N/A') div, 
                                COALESCE (trim(eth.platform),'N/A') pltf
                from            gbi_stg.dim_prod_eth eth
                where not exists 
								(
								select 	1
								from 	gbi_stg.eth_internal_sales_drg eis
								where   upper(trim(eth.jnj_code)) = upper(trim(eis.prod_base_cd)) 
								)
				and 			upper(trim(eth.company_grouping)) = 'ETHICON'
				and 			eth.eth_ind is not null
),
prod_code_in_euss_with_diff_mfr as
(
                select trim(prod_cd) prod_cd from codes_in_map_not_in_eth
                intersect
                select trim(prod_base_cd) prod_cd from gbi_stg.eth_internal_sales_drg              
)
select 
				'EUSS_ETH'::VARCHAR(30) AS src_fran,
                '000000'::integer fisc_mo_id,   ---added 000000 as cannot insert 'NULL' in PK column
                t1.prod_cd,
				t1.hrmnzd_prod_cd, 
                t1.mfr_nm, 
                t1.div, 
                t1.pltf,
				null::integer revn,
				null::integer unit,
                case
					when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case 
                    when e.harmonized_product_code is not null 		--use jnj_code in future if the format of Exclusion file changes to be similar to DPS/CSS Exclusion files
					then 'Y' 
					else 'N'
                end excln_ind,					
                'Y' map_no_eth_ind,
                'ETH_QC_INT-PRODUCT_IN_MAPPING_NOT_IN_EUSS_SALES' crtd_by,
                current_timestamp crt_dttm      
from
                codes_in_map_not_in_eth t1
left outer join prod_code_in_euss_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd
left outer join (select distinct harmonized_product_code, manufacturer from gbi_stg.exclusion_eth) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.harmonized_product_code))
where 			t1.prod_cd is not null;
 
commit;

----End----map_no_eth_ind------

-----End-----ETH QC for Internal (EUSS) sales vs Mapping File--------

-----Start-----ETH QC for External (DRG) sales vs Mapping File--------

--------Ethicon DRG QC Begins------------

-- codes in drg monthly sales but not in mapping file
delete from gbi_wrk.intrm_new_prod where src_fran = 'DRG_ETH' and drg_no_map_ind = 'Y';

commit;

insert into gbi_wrk.intrm_new_prod 
(
        src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , drg_no_map_ind
       , Crtd_By
       , Crt_Dttm
) 
with 
codes_in_drg_not_in_map as
(
                select
                                'DRG_ETH'::varchar(30) as src_fran,
                                q1.fisc_mo_id,
                                q1.prod_cd,
                                q1.mfr_nm,
                                q1.div,
                                q1.pltf,
                                q1.revn,
                                q1.unit,
                                case
                                    when e.harmonized_product_code is not null 		--use data_src_cd in future if the format of Exclusion file changes to be similar to DPS/CSS Exclusion files
									then 'Y' 
									else 'N'
                                end excln_ind,
                                q1.drg_no_map_ind,
                                q1.Crtd_By,
                                q1.Crt_Dttm
                from
                (
                                select 
                                                cast(substring(replace(cast(dms.date as varchar), '-','') from 1 for 6) as integer) fisc_mo_id,
                                                trim(dms.sku) prod_cd,
                                                trim(dms.manufacturer) mfr_nm,
                                                COALESCE (trim(dms.division),'N/A') div,
                                                COALESCE (trim(dms.platform),'N/A') pltf,
                                                sum(dms.revenue) revn,
                                                sum(dms.units) unit,
                                                'Y' drg_no_map_ind,
                                                'ETH_QC_EXT-PRODUCT_IN_DRG_SALES_NOT_IN_MAPPING' Crtd_by,
                                                current_timestamp Crt_Dttm
                                from 
                                                gbi_stg.drg_monthly_sales dms
                                where not exists
                                (
                                                select 	1
                                                from 	gbi_stg.dim_prod_eth gpd
                                                where 	upper(trim(gpd.drg_code)) = upper(trim(dms.sku)) 
												and 	upper(trim(gpd.manufacturer)) = upper(trim(dms.manufacturer))
												and		gpd.drg_ind is not null                               
								)
								and trim(dms.platform) not in (select distinct trim(platform) from gbi_stg.dim_prod_eth where ghx_ind = '1')
                                group by 
                                                dms.date,
                                                dms.sku,
                                                dms.manufacturer,
                                                dms.division,
                                                dms.platform
                )q1
                left outer join (SELECT DISTINCT harmonized_product_code,manufacturer FROM gbi_stg.exclusion_eth) e  
                on           	upper(trim(q1.prod_cd)) = upper(trim(e.harmonized_product_code))
                and 			upper(trim(q1.mfr_nm)) = upper(trim(e.manufacturer))
),
prod_code_in_map_with_diff_mfr as
(
                select trim(prod_cd) prod_cd from codes_in_drg_not_in_map
                intersect
                select trim(drg_code) from gbi_stg.dim_prod_eth
				where drg_ind = '1' 
)
select
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.div,
                t1.pltf,
                t1.revn,
                t1.unit,
                case 
                    when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
                t1.excln_ind,
                t1.drg_no_map_ind,
                t1.Crtd_By,
                t1.Crt_Dttm
from      
                codes_in_drg_not_in_map t1
left outer join 
                prod_code_in_map_with_diff_mfr t2
on           	t1.prod_cd = t2.prod_cd;
 
commit;

-- codes in mapping file but not in drg monthly sales
 
delete from gbi_wrk.intrm_new_prod where src_fran = 'DRG_ETH' and  map_no_drg_ind = 'Y';

commit;
 
insert into gbi_wrk.intrm_new_prod 
(
         src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
	   , hrmnzd_prod_cd
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_drg_ind
       , crtd_by
       , crt_dttm
)
with 
codes_in_map_not_in_drg as
(
                select 
                                trim(gpd.drg_code) prod_cd,
								trim(gpd.hrmnzd_prod_cd) hrmnzd_prod_cd,
                                trim(gpd.manufacturer) mfr_nm, 
                                COALESCE (trim(gpd.division),'N/A') div, 
                                COALESCE (trim(gpd.platform),'N/A') pltf
                from 			gbi_stg.dim_prod_eth gpd
                where 	not exists 
			                (
			                select 	1
			                from 	gbi_stg.drg_monthly_sales dms
			                where 	upper(trim(gpd.drg_code)) = upper(trim(dms.sku)) 
							and   	upper(trim(gpd.manufacturer)) = upper(trim(dms.manufacturer))
			                )
                and gpd.drg_ind = '1'
),
prod_code_in_drg_with_diff_mfr as
(
                select trim(prod_cd) prod_cd from codes_in_map_not_in_drg
                intersect
                select trim(sku) prod_cd from gbi_stg.drg_monthly_sales
				where trim(platform) not in (select distinct trim(platform) from gbi_stg.dim_prod_eth where ghx_ind = '1')
)
select 	distinct
				'DRG_ETH'::VARCHAR(30) AS src_fran,
                '000000'::integer fisc_mo_id,   ---added 000000 as cannot insert 'NULL' in PK column
                t1.prod_cd,
                t1.mfr_nm,
				t1.hrmnzd_prod_cd,    --pk added				
                t1.div, 
                t1.pltf,
				null::integer revn,
				null::integer unit,
                case
					when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
				case 
                    when e.harmonized_product_code is not null      --use data_src_cd in future if the format of Exclusion file changes to be similar to DPS/CSS Exclusion files
					then 'Y' 
					else 'N'
                end excln_ind,					
                'Y' map_no_drg_ind,
                'ETH_QC_EXT-PRODUCT_IN_MAPPING_NOT_IN_DRG_SALES' crtd_by,
                current_timestamp crt_dttm      
from 			codes_in_map_not_in_drg t1
left outer join prod_code_in_drg_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd
left outer join (select distinct harmonized_product_code, manufacturer from gbi_stg.exclusion_eth) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.harmonized_product_code))
and				upper(trim(t1.mfr_nm)) = upper(trim(e.manufacturer))
where 			t1.prod_cd is not null;
 
commit;
--------Ethicon DRG QC Ends------------

-----End-----ETH QC for External (DRG) sales vs Mapping File--------

-----Start-----ETH QC for External (GHX) sales vs Mapping File------

--------Ethicon GHX QC Begins----------

 
-- codes in ghx quarterly sales but not in mapping file
 
delete from gbi_wrk.intrm_new_prod where src_fran = 'GHX_ETH' and   ghx_no_map_ind = 'Y';
 
commit;
 
insert into gbi_wrk.intrm_new_prod
(
        src_fran
       , fisc_mo_id
       , prod_cd
       , mfr_nm
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , ghx_no_map_ind
       , crtd_by
       , crt_dttm
) 
with 
codes_in_ghx_not_in_map as
(
                select
                                'GHX_ETH'::varchar(30) as src_fran,
                                q1.fisc_mo_id,
                                q1.prod_cd,
                                q1.mfr_nm,
                                q1.revn,
                                q1.unit,
                                case
                                    when e.harmonized_product_code is not null ----use data_src_cd in future if the format of Exclusion file changes to be similar to DPS/CSS Exclusion files
									then 'Y' 
									else 'N'
                                end excln_ind,
                                q1.ghx_no_map_ind,
                                q1.crtd_by,
                                q1.crt_dttm
                from
                (
                                select 
                                                ((gqs.year*100)+(gqs.quarter*3)) fisc_mo_id,
                                                trim(gqs.hpis_sku) prod_cd,
                                                trim(gqs.mfg_name) mfr_nm,
                                                sum(gqs.dollars) revn,
                                                sum(gqs.units) unit,
                                                'Y' ghx_no_map_ind,
                                                'ETH_QC_EXT-PRODUCT_IN_GHX_SALES_NOT_IN_MAPPING' crtd_by,
                                                current_timestamp crt_dttm
                                from 
                                                gbi_stg.ghx_quarterly_sales gqs
                                where not exists
                                (
                                                select 	1
                                                from 	gbi_stg.dim_prod_eth gpd
                                                where 	upper(trim(gpd.ghx_code)) = upper(trim(gqs.hpis_sku)) 
												and 	upper(trim(gpd.manufacturer)) = upper(trim(gqs.mfg_name))
												and		gpd.ghx_ind = '1'                               
                                )
								group by 
                                                fisc_mo_id,
                                                prod_cd,
                                                mfr_nm
                                                
                )q1
                left outer join (SELECT DISTINCT harmonized_product_code,manufacturer FROM gbi_stg.exclusion_eth) e
                on           	upper(trim(q1.prod_cd)) = upper(trim(e.harmonized_product_code))
                and 			upper(trim(q1.mfr_nm)) = upper(trim(e.manufacturer))
),
prod_code_in_map_with_diff_mfr as
(
                select trim(prod_cd) prod_cd from codes_in_ghx_not_in_map
                intersect
                select trim(ghx_code) prod_cd from gbi_stg.dim_prod_eth
				where ghx_ind = '1'
)
select
                t1.src_fran,
                t1.fisc_mo_id,
                t1.prod_cd,
                t1.mfr_nm,
                t1.revn,
                t1.unit,
                case 
                    when t2.prod_cd is not null 
					then 'Y' 
					else null
                end mfr_diff_ind,
                t1.excln_ind,
                t1.ghx_no_map_ind,
                t1.crtd_by,
                t1.crt_dttm
from      
                codes_in_ghx_not_in_map t1
left outer join 
                prod_code_in_map_with_diff_mfr t2
on           	t1.prod_cd = t2.prod_cd;
 
commit;
 
-- codes in mapping file but not in ghx quarterly sales file
 
delete from gbi_wrk.intrm_new_prod where src_fran = 'GHX_ETH' and map_no_ghx_ind = 'Y';
 
commit;
 
insert into gbi_wrk.intrm_new_prod 
(
        src_fran
       , fisc_mo_id
       , prod_cd
	   , hrmnzd_prod_cd
       , mfr_nm
       , div
       , pltf
       , revn
       , unit
       , mfr_diff_ind
       , excln_ind
       , map_no_ghx_ind
       , crtd_by
       , crt_dttm
)
with 
codes_in_map_not_in_ghx as
(
	select 
			trim(gpd.ghx_code) prod_cd, 
			trim(gpd.hrmnzd_prod_cd) hrmnzd_prod_cd,			
			trim(gpd.manufacturer) mfr_nm, 
			COALESCE (trim(gpd.division),'N/A') div, 
			COALESCE (trim(gpd.platform),'N/A') pltf
	from	gbi_stg.dim_prod_eth gpd
	where not exists 
					(select 1
					from	gbi_stg.ghx_quarterly_sales gqs
					where 	upper(trim(gpd.ghx_code)) = upper(trim(gqs.hpis_sku)) 
					and     upper(trim(gpd.manufacturer)) = upper(trim(gqs.mfg_name))
					)
	and gpd.ghx_ind = '1'
),
prod_code_in_ghx_with_diff_mfr as
(
	select trim(prod_cd) prod_cd from codes_in_map_not_in_ghx
	intersect
	select trim(hpis_sku) prod_cd from gbi_stg.ghx_quarterly_sales              
)
select 			'GHX_ETH'::varchar(30) as src_fran,
				'000000'::integer fisc_mo_id,                      ---added 000000 as cannot insert 'NULL' in PK column
				t1.prod_cd,
				t1.hrmnzd_prod_cd, 
				t1.mfr_nm, 
				t1.div, 
				t1.pltf,
				null::integer revn,
				null::integer unit,
				case
					 when t2.prod_cd is not null 
					 then 'Y' 
					 else null
				end mfr_diff_ind,
				case 
                    when e.harmonized_product_code is not null		--use data_src_cd in future if the format of Exclusion file changes to be similar to DPS/CSS Exclusion files 
					then 'Y' 
					else 'N'
                end excln_ind,					
				'Y' map_no_ghx_ind,
				'ETH_QC_EXT-PRODUCT_IN_MAPPING_NOT_IN_GHX_SALES' crtd_by,
				current_timestamp crt_dttm      
from 			codes_in_map_not_in_ghx t1
left outer join prod_code_in_ghx_with_diff_mfr t2
on 				t1.prod_cd = t2.prod_cd
left outer join (select distinct harmonized_product_code, manufacturer from gbi_stg.exclusion_eth) e
on           	upper(trim(t1.prod_cd)) = upper(trim(e.harmonized_product_code))
where 			t1.prod_cd is not null;
 
commit;
 
-------------Ethicon GHX QC Ends-----------------

-----End-----ETH QC for External (GHX) sales vs Mapping File--------

end;