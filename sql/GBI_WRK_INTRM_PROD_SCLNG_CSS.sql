/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin ;
delete from gbi_wrk.intrm_prod_sclng where src_fran = 'CSS';

commit;

insert into gbi_wrk.intrm_prod_sclng (
src_fran ,fran_cd ,mfr_nm , prod_cd , prod_nm , jnj_code , data_src_prod_cd , data_src ,
div , maj_pltf , pltf , sub_pltf , brnd_fmly , brnd , corp , oem_mfr_nm , comp_grpng , oem_reproc , 
main_cmpttr , matl ,  size , type , sclg_type , sclg_lvl ,sclg_lvl_val ,rev_scaling_src , rev_scaling , 
scaled_value , unit_scaling , rev_mult , unit_mult , src_sys_cd , crtd_by ,  updt_by , crt_dttm , updt_dttm) 

 SELECT 
    'CSS' src_fran ,	prod.franchise,  coalesce(prod.manufacturer::text,'NA'::text) manufacturer,
    coalesce(prod.product_code::text,'NA'::text) product_code,
    prod.product_name, coalesce(prod.jnj_code::text,'NA'::text) jnj_code,
    coalesce(prod.data_Src_cd::text,'NA'::text) data_Src_cd, 
	coalesce(prod.data_src::text,'NA'::text) data_src,	
    prod.division,    	  prod.major_platform,    
    prod.platform,		  prod.sub_platform,  		prod.brand_family,    	
    prod.brand,		      prod.corporation,    		prod.oem_manufacturer,
    prod.company_grouping,  prod.oem_reprocessed,	prod.main_competitor,   prod.material,
    prod.size,    			prod.type,
    scal.scaling_level AS sclg_type,
    scal.scaling_level AS sclg_lvl,
        CASE
            WHEN btrim(upper(scal.scaling_level::text)) = 'SUB PLATFORM'::text THEN prod.sub_platform
            WHEN btrim(upper(scal.scaling_level::text)) = 'PLATFORM'::text THEN prod.platform
            ELSE scal.scaling_level
        END AS sclg_lvl_val,
    scal.revenue_scaling_source AS sources,	    scal.revenue_scaling AS rev_method, scal.scaled_value ,
    scal.units_scaling AS unit_method,	    scal.revenue_multiplier,
    scal.unit_multiplier,					    'GBI'::text AS src_sys_cd,
	'GBI_WRK_INTRM_PROD_SCLNG_CSS' crtd_by,                      -- Added missing column in SQL for crtd_by,
    'GBI_WRK_INTRM_PROD_SCLNG_CSS' updt_by,                      -- Added missing column in SQL for updt_by, 

	now() crt_dttm,	now() updt_dttm
   FROM 
		(SELECT map.franchise,     map.division,            map.major_platform,
            map.platform,           map.sub_platform,        map.brand_family,
            map.brand,            	map.manufacturer,        map.corporation,
            map.oem_manufacturer,   map.company_grouping,    map.hrmnzd_prod_cd AS product_code,
            map.jnj_code ,          map.data_src_cd,            map.product_name,
            map.data_src,           map.oem_reprocessed,     map.main_competitor,
            map.material,           map.size,               map.type,
            map.r_num
           FROM ( SELECT css_stg_drg_prod.franchise,	 css_stg_drg_prod.division,
                    css_stg_drg_prod.major_platform,      css_stg_drg_prod.platform,
                    css_stg_drg_prod.sub_platform,        css_stg_drg_prod.brand_family,
                    css_stg_drg_prod.brand,               css_stg_drg_prod.manufacturer,
                    css_stg_drg_prod.corporation,         css_stg_drg_prod.oem_manufacturer,
                    css_stg_drg_prod.company_grouping,    css_stg_drg_prod.hrmnzd_prod_cd,
                    css_stg_drg_prod.jnj_code,            css_stg_drg_prod.product_name,
					css_stg_drg_prod.data_src_cd,         css_stg_drg_prod.data_src,
                    css_stg_drg_prod.oem_reprocessed,
                    css_stg_drg_prod.main_competitor,     css_stg_drg_prod.material,
                    css_stg_drg_prod.size,                css_stg_drg_prod.type,
                    css_stg_drg_prod.date_updated,        css_stg_drg_prod.date_added,
                    row_number() OVER (PARTITION BY css_stg_drg_prod.hrmnzd_prod_cd,css_stg_drg_prod.jnj_code,css_stg_drg_prod.manufacturer,css_stg_drg_prod.data_src_cd,css_stg_drg_prod.data_src ORDER BY css_stg_drg_prod.date_updated DESC) AS r_num
                   FROM ( SELECT franchise,   division,
                            major_platform,    platform,
                            sub_platform,      brand_family,
                            brand,             manufacturer,
                            corporation,       oem_manufacturer,  
                            company_grouping,  product_name,  
							hrmnzd_prod_cd,    jnj_code,          
                            data_src_cd,data_src,
                            oem_reprocessed, 
                            main_competitor,   material, 
                            size,             type, 
                            date_updated,     date_added,
                            crtd_by AS crt_by,  crt_dttm AS crt_tmst
                           FROM gbi_stg.dim_prod_css  ) css_stg_drg_prod
          ) map WHERE map.r_num = 1
		) prod
     LEFT JOIN gbi_stg.scalinglevel_css scal ON upper(prod.sub_platform::text) = upper(scal.sub_platform::text);

commit;
end ;


