/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/


\set ON_ERROR_STOP on

begin;

delete from gbi_wrk.intrm_int_sls_dps
where opco ='3';

commit;

insert into gbi_wrk.intrm_int_sls_dps
(prod_cd,fisc_mo_yr,opco,aggr_eus_prod_qty,aggr_eus_prod_sls_amt,src_sys_cd,crtd_by,updtd_by,crt_dttm,updt_dttm)
select 
	trim(upper(product_code)), 
	cast(fiscal_year_number as integer)*100+cast(fiscal_month as integer), 
	company_code,   	sum(eaches_qty),   	sum(sales_amt), 
  	'DPS' as src_sys_cd ,  	'GBI_WRK_INTRM_SLS_DPS_MITEK' as crtd_by ,  	null as updt_by ,
  	now() as crt_dttm, 	now() updt_dttm
	from 
	gbi_stg.dps_mitek_acct_sales
	where shpto_cot_cd in (select busn_rule_gr_3  from gbi_cdh.busn_rule where busn_rule_gr_2='MITEK'
and busn_rule_gr_1='DPS' and rule_seq='1' )
	and   shpto_setting_hosp_ooh not in (select busn_rule_gr_3  from gbi_cdh.busn_rule where busn_rule_gr_2='ALL'
and busn_rule_gr_1='DPS' and rule_seq='2')
	group by 	trim(upper(product_code)), 	cast(fiscal_year_number as integer)*100+cast(fiscal_month as integer), 
	company_code, 	src_sys_cd ;
	
commit;
end;