/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin;

truncate table gbi_wrk.intrm_stem_alctn_dps;

WITH 
Bipolar AS 
	(	
	select 
		dtr.fisc_mo_id as fisc_mo_yr_bipolar,
		coalesce(sum(tot_units),'0') as tot_units_bipolar,
		coalesce(sum(tot_dol),'0') as tot_dol_bipolar 
	from
		(select 
			distinct fisc_mo_id 
		from gbi_cdh.dim_time_rolling) dtr 
		left outer join 
		(SELECT 
			jnj_code as prod_cd,
			sls_dps.fisc_mo_yr,
			aggr_eus_prod_qty as tot_units, 
			aggr_eus_prod_sls_amt as tot_dol
		FROM 
		(	
			select 
				distinct jnj_code 
			from gbi_wrk.intrm_prod_sclng 
				where sub_pltf= 'Bipolar' and src_fran='DPS' and jnj_code <> 'NA'
		) prod_dps
	INNER JOIN gbi_wrk.intrm_int_sls_dps sls_dps
	ON prod_dps.jnj_code = replace(trim(upper(sls_dps.prod_cd)),'-','') 
		) bp 
	ON dtr.fisc_mo_id=bp.fisc_mo_yr
	group by dtr.fisc_mo_id
	) , 
	
Unipolar AS 
	(
	select 
		dtr.fisc_mo_id as fisc_mo_yr_unipolar,
		coalesce(sum(tot_units),'0') as tot_units_unipolar,
		coalesce(sum(tot_dol),'0') as tot_dol_unipolar 
	from
		(select 
			distinct fisc_mo_id 
		from gbi_cdh.dim_time_rolling) dtr 
		left outer join 
		(SELECT 
			jnj_code as prod_cd,
			sls_dps.fisc_mo_yr,
			aggr_eus_prod_qty as tot_units, 
			aggr_eus_prod_sls_amt as tot_dol
		FROM 
		(	
			select 
				distinct jnj_code 
			from gbi_wrk.intrm_prod_sclng 
				where sub_pltf= 'Unipolar' and src_fran='DPS' and jnj_code <> 'NA'
		) prod_dps
	INNER JOIN gbi_wrk.intrm_int_sls_dps sls_dps
	ON prod_dps.jnj_code = replace(trim(upper(sls_dps.prod_cd)),'-','') 
		) bp 
	ON dtr.fisc_mo_id=bp.fisc_mo_yr
	group by dtr.fisc_mo_id
	),
	
Femur_FX AS
	(
	select 
		dtr.fisc_mo_id as fisc_mo_yr_femur,
		coalesce(sum(tot_units),'0') as tot_units_femur,
		coalesce(sum(tot_dol),'0') as tot_dol_femur 
	from
		(select 
			distinct fisc_mo_id 
		from gbi_cdh.dim_time_rolling) dtr 
		left outer join 
		(SELECT 
			jnj_code as prod_cd,
			sls_dps.fisc_mo_yr,
			aggr_eus_prod_qty as tot_units, 
			aggr_eus_prod_sls_amt as tot_dol
		FROM 
		(	
			select 
				distinct jnj_code 
			from gbi_wrk.intrm_prod_sclng 
				where sub_pltf= 'Femur-FX' and src_fran='DPS' and jnj_code <> 'NA'
		) prod_dps
	INNER JOIN gbi_wrk.intrm_int_sls_dps sls_dps
	ON prod_dps.jnj_code = replace(trim(upper(sls_dps.prod_cd)),'-','') 
		) bp 
	ON dtr.fisc_mo_id=bp.fisc_mo_yr
	group by dtr.fisc_mo_id
	),
	
Femur_Coated AS
	(
	select 
		dtr.fisc_mo_id as fisc_mo_yr_FC,
		coalesce(sum(tot_units),'0') as tot_units_FC,
		coalesce(sum(tot_dol),'0') as tot_dol_FC 
	from
		(select 
			distinct fisc_mo_id 
		from gbi_cdh.dim_time_rolling) dtr 
		left outer join 
		(SELECT 
			jnj_code as prod_cd,
			sls_dps.fisc_mo_yr,
			aggr_eus_prod_qty as tot_units, 
			aggr_eus_prod_sls_amt as tot_dol
		FROM 
		(	
			select 
				distinct left(jnj_code, length(jnj_code) - 3) as jnj_code 
			from gbi_wrk.intrm_prod_sclng 
				where sub_pltf= 'Femur-Coated' and src_fran='DPS' and jnj_code <> 'NA'
		) prod_dps
	INNER JOIN gbi_wrk.intrm_int_sls_dps sls_dps
	ON prod_dps.jnj_code = replace(trim(upper(sls_dps.prod_cd)),'-','') 
		) bp 
	ON dtr.fisc_mo_id=bp.fisc_mo_yr
	group by dtr.fisc_mo_id
	),
	
Femur_Uncoated AS
	(
	select 
		dtr.fisc_mo_id as fisc_mo_yr_FU,
		coalesce(sum(tot_units),'0') as tot_units_FU,
		coalesce(sum(tot_dol),'0') as tot_dol_FU 
	from
		(select 
			distinct fisc_mo_id 
		from gbi_cdh.dim_time_rolling) dtr 
		left outer join 
		(SELECT 
			jnj_code as prod_cd,
			sls_dps.fisc_mo_yr,
			aggr_eus_prod_qty as tot_units, 
			aggr_eus_prod_sls_amt as tot_dol
		FROM 
		(	
			select 
				distinct left(jnj_code, length(jnj_code) - 3) as jnj_code 
			from gbi_wrk.intrm_prod_sclng 
				where sub_pltf= 'Femur-Uncoated' and src_fran='DPS' and jnj_code <> 'NA'
		) prod_dps
	INNER JOIN gbi_wrk.intrm_int_sls_dps sls_dps
	ON prod_dps.jnj_code = replace(trim(upper(sls_dps.prod_cd)),'-','') 
		) bp 
	ON dtr.fisc_mo_id=bp.fisc_mo_yr
	group by dtr.fisc_mo_id
	),
	
Primary_Partial_Consolidation AS
	(
		select 
				Bipolar.fisc_mo_yr_bipolar,
				Unipolar.fisc_mo_yr_unipolar,
				Femur_FX.fisc_mo_yr_femur,
				Femur_Uncoated.fisc_mo_yr_FU,
				Femur_Coated.fisc_mo_yr_FC,
				case when Unipolar.tot_units_unipolar is NULL then 0 else Unipolar.tot_units_unipolar end as tot_units_unipolar , 
				case when Bipolar.tot_units_bipolar is NULL then 0 else Bipolar.tot_units_bipolar end as tot_units_bipolar,
				case when Femur_FX.tot_units_femur is NULL then 0 else Femur_FX.tot_units_femur end as tot_units_femur,
				case when Femur_Uncoated.tot_units_FU is NULL then 0 else Femur_Uncoated.tot_units_FU end as tot_units_FU ,
				case when Femur_Coated.tot_units_FC is NULL then 0 else Femur_Coated.tot_units_FC end as tot_units_FC ,
				case when Unipolar.tot_dol_unipolar  is NULL then 0 else Unipolar.tot_dol_unipolar end as tot_dol_unipolar,
				case when Bipolar.tot_dol_bipolar  is NULL then 0 else Bipolar.tot_dol_bipolar end as tot_dol_bipolar,
				case when Femur_FX.tot_dol_femur is NULL then 0 else Femur_FX.tot_dol_femur end as tot_dol_femur,
				case when Femur_Uncoated.tot_dol_FU is NULL then 0 else Femur_Uncoated.tot_dol_FU end as tot_dol_FU,
				case when Femur_Coated.tot_dol_FC is NULL then 0 else Femur_Coated.tot_dol_FC end as tot_dol_FC
			from 
				Bipolar 
			full outer join Unipolar 
			on Bipolar.fisc_mo_yr_bipolar = Unipolar.fisc_mo_yr_unipolar
			full outer join Femur_FX 
			on Unipolar.fisc_mo_yr_unipolar= Femur_FX.fisc_mo_yr_femur
			full outer join Femur_Coated 
			on Unipolar.fisc_mo_yr_unipolar = Femur_Coated.fisc_mo_yr_FC
			full outer join Femur_Uncoated 
			on Femur_Uncoated.fisc_mo_yr_FU=Femur_Coated.fisc_mo_yr_FC

	) , 
	
Total_Partial_Calculation AS	
	(select 
	fisc_mo_yr_bipolar as fisc_mo_yr,
	tot_units_unipolar,
	tot_dol_unipolar,
	tot_units_bipolar,
	tot_dol_bipolar,
	tot_units_femur,
	tot_dol_femur,
	(tot_units_unipolar + tot_units_bipolar - tot_units_femur) as tot_units,
	(tot_dol_unipolar + tot_dol_bipolar - tot_dol_femur) as tot_dol,
	tot_units_FU,
	tot_units_FC,
	tot_dol_FU,
	tot_dol_FC,
	'DPS' as src_sys_cd, 
	now(),
	now() 
	from 
	Primary_Partial_Consolidation),

Partial_Allocation_Percentage AS	
(select
	Total_Partial_Calculation.*,
	case
		when tot_units=0 then 0
		when tot_units > tot_units_FU then 1 
		when (tot_units < tot_units_FU and tot_units_FU <> 0) then tot_units/tot_units_FU
		when (tot_units < tot_units_FU and tot_units_FU = 0) then NULL 
		end as Percent_Allocation_FU,
	case
		when tot_units=0 then 0
		when tot_units < tot_units_FU then 0
		when (tot_units > tot_units_FU and tot_units_FC <> 0) then (tot_units - tot_units_FU)/tot_units_FC
		when (tot_units > tot_units_FU and tot_units_FC = 0) then NULL 
		end as Percent_Allocation_FC
	from	
		Total_Partial_Calculation)
		
insert into gbi_wrk.intrm_stem_alctn_dps
(	fisc_mo_yr ,
	tot_units_bipolar ,
	tot_dol_bipolar ,
	tot_units_unipolar ,
	tot_dol_unipolar ,
	tot_units_femurfx ,
	tot_dol_femurfx ,
	tot_units_par ,
	tot_dol_par ,
	tot_units_femurcoated,
	tot_dol_femurcoated ,
	tot_units_femuruncoated ,
	tot_dol_femuruncoated ,
	Percent_Allocation_FU ,
	Percent_Allocation_FC ,
	src_sys_cd ,
	crt_dttm,
        crtd_by)
select 
	fisc_mo_yr,
	tot_units_bipolar,
	tot_dol_bipolar,
	tot_units_unipolar,
	tot_dol_unipolar ,
	tot_units_femur ,
	tot_dol_femur,
	tot_units,
	tot_dol,
	tot_units_fc ,
	tot_dol_fc ,
	tot_units_fu ,
	tot_dol_fu ,
	Percent_Allocation_FU ,
	Percent_Allocation_FC ,
	src_sys_cd ,
	now(),
        'GBI_WRK_INTRM_STEM_ALLC_DPS'::text
from	
	Partial_Allocation_Percentage;
	
commit;
end;