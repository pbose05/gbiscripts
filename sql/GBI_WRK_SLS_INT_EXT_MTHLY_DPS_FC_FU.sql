/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin;

----gbi_cdh.sls_int_ext_mnthly population for 'Femur-Uncoated' & 'Femur-Coated'

delete from gbi_wrk.intrm_int_ext_mnthly_sls_dps
where crtd_by ='GBI_WRK_SLS_INT_EXT_MTHLY_DPS_FC_FU' 
/*
(prod_cd,mfr_nm,pltf) in 
				(select prod_cd,mfr_nm,pltf
				 from gbi_wrk.intrm_prod_sclng where sub_pltf IN ('Femur-Uncoated','Femur-Coated') and src_fran='DPS' and jnj_code<>'NA')
*/
;

commit;

with Prod_Scaling_FU as
(select distinct 
	left(jnj_code, length(jnj_code) - 3) as ref_prod_cd,
	jnj_code,
	prod_cd,
	mfr_nm, 
	fran_cd, 
	pltf as platform, 
	sub_pltf as sub_platform
from gbi_wrk.intrm_prod_sclng
	where 
	sub_pltf= 'Femur-Uncoated' and src_fran='DPS'),
	
Prod_Scaling_FC as
(select distinct 
	left(jnj_code, length(jnj_code) - 3) as ref_prod_cd,
	jnj_code,
	prod_cd,
	mfr_nm, 
	fran_cd, 
	pltf as platform, 
	sub_pltf as sub_platform
from gbi_wrk.intrm_prod_sclng
	where 
	sub_pltf= 'Femur-Coated' and src_fran='DPS'),
	
dps_sales_FU as	
(select 
	Prod_Scaling_FU.ref_prod_cd,
	Prod_Scaling_FU.jnj_code,
	Prod_Scaling_FU.prod_cd,
	sls_fu.fisc_mo_yr, 
	sls_fu.aggr_eus_prod_qty, 
	sls_fu.aggr_eus_prod_sls_amt, 
	Prod_Scaling_FU.mfr_nm,
	Prod_Scaling_FU.fran_cd,
	--Prod_Scaling_FU.franchise,
	Prod_Scaling_FU.platform,
	Prod_Scaling_FU.sub_platform,
	sls_fu.src_sys_cd
from gbi_wrk.intrm_int_sls_dps sls_fu
	inner join Prod_Scaling_FU
 on sls_FU.prod_cd=Prod_Scaling_FU.ref_prod_cd ),
 
 
 dps_sales_FC as	
(select 
	Prod_Scaling_FC.ref_prod_cd,
	Prod_Scaling_FC.jnj_code,
	Prod_Scaling_FC.prod_cd,
	sls_fc.fisc_mo_yr, 
	sls_fc.aggr_eus_prod_qty, 
	sls_fc.aggr_eus_prod_sls_amt, 
	Prod_Scaling_FC.mfr_nm,
	Prod_Scaling_FC.fran_cd,
	--Prod_Scaling_FC.franchise,
	Prod_Scaling_FC.platform,
	Prod_Scaling_FC.sub_platform,
	sls_fc.src_sys_cd
from gbi_wrk.intrm_int_sls_dps sls_fc
	inner join Prod_Scaling_FC
 on sls_FC.prod_cd=Prod_Scaling_FC.ref_prod_cd ),
 
 
dps_sales_FU_allocation as 
( select 
	dps_sales_FU.ref_prod_cd,
	dps_sales_FU.jnj_code,
	dps_sales_FU.prod_cd,
	dps_sales_FU.fisc_mo_yr, 
	case
		when (platform='Primary' and (percent_allocation_fu IS NOT NULL or percent_allocation_fu <> 0 or percent_allocation_fu <> 1)) 
				then aggr_eus_prod_qty*(1 - percent_allocation_fu)
		when (platform='Primary' and (percent_allocation_fu IS NULL or percent_allocation_fu= 0 ))
				then aggr_eus_prod_qty
		when (platform='Primary' and percent_allocation_fu = 1) 
				then 0
		when (platform='Partial' and (percent_allocation_fu IS NOT NULL or percent_allocation_fu <> 0 or percent_allocation_fu <> 1)) 
				then aggr_eus_prod_qty*percent_allocation_fu
		when (platform='Partial' and (percent_allocation_fu IS NULL or percent_allocation_fu= 0 ))
				then 0
		when (platform='Partial' and percent_allocation_fu = 1)
				then aggr_eus_prod_qty
	end as aggr_prod_qty,
	case
		when (platform='Primary' and (percent_allocation_fu IS NOT NULL or percent_allocation_fu <> 0 or percent_allocation_fu <> 1)) 
				then aggr_eus_prod_sls_amt*(1 - percent_allocation_fu)
		when (platform='Primary' and (percent_allocation_fu IS NULL or percent_allocation_fu= 0 ))
				then aggr_eus_prod_sls_amt
		when (platform='Primary' and percent_allocation_fu = 1) 
				then 0
		when (platform='Partial' and (percent_allocation_fu IS NOT NULL or percent_allocation_fu <> 0 or percent_allocation_fu <> 1)) 
				then aggr_eus_prod_sls_amt*percent_allocation_fu
		when (platform='Partial' and (percent_allocation_fu IS NULL or percent_allocation_fu= 0 ))
				then 0
		when (platform='Partial' and percent_allocation_fu = 1)
				then aggr_eus_prod_sls_amt
	end as aggr_prod_sls_amt,
	dps_sales_FU.mfr_nm,
	dps_sales_FU.fran_cd,
	--dps_sales_FU.franchise,
	dps_sales_FU.platform,
	dps_sales_FU.sub_platform,
	dps_sales_FU.src_sys_cd
from dps_sales_FU
inner join gbi_wrk.intrm_stem_alctn_dps dps_sls_alctn_fu
on  dps_sales_FU.fisc_mo_yr = dps_sls_alctn_fu.fisc_mo_yr ),



dps_sales_FC_allocation as 
( select 
	dps_sales_FC.ref_prod_cd,
	dps_sales_FC.jnj_code,
	dps_sales_FC.prod_cd,
	dps_sales_FC.fisc_mo_yr, 
	case
		when (platform='Primary' and (percent_allocation_fc IS NOT NULL or percent_allocation_fc <> 0 or percent_allocation_fc <> 1)) 
				then aggr_eus_prod_qty*(1 - percent_allocation_fc)
		when (platform='Primary' and (percent_allocation_fc IS NULL or percent_allocation_fc= 0 ))
				then aggr_eus_prod_qty
		when (platform='Primary' and percent_allocation_fc = 1) 
				then 0
		when (platform='Partial' and (percent_allocation_fc IS NOT NULL or percent_allocation_fc <> 0 or percent_allocation_fc <> 1)) 
				then aggr_eus_prod_qty*percent_allocation_fc
		when (platform='Partial' and (percent_allocation_fc IS NULL or percent_allocation_fc= 0 ))
				then 0
		when (platform='Partial' and percent_allocation_fc = 1)
				then aggr_eus_prod_qty
	end as aggr_prod_qty,
	case
		when (platform='Primary' and (percent_allocation_fc IS NOT NULL or percent_allocation_fc <> 0 or percent_allocation_fc <> 1)) 
				then aggr_eus_prod_sls_amt*(1 - percent_allocation_fc)
		when (platform='Primary' and (percent_allocation_fc IS NULL or percent_allocation_fc= 0 ))
				then aggr_eus_prod_sls_amt
		when (platform='Primary' and percent_allocation_fc = 1) 
				then 0
		when (platform='Partial' and (percent_allocation_fc IS NOT NULL or percent_allocation_fc <> 0 or percent_allocation_fc <> 1)) 
				then aggr_eus_prod_sls_amt*percent_allocation_fc
		when (platform='Partial' and (percent_allocation_fc IS NULL or percent_allocation_fc= 0 ))
				then 0
		when (platform='Partial' and percent_allocation_fc = 1)
				then aggr_eus_prod_sls_amt
	end as aggr_prod_sls_amt,
	dps_sales_FC.mfr_nm,
	dps_sales_FC.fran_cd,
	--dps_sales_FC.franchise,
	dps_sales_FC.platform,
	dps_sales_FC.sub_platform,
	dps_sales_FC.src_sys_cd
from dps_sales_FC
inner join gbi_wrk.intrm_stem_alctn_dps dps_sls_alctn_fc
on  dps_sales_FC.fisc_mo_yr = dps_sls_alctn_fc.fisc_mo_yr )




insert into  gbi_wrk.intrm_int_ext_mnthly_sls_dps
	(fisc_mo_id,
	prod_cd,
	mfr_nm,
	src_sys_cd,
	fran_cd,
	soc_setng,
	pltf,
	comp_sls_amt,
	comp_sls_unit,
	cnsmr_sls_amt,
	cnsmr_sls_unit,
	nts_sls_amt,
	nts_sls_unit,
	crtd_by,
	updt_by,
	crt_dttm,
	updt_dttm)
(select 
	fisc_mo_yr,
	prod_cd,
	mfr_nm,
	'DPS' as src_sys_cd,
	'DPS',
	'HOSP',
	platform,
	0,
	0,
	aggr_prod_sls_amt,
	aggr_prod_qty,
	0,
	0,
	'GBI_WRK_SLS_INT_EXT_MTHLY_DPS_FC_FU',   
	'GBI_WRK_SLS_INT_EXT_MTHLY_DPS_FC_FU',
	now(),
	now()
from 
	dps_sales_FU_allocation
union all
	select 
		fisc_mo_yr,
		prod_cd,
		mfr_nm,
		'DPS' as src_sys_cd,
		'DPS' as fran_cd,
		'HOSP',
		platform,
		0,
		0,
		aggr_prod_sls_amt,
		aggr_prod_qty,
		0,
		0,
	'GBI_WRK_SLS_INT_EXT_MTHLY_DPS_FC_FU',   
	'GBI_WRK_SLS_INT_EXT_MTHLY_DPS_FC_FU',
		now(),
		now()
	from 
		dps_sales_FC_allocation
);

	
commit;
end;