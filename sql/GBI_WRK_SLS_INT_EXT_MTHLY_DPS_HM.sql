/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/


\set ON_ERROR_STOP on


begin;


delete from gbi_wrk.intrm_int_ext_mnthly_sls_dps
where crtd_by='GBI_WRK_SLS_INT_EXT_MTHLY_DPS_HM'
/*
(prod_cd,mfr_nm,pltf) in 
				(select prod_cd,mfr_nm,pltf
				 from gbi_wrk.intrm_prod_sclng where sub_pltf= 'Head-Metal' and src_fran='DPS' and jnj_code<>'NA')
*/
;

				 
COMMIT;



with Prod_Scaling_HM as
(select distinct 
	left(jnj_code, length(jnj_code) - 3) as ref_prod_cd,
	jnj_code,
	prod_cd,
	mfr_nm, 
	fran_cd, 
	pltf as platform, 
	sub_pltf as sub_platform
from gbi_wrk.intrm_prod_sclng
	where 
	UPPER(sub_pltf)= 'HEAD-METAL' and src_fran='DPS'),
	

	
dps_sales_HM as	
(select 
	Prod_Scaling_HM.ref_prod_cd,
	Prod_Scaling_HM.jnj_code,
	Prod_Scaling_HM.prod_cd,
	sls_fu.fisc_mo_yr, 
	sls_fu.aggr_eus_prod_qty, 
	sls_fu.aggr_eus_prod_sls_amt, 
	Prod_Scaling_HM.mfr_nm,
	Prod_Scaling_HM.fran_cd,
	--Prod_Scaling_HM.franchise,
	Prod_Scaling_HM.platform,
	Prod_Scaling_HM.sub_platform,
	sls_fu.src_sys_cd
from gbi_wrk.intrm_int_sls_dps sls_fu
	inner join Prod_Scaling_HM
 on sls_FU.prod_cd=Prod_Scaling_HM.ref_prod_cd ),
 
 

 
dps_sales_HM_allocation as 
( select 
	dps_sales_HM.ref_prod_cd,
	dps_sales_HM.jnj_code,
	dps_sales_HM.prod_cd,
	dps_sales_HM.fisc_mo_yr, 
	case
		when (platform='Primary' and (percent_allocation_hm IS NOT NULL or percent_allocation_hm <> 0 or percent_allocation_hm <> 1)) 
				then aggr_eus_prod_qty*(1 - percent_allocation_hm)
		when (platform='Primary' and (percent_allocation_hm IS NULL or percent_allocation_hm= 0 ))
				then aggr_eus_prod_qty
		when (platform='Primary' and percent_allocation_hm = 1) 
				then 0
		when (platform='Partial' and (percent_allocation_hm IS NOT NULL or percent_allocation_hm <> 0 or percent_allocation_hm <> 1)) 
				then aggr_eus_prod_qty*percent_allocation_hm
		when (platform='Partial' and (percent_allocation_hm IS NULL or percent_allocation_hm= 0 ))
				then 0
		when (platform='Partial' and percent_allocation_hm = 1)
				then aggr_eus_prod_qty
	end as aggr_prod_qty,
	case
		when (platform='Primary' and (percent_allocation_hm IS NOT NULL or percent_allocation_hm <> 0 or percent_allocation_hm <> 1)) 
				then aggr_eus_prod_sls_amt*(1 - percent_allocation_hm)
		when (platform='Primary' and (percent_allocation_hm IS NULL or percent_allocation_hm= 0 ))
				then aggr_eus_prod_sls_amt
		when (platform='Primary' and percent_allocation_hm = 1) 
				then 0
		when (platform='Partial' and (percent_allocation_hm IS NOT NULL or percent_allocation_hm <> 0 or percent_allocation_hm <> 1)) 
				then aggr_eus_prod_sls_amt*percent_allocation_hm
		when (platform='Partial' and (percent_allocation_hm IS NULL or percent_allocation_hm= 0 ))
				then 0
		when (platform='Partial' and percent_allocation_hm = 1)
				then aggr_eus_prod_sls_amt
	end as aggr_prod_sls_amt,
	dps_sales_HM.mfr_nm,
	dps_sales_HM.fran_cd,
	--dps_sales_HM.franchise,
	dps_sales_HM.platform,
	dps_sales_HM.sub_platform,
	dps_sales_HM.src_sys_cd
from dps_sales_HM
inner join gbi_wrk.intrm_knee_alctn_dps dps_sls_alctn_hm
on  dps_sales_HM.fisc_mo_yr = dps_sls_alctn_hm.fisc_mo_id )





insert into gbi_wrk.intrm_int_ext_mnthly_sls_dps
(
	fisc_mo_id,
	prod_cd,
	mfr_nm,
	src_sys_cd,
	fran_cd,
	soc_setng,
	pltf,
	cnsmr_sls_amt,
	cnsmr_sls_unit,
	comp_sls_amt,
	comp_sls_unit,
	nts_sls_amt,
	nts_sls_unit,
	crtd_by,
	updt_by,
	crt_dttm,
	updt_dttm
)
select 
	fisc_mo_yr ,
	prod_cd,
	mfr_nm,
	'DPS' as src_sys_cd,
	'DPS' as fran_cd,
	'HOSP' as soc_setng,
	platform,
	aggr_prod_sls_amt,
	aggr_prod_qty,
	0,
	0,
	0,
	0,
	'GBI_WRK_SLS_INT_EXT_MTHLY_DPS_HM',
	'GBI_WRK_SLS_INT_EXT_MTHLY_DPS_HM',
	now(),
	now()
from 
	dps_sales_HM_allocation;


COMMIT ;
end;