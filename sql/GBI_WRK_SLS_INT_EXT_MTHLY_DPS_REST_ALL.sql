/*
* Copyright: Copyright © 2017
* This file contains trade secrets of Johnson & Johnson. No part may be reproduced or transmitted in any
* form by any means or for any purpose without the express written permission of Johnson & Johnson.
*/

\set ON_ERROR_STOP on

begin;


delete from gbi_wrk.intrm_int_ext_mnthly_sls_dps
where crtd_by='GBI_WRK_INTRM_INT_EXT_MNTHLY_SLS_DPS_RST_ALL'
/*
(prod_cd,mfr_nm,pltf) in 
				(select 
		distinct prod_cd,mfr_nm, pltf
		from gbi_wrk.intrm_prod_sclng where sub_pltf not in ('Head-Metal','Femur-Coated','Femur-Uncoated') 
		and src_fran ='DPS' and jnj_code <> 'NA')
*/
 ;
						
COMMIT;

insert into gbi_wrk.intrm_int_ext_mnthly_sls_dps
(fisc_mo_id,prod_cd,mfr_nm,src_sys_cd,fran_cd,soc_setng,pltf,cnsmr_sls_amt,cnsmr_sls_unit,comp_sls_amt,comp_sls_unit,nts_sls_amt,nts_sls_unit,
crtd_by,updt_by,crt_dttm,updt_dttm)
select 
fisc_mo_yr as fisc_mo_id,
prod.prod_cd, 
mfr_nm, 
'DPS' as src_sys_cd,
'DPS' as fran_cd,
'HOSP' as soc_setng,
pltf as pltf,
aggr_eus_prod_sls_amt as cnsmr_sls_amt,
aggr_eus_prod_qty as cnsmr_sls_unit,
0 as comp_sls_amt,
0 as comp_sls_unit,
0 as nts_sls_amt,
0 as nts_sls_unit,
'GBI_WRK_INTRM_INT_EXT_MNTHLY_SLS_DPS_RST_ALL' as crtd_by,
'GBI_WRK_INTRM_INT_EXT_MNTHLY_SLS_DPS_RST_ALL' as updt_by,
now() as crt_dttm,
now() as updt_dttm
from (select 
		distinct prod_cd,jnj_code, mfr_nm,pltf
		from gbi_wrk.intrm_prod_sclng where sub_pltf not in ('Head-Metal','Femur-Coated','Femur-Uncoated') 
		and src_fran ='DPS' and jnj_code <> 'NA') prod
INNER JOIN gbi_wrk.intrm_int_sls_dps sls_dps
ON trim(upper(prod.jnj_code)) = trim(upper(sls_dps.prod_cd)) ;


COMMIT ;
end;