create temp table JR_Extr as
select sls.co_cd,sls.co_nm, fisc_yr_nbr ,substring(fisc_yr_mo, 5,2) fisc_mo,sls.prod_cd,
case when sls.uom=sls.alt_uom_cd then  'EA' else sls.uom end as UOM,
nvl(shpto_cust_nm,'UNKNOWN') as shpto_cust_nm,shpto_cot_cd,
nvl(shpto_uni_cust_no,'99999998') as shpto_uni_cust_no,
nvl(decode(shpto_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN') Shpto_Setting_Hosp_OOH,
nvl(decode(indv_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN') Indv_Setting_Hosp_OOH,
nvl(indv_cust_nm,'UNKNOWN') as indv_cust_nm,
indv_cot_cd,
nvl(indv_uni_cust_no,'99999998') as indv_uni_cust_no,
sum(eaches_qty) eaches_Qty,sum(net_sls_amt) sales_amt,sysdate
 from 
(select s.*,alt_uom_cd,uom from (select sf.uhs_co_cd,sf.prod_cd,sf.eaches_qty,sf.net_sls_amt,sf.co_cd,
sf.uni_cust_no ,co.co_nm,cal.*,c.* from onemd_adhoc.vw_f_sls_dps sf,onemd_adhoc.vw_co_dim co,onemd_adhoc.vw_cal_dim cal,
onemd_adhoc.vw_cust_all_hier_dim c where sf.invc_dt=cal.cal_dt and sf.co_cd =co.dpy_co_cd and sf.co_cd = '1'
and invc_dt>= '30-Dec-2013' and sf.uni_cust_no=c.shpto_uni_cust_no )s
left outer join (select distinct p.prod_cd,p.uom,u.alt_uom_cd,eaches_qty from
(select * from onemd_adhoc.vw_prod_src_dim where sls_org_co_cd= '1' ) p
left outer join
ecg_dwh.ems_mdm_unit_measure_ean_upc u on
p.prod_cd=u.prod_cd
and p.uom=u.alt_uom_cd) ef
on ef.prod_cd=s.prod_cd)sls
group by sls.co_cd,sls.co_nm, fisc_yr_nbr ,substring(fisc_yr_mo, 5,2),prod_cd,uom,alt_uom_cd,shpto_cust_nm,
shpto_cot_cd,
shpto_uni_cust_no,
indv_cust_nm,
indv_cot_cd,
indv_uni_cust_no,
nvl(decode(shpto_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN'),
nvl(decode(indv_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN') 
;
unload ('select * from JR_Extr')
to 's3://itx-aew-onemd-gbi-prod/landing/JR_Extr/'
credentials 'aws_access_key_id=v1;aws_secret_access_key=v2'
allowoverwrite;