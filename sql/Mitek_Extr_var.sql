create temp table Mitek_Extr as
select co_cd,co_nm,fisc_yr_nbr,fisc_mo,prod_cd,uom,shpto_cust_nm,shpto_cot_cd,shpto_uni_cust_no,
nvl(decode
(shpto_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN') Shpto_Setting_Hosp_OOH,
nvl(decode
(indv_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN') 
Indv_Setting_Hosp_OOH,
indv_cust_nm,indv_cot_cd,indv_uni_cust_no,sum(eaches_qty) eaches_qty,sum(sales_amt) sales_amt,sysdate from (
-- Direct Sales
select sls.co_cd,'MITEK SPORTS' AS Co_nm,fisc_yr_nbr,substring(fisc_yr_mo, 5,2) fisc_mo,sls.prod_cd,
nvl(lwr_lvl_pkg_uom_cd,prod_uom_cd) uom,shpto_cust_nm,shpto_cot_cd,uni_cust_no shpto_uni_cust_no,indv_cust_nm,indv_cot_cd,indv_uni_cust_no,
sum(qty*nvl(c.eaches_qty,1))  as eaches_qty,sum(Net_Sls_amt) sales_amt from onemd_adhoc.vw_f_sls_dps  sls                                     
						left join ecg_dwh.ems_mdm_unit_measure_ean_upc c on ltrim(sls.prod_cd,'0') = c.prod_cd 
						and alt_uom_cd = decode(prod_uom_cd,'PAK','PK','KI','KT',prod_uom_cd)
						left join onemd_adhoc.vw_cal_dim cal on invc_dt=cal_Dt
						left join (select distinct indv_uni_cust_no, indv_cust_nm,indv_cot_cd, shpto_uni_cust_no, 
                    				shpto_cust_nm,shpto_cot_cd from onemd_adhoc.vw_cust_all_hier_dim ) cust on sls.uni_cust_no=cust.shpto_uni_cust_no	
		 				where  sls.co_cd='3' and invc_dt>='30-Dec-2013' and sls_typ_cd='DIRECT' and 
						nvl(sls.uni_cust_no,'00') not in (select distinct nvl(shpto_ucn,'999998') from onemd_adhoc.vw_cust_sls_rep_algn where 
area_id='70' and prvdr_id='OPCO_DPM')
						group by sls.co_cd,fisc_yr_nbr,substring(fisc_yr_mo, 5,2),sls.prod_cd,
nvl(lwr_lvl_pkg_uom_cd,prod_uom_cd),shpto_cust_nm,shpto_cot_cd,uni_cust_no,indv_cust_nm,indv_cot_cd,indv_uni_cust_no
union all
-- Indirect Sales
select co_cd,'MITEK SPORTS' co_nm,fisc_yr_nbr,substring(fisc_yr_mo, 5,2) fisc_mo,prod_cd,null uom,
nvl(decode(shpto_cust_nm,'',name,shpto_cust_nm),name) shpto_cust_nm,shpto_cot_cd,acct_num,indv_cust_nm,indv_cot_cd ,indv_uni_cust_no,
sum(qty),sum(net_sls_amt) net_sls_amt from ecg_dwh.fact_indir_sls sls,onemd_adhoc.vw_cal_dim cal,(select distinct indv_uni_cust_no, indv_cust_nm,indv_cot_cd, 
shpto_uni_cust_no, shpto_cust_nm,shpto_cot_cd
from onemd_adhoc.vw_cust_all_hier_dim ) cust 
where TO_DATE(to_date(invc_dt,'MMDDYYYY'),'YYYY-MM-DD')=cal_Dt(+) and nvl(sls.acct_num,'000')=cust.shpto_uni_cust_no(+) 
--and decode(sls.sls_typ_cd,NULL,'X',sls.sls_typ_cd)='X' and sls.co_cd='3'
and TO_DATE(to_date(invc_dt,'MMDDYYYY'),'YYYY-MM-DD')>='2013-12-30' 
and  nvl(sls.acct_num,'000') not in (select distinct nvl(shpto_ucn,'9999998') from onemd_adhoc.vw_cust_sls_rep_algn where area_id='70' and prvdr_id='OPCO_DPM')
group by co_cd,fisc_yr_nbr,substring(fisc_yr_mo, 5,2),prod_cd,nvl(decode(shpto_cust_nm,'',name,shpto_cust_nm),name),
shpto_cot_cd,indv_cust_nm,indv_cot_cd,acct_num,indv_uni_cust_no) group by 
co_cd,co_nm,fisc_yr_nbr,fisc_mo,prod_cd,uom,shpto_cust_nm,shpto_cot_cd,shpto_uni_cust_no,
nvl(decode
(shpto_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN'),
nvl(decode
(indv_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN'),
indv_cust_nm,indv_cot_cd,indv_uni_cust_no
;
unload ('select * from Mitek_Extr')
to 's3://itx-aew-onemd-gbi-prod/landing/Mitek_Extr/'
credentials 'aws_access_key_id=v1;aws_secret_access_key=v2'
allowoverwrite;
