create temp table Trcmf_Extr as
select co_cd,co_nm,fisc_yr_nbr,fisc_mo,prod_cd,uom,shpto_cust_nm,shpto_cot_cd,shpto_uni_cust_no,
nvl(decode(shpto_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN') Shpto_Setting_Hosp_OOH,
nvl(decode(indv_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN') Indv_Setting_Hosp_OOH,
indv_cust_nm,indv_cot_cd,indv_uni_cust_no,sum(eaches_qty) eaches_qty,sum(sales_amt) sales_amt,sysdate from
(select co_cd,co_nm,fisc_yr_nbr,fisc_mo,prod_cd,nvl(lwr_lvl_pkg_uom_cd,uom) uom,
shpto_cust_nm,shpto_cot_cd,shpto_uni_cust_no,sum(eache_qty) eaches_qty,sum(Net_Sls_amt) sales_amt,indv_cust_nm,indv_cot_cd,indv_uni_cust_no
from (select sls.*,fisc_yr_nbr,lwr_lvl_pkg_uom_cd,substring(fisc_yr_mo, 5,2) fisc_mo,qty * nvl(conv.eaches_qty,1)  as eache_qty,
indv_uni_cust_no, indv_cust_nm,indv_cot_cd, shpto_uni_cust_no, shpto_cust_nm,shpto_cot_cd
  from (select s.*,uom,co_nm from onemd_adhoc.vw_f_sls_dps s,(select  distinct doc,ln,uom  FROM ECG_INT.jde_trauma_CMF_sales  S,
                       ECG_INT.JDE_TRAUMA_CMF_USHITM P ,
                       onemd_adhoc.vw_cal_dim C
                 WHERE s.ENTERED_ITEM = p.ITEM_NUMBER      
                       and S.Gl_dt = C.CAL_DT_NUM          
                       and s.SLS_CAT_SCT = P.DIVISION      
                   and SLS_CAT_SCT IN ('100', '150')                 
                   and p.DIVISION IN ('100', '150') and S.Gl_dt = C.CAL_DT_NUM and 
				   trunc(TO_DATE(to_date(inv_dt,'YYYYMMDD'),'YYYY-MM-DD'))>='2013-12-30') i,
		           onemd_adhoc.vw_co_dim co
				   where co_cd in ('11','12') and invc_dt>='30-Dec-2013' 
				   and s.invc_no=i.doc(+) and s.invc_ln_no=i.ln(+) and s.co_cd=co.dpy_co_cd) sls,
				   ecg_dwh.ems_mdm_unit_measure_ean_upc conv,onemd_adhoc.vw_cal_dim cal,(select distinct indv_uni_cust_no, indv_cust_nm,indv_cot_cd, shpto_uni_cust_no, shpto_cust_nm,shpto_cot_cd
from onemd_adhoc.vw_cust_all_hier_dim ) cust 
				   where invc_dt=cal_Dt(+) and sls.uni_cust_no=cust.shpto_uni_cust_no(+) 
				and  sls.prod_cd=conv.prod_cd(+) and conv.alt_uom_cd(+) = sls.uom)
group by co_cd,co_nm,fisc_yr_nbr,fisc_mo,prod_cd,shpto_cust_nm,shpto_cot_cd,shpto_uni_cust_no,nvl(lwr_lvl_pkg_uom_cd,uom)
,indv_cust_nm,indv_cot_cd,indv_uni_cust_no) group by 
co_cd,co_nm,fisc_yr_nbr,fisc_mo,prod_cd,uom,shpto_cust_nm,shpto_cot_cd,shpto_uni_cust_no,
nvl(decode(shpto_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN'),
nvl(decode(indv_cot_cd,'1056','Hospital','1019','Hospital','1000','Hospital','1044','Hospital','1045','Hospital','1149','Hospital','1017','Hospital','0050','OOH','7200','OOH','9000','OOH',
'4100','OOH','1050','OOH','5103','OOH','9002','OOH','1054','OOH','1052','OOH','9001','OOH','5102','OOH','9003','OOH','5104','OOH','7300','OOH','3804','OOH','3803','OOH','5200','OOH',
'1059','OOH','9400','OOH','7950','OOH','1046','OOH','4000','OOH','6900','OOH','3800','OOH','9999','OOH','3026','OOH','6700','OOH','4036','OOH','7999','OOH','1047','OOH','6800','OOH',
'3200','OOH','7500','OOH','3806','OOH','9500','OOH','5201','OOH','1060','OOH','7600','OOH','3900','OOH','8000','OOH','6601','OOH','3025','OOH','6901','OOH','6500','OOH','3024','OOH',
'9700','OOH','5300','OOH','1145','OOH','6101','OOH','2999','OOH','3023','OOH','5932','OOH','2000','OOH','1018','OOH','2930','OOH','2800','OOH','5935','OOH','3001','OOH','6103','OOH',
'3740','OOH','2925','OOH','6100','OOH','3016','OOH','2899','OOH','5936','OOH','3009','OOH','5931','OOH','6200','OOH','4134','OOH','3002','OOH','9800','OOH','1147','OOH','5900','OOH',
'5101','OOH','6300','OOH','3020','OOH','3000','OOH','3710','OOH','6102','OOH','5934','OOH','2417','OOH','2415','OOH','2301','OOH','2416','OOH','5933','OOH'),'UNKNOWN'),
indv_cust_nm,indv_cot_cd,indv_uni_cust_no
;
unload ('select * from Trcmf_Extr')
to 's3://itx-aew-onemd-gbi-prod/landing/Trcmf_Extr/'
credentials 'aws_access_key_id=v1;aws_secret_access_key=v2'
allowoverwrite;